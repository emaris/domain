package org.iotc.emaris.test.requirement;

import static java.util.Collections.emptyList;
import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;
import org.iotc.emaris.requirement.data.RequirementService;

import apprise.backend.data.Events.DatabaseReady;
import static test.apprise.backend.iam.tag.Testmate.stageTagsOf;;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes @Priority(LIBRARY_BEFORE) DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    RequirementService service;

    @Inject
    RequirementDao dao;

    public static Requirement stageDepsOf(Requirement requirement) {

        stageTagsOf(requirement);

        requirement.lineage().forEach(lineage ->

        stage(someRequirement()
                .id(lineage)
                .lineage(emptyList())
                .tags(emptyList()))

        );

        return requirement;

    }

    public static Requirement stage(Requirement requirement) {

        if (_this.dao.lookup(requirement.id()).isPresent())
            return requirement;

        stageDepsOf(requirement);

        return _this.service.add(requirement);

    }

}
