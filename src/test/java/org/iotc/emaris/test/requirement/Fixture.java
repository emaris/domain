package org.iotc.emaris.test.requirement;

import static apprise.backend.bytestream.Fixture.someBytestreamWith;
import static apprise.backend.bytestream.Fixture.someDocumentWith;
import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static java.lang.String.format;
import static org.iotc.emaris.requirement.Constants.requirementPrefix;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static test.apprise.backend.Fixture.someMultilingual;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.any;
import static test.apprise.backend.Testmate.listOf;
import static test.apprise.backend.iam.Fixture.someTenant;

import java.util.Set;

import org.iotc.emaris.common.TenantAudience;
import org.iotc.emaris.requirement.Requirement;

import apprise.backend.model.Id;

public class Fixture {

    public static Requirement someRequirement() {

        String id = Id.mint(requirementPrefix);

        Requirement requirement = new Requirement()
                .id(id)
                .name(someMultilingual(format("requirement %s", id)))
                .lineage(listOf(Id.mint("R")))
                .audience(someTagExpession(tenantType))
                .audienceList(new TenantAudience()
                    .includes(Set.of(someTenant().ref(),someTenant().ref()))
                    .excludes(Set.of(someTenant().ref(),someTenant().ref()))
                )
                .userProfile(someTagExpession(userType))
                .description(someMultilingual(any("requirement-description")))
                .tags(someTag(requirementType), someTag().type(requirementType));

        requirement.documents().put("id1", listOf(
                someDocumentWith(someBytestreamWith("test1.1")),
                someDocumentWith(someBytestreamWith("test1.2"))
        ));
        requirement.documents().put("id2", listOf(
            someDocumentWith(someBytestreamWith("test2.1"))
    )); 

        return requirement;
    }
}
