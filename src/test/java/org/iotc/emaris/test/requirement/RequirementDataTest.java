package org.iotc.emaris.test.requirement;

import static org.iotc.emaris.requirement.Requirement.State.active;
import static org.iotc.emaris.requirement.Requirement.State.inactive;
import static org.iotc.emaris.requirement.data.RequirementFilter.filter;
import static org.iotc.emaris.requirement.data.RequirementFilter.withNoConditions;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.iotc.emaris.test.requirement.Testmate.stage;
import static org.iotc.emaris.test.requirement.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.Fixture.someMultilingual;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;
import org.iotc.emaris.requirement.data.RequirementService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class RequirementDataTest extends Testbase {

    @Inject
    RequirementDao dao;

    @Inject
    RequirementService service;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        Requirement added;
        Requirement updated;
        Requirement removed;

        void onAdd(@Observes @Added Requirement requirement) {
            this.added = requirement;
        }

        void onUpdate(@Observes @Updated Requirement requirement) {
            this.updated = requirement;
        }

        void onRemove(@Observes @Removed Requirement requirement) {
            this.removed = requirement;
        }
    }

    @Test
    public void can_add_and_retrieve_requirements() {

        Requirement requirement = someRequirement();

        stageDepsOf(requirement);

        List<Requirement> requirements = dao.all(withNoConditions());

        assertFalse(requirements.contains(requirement));

        service.add(requirement);

        requirements = dao.all(withNoConditions());

        assertTrue(requirements.contains(requirement));

        Requirement retrieved = dao.get(requirement.id());

        assertEquals(requirement, retrieved);

    }

    @Test
    public void can_observe_requirement_creation() {

        Requirement requirement = stage(someRequirement());

        assertEquals(requirement, observed.added());

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_requirements() {

        Requirement requirement = stage(someRequirement());

        service.add(requirement);

    }

    @Test(expected = ValidationException.class)
    public void cannot_add_invalid_requirements() {

        Requirement requirement = someRequirement();

        requirement.name().inDefaultLanguage(null);

        stage(requirement);

    }

    @Test
    public void stages_requirements_only_if_they_dont_exist_already() {

        Requirement requirement = stage(someRequirement());

        Requirement changed = someRequirement().id(requirement.id());

        stageDepsOf(changed);

        dao.add(changed, true);

        Requirement retrieved = dao.get(requirement.id());

        assertEquals(requirement, retrieved);

    }

    @Test
    public void can_retrieve_all_active_requirements() {

        Requirement active = someRequirement().state(Requirement.State.active);
        Requirement inactive = someRequirement().state(Requirement.State.inactive);

        stage(active);
        stage(inactive);

        List<Requirement> requirements = dao.all(filter().state(Requirement.State.active));

        assertTrue(requirements.contains(active));
        assertFalse(requirements.contains(inactive));

    }

    @Test
    public void can_update_requirements_without_side_effects_and_observe_it() {

        Requirement requirement = stage(someRequirement()); // this we change
        Requirement anotherRequirement = stage(someRequirement()); // this we dont touch

        List<Requirement> requirements = dao.all(withNoConditions());

        requirement.name(someMultilingual("changed"));

        assertFalse(requirements.contains(requirement));

        service.update(requirement);

        requirements = dao.all(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(requirements.containsAll(setOf(requirement, anotherRequirement)));

        assertEquals(requirement, observed.updated());
    }

    @Test
    public void cannot_remove_active_guarded_or_predefined_requirements() {

        try {
            Requirement requirement = stage(someRequirement().state(active));
            service.remove(requirement.id());
            fail();
        } catch (ValidationException expected) {
        }

        try {
            Requirement requirement = stage(someRequirement().predefined(true));
            service.remove(requirement.id());
            fail();
        } catch (ValidationException expected) {
        }

        try {
            Requirement requirement = stage(someRequirement().guarded(true));
            service.remove(requirement.id());
            fail();
        } catch (ValidationException expected) {
        }
    }

    @Test
    public void can_remove_inactive_requirements_without_side_effects_and_observe_it() {

        Requirement requirement = someRequirement();

        requirement.state(inactive);

        stage(requirement);

        Requirement otherRequirement = stage(someRequirement());

        service.remove(requirement.id());

        List<Requirement> requirements = dao.all(withNoConditions());

        assertFalse(requirements.contains(requirement));
        assertTrue(requirements.contains(otherRequirement));

        assertEquals(requirement.id(), observed.removed().id());

    }
}
