package org.iotc.emaris.test.requirement;

import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static org.iotc.emaris.requirement.Requirement.Predefined.guarded;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.listOf;
import static test.apprise.backend.Testmate.setOf;

import org.iotc.emaris.requirement.Requirement;
import org.junit.Test;

public class RequirementTest {

    @Test
    public void requirements_roundtrip() {

        assertRoundTrips(someRequirement());
    }

    @Test
    public void changes_can_be_copied() {

        Requirement requirement = someRequirement().predefined(true).guarded(true);

        Requirement updated = requirement.copy();

        assertEquals(requirement, updated);
    }

    @Test
    public void changes_are_controlled() {

        Requirement requirement = someRequirement().guarded(true);
        Requirement original = requirement.copy();
        Requirement updated = requirement.copy();

        updated.audience(someTagExpession(tenantType));
        requirement.updateWith(updated);
        assertEquals(original, requirement);

        updated.userProfile(someTagExpession(userType));
        requirement.updateWith(updated);
        assertEquals(original, requirement);

        updated.lineage(listOf("anyreq"));
        requirement.updateWith(updated);
        assertEquals(original, requirement);

        updated.tags(setOf("anytag"));
        requirement.updateWith(updated);
        assertEquals(original, requirement);

        updated.state(Requirement.State.active);
        requirement.updateWith(updated);
        assertEquals(original, requirement);

        // some changes are allowed

        updated.name().inDefaultLanguage("changed");

        requirement.updateWith(updated);

        assertNotEquals(original, requirement);

    }

    @Test
    public void can_change_guarded_along_with_other_properties() {

        Requirement requirement = someRequirement().guarded(true);
        Requirement updated = requirement.copy();

        updated.guarded(false);
        updated.state(Requirement.State.active);

        requirement.updateWith(updated);

        assertFalse(requirement.guarded());
        assertEquals(requirement.state(), updated.state());
    }

    @Test
    public void cannot_change_predefined_properties() {

        Requirement event = someRequirement().guarded(true).predefine(guarded);
        Requirement updated = event.copy();

        updated.guarded(false);

        event.updateWith(updated);

        assertTrue(event.guarded());
    }

}
