package org.iotc.emaris.test.requirement;

import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.iotc.emaris.productinstance.api.ApiConstants.search;
import static org.iotc.emaris.requirement.Requirement.State.inactive;
import static org.iotc.emaris.requirement.Rights.manage_requirements;
import static org.iotc.emaris.requirement.api.ApiConstants.requirementapi;
import static org.iotc.emaris.requirement.data.RequirementFilter.filter;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.iotc.emaris.test.requirement.Testmate.stage;
import static org.iotc.emaris.test.requirement.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertForbidden;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;
import static test.apprise.backend.api.Apimate.statusOf;
import static test.apprise.backend.iam.Fixture.someUser;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.requirement.Requirement;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class RequirementApiTest extends Testbase {

	@Inject
	LoginService service;

	@Test
	public void requirements_can_be_created_retrieved_updated_and_removed() {

		Requirement requirement = someRequirement();

		stageDepsOf(requirement);

		Response response = at(requirementapi, requirement.id()).get();

		assertEquals(NOT_FOUND, statusOf(response));

		response = at(requirementapi).post(jsonOf(requirement));

		assertSuccessful(response);

		Requirement fetched = at(requirementapi, requirement.id()).get(Requirement.class);

		assertEquals(requirement, fetched);

		Collection<Requirement> requirements = at(requirementapi).get(collofrequirements);

		assertTrue(requirements.stream().map(p -> p.id()).collect(toList()).contains(requirement.id()));

		// some changes.
		requirement.description().inDefaultLanguage("changed");
		requirement.lifecycle().state(inactive); // lets us delete it later.

		response = at(requirementapi, requirement.id()).put(jsonOf(requirement));

		assertSuccessful(response);

		fetched = at(requirementapi, requirement.id()).get(Requirement.class);

		// properties are lazily loaded from list, so erase them before comparisons
		requirement.properties(null);
		fetched.properties(null);

		assertEquals(requirement, fetched);

		response = at(requirementapi, requirement.id()).delete();

		assertSuccessful(response);

		response = at(requirementapi, requirement.id()).get();

		assertFailed(response);
	}

	@Test
	public void creating_requirements_requires_privileges() {

		Requirement requirement = someRequirement();

		stageDepsOf(requirement);

		User user = stage(someUser());

		service.login(user);

		Response response = at(requirementapi).post(jsonOf(requirement));

		assertForbidden(response);

		user = stage(someUser().grant(manage_requirements));

		service.login(user);

		response = at(requirementapi).post(jsonOf(requirement));

		assertSuccessful(response);

	}

	@Test
	public void requirements_can_be_lookedup_in_bulk() {

		List<Requirement> requirements = listOf(stage(someRequirement()), stage(someRequirement()));

		List<Requirement> fetched = at(requirementapi,search).post(jsonOf(filter().ids(listOf(all(requirements).map(r->r.id()))))).readEntity(collofrequirements);

		assertEquals(requirements,fetched);
	}

	@Test
	public void updating_requirements_requires_privileges() {

		Requirement requirement = stage(someRequirement());

		User user = stage(someUser());

		service.login(user);

		Response response = at(requirementapi, requirement.id()).put(jsonOf(requirement));

		assertForbidden(response);

		user = stage(someUser().grant(manage_requirements.on(requirement.id())));

		service.login(user);

		response = at(requirementapi, requirement.id()).put(jsonOf(requirement));

		assertSuccessful(response);

	}

	@Test
	public void removing_requirements_requires_privileges() {

		Requirement requirement = stage(someRequirement().state(inactive));

		User user = stage(someUser());

		service.login(user);

		Response response = at(requirementapi, requirement.id()).delete();

		assertForbidden(response);

		user = stage(someUser().grant(manage_requirements.on(requirement.id())));

		service.login(user);

		response = at(requirementapi, requirement.id()).delete();

		assertSuccessful(response);

	}

	/////// helpers

	static GenericType<List<Requirement>> collofrequirements = new GenericType<List<Requirement>>() {
	};

}
