package org.iotc.emaris.test.campaign;

import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.iotc.emaris.campaign.Campaign.State.archived;
import static org.iotc.emaris.campaign.Rights.manage_campaigns;
import static org.iotc.emaris.campaign.api.ApiConstants.campaignapi;
import static org.iotc.emaris.campaign.stage.StagedData.campaignEnd;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;
import static org.iotc.emaris.eventinstance.EventDate.RelativeDate.Direction.after;
import static org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod.Unit.months;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.campaign.Testmate.stage;
import static org.iotc.emaris.test.campaign.Testmate.stageDepsOf;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertForbidden;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;
import static test.apprise.backend.api.Apimate.statusOf;
import static test.apprise.backend.iam.Fixture.someUser;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.api.LifecycleApi.CampaignAndSummary;
import org.iotc.emaris.campaign.archive.Dtos.CampaignData;
import org.iotc.emaris.campaign.data.CampaignSummary;
import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod;
import org.iotc.emaris.eventinstance.EventInstance;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class CampaignApiTest extends Testbase {

	@Inject
	LoginService service;

	@Test
	public void campaigns_can_be_created_retrieved_updated_and_removed() {

		Campaign campaign = someCampaign();

		stageDepsOf(campaign);

		Response response = at(campaignapi, campaign.id()).get();

		assertEquals(NOT_FOUND, statusOf(response));

		response = at(campaignapi).post(jsonOf(campaign));

		assertSuccessful(response);

		CampaignData fetched = at(campaignapi, campaign.id()).get(CampaignData.class);

		assertEquals(campaign, fetched.campaign());

		Collection<CampaignAndSummary> campaigns = at(campaignapi).get(collofcampaigns);

		assertTrue(listOf(all(campaigns).map(cs -> cs.campaign())).contains(campaign));

		// some changes.
		campaign.description().inDefaultLanguage("changed");
		campaign.lifecycle().state(archived); // lets us delete it later.

		response = at(campaignapi, campaign.id()).put(jsonOf(campaign));

		assertSuccessful(response);

		fetched = at(campaignapi, campaign.id()).get(CampaignData.class);

		assertEquals(campaign, fetched.campaign());

		response = at(campaignapi, campaign.id()).delete();

		assertSuccessful(response);

		response = at(campaignapi, campaign.id()).get();

		assertFailed(response);
	}

	@Test
	public void campaigns_summary_includes_start_and_end_dates() {

		Campaign campaign = stage(someCampaign());

		EventInstance start = someEventInstance()
				.campaign(campaign.id())
				.source(campaignStart.id())
				.tags(emptyList())
				.target(campaign.id())
				.lineage(null)
				.date(EventDate.absolute(OffsetDateTime.now()));

		RelativePeriod endperiod = new RelativePeriod(1, months, after);

		EventInstance end = someEventInstance()
				.campaign(campaign.id())
				.source(campaignEnd.id())
				.tags(emptyList())
				.target(campaign.id())
				.lineage(null)
				.date(EventDate.relativeTo(start).period(endperiod));

		stage(start, end);

		OffsetDateTime startdate = start.date().get().absoluteValue();
		OffsetDateTime enddate = startdate.plus(endperiod.number(), endperiod.unit().asTemporalUnit());

		List<CampaignAndSummary> summaries = at(campaignapi).get(collofcampaigns);

		CampaignSummary summary = summaries.stream()
				.filter(s -> s.campaign().equals(campaign))
				.findAny()
				.get()
				.summary();

		// compares durations as
		assertEquals(Duration.between(enddate, startdate), Duration.between(summary.endDate(), summary.startDate()));

	}

	@Test
	public void creating_campaigns_requires_privileges() {

		Campaign campaign = someCampaign();

		stageDepsOf(campaign);

		User user = stage(someUser());

		service.login(user);

		Response response = at(campaignapi).post(jsonOf(campaign));

		assertForbidden(response);

		user = stage(someUser().grant(manage_campaigns));

		service.login(user);

		response = at(campaignapi).post(jsonOf(campaign));

		assertSuccessful(response);

	}

	@Test
	public void updating_campaigns_requires_privileges() {

		Campaign campaign = stage(someCampaign());

		User user = stage(someUser());

		service.login(user);

		Response response = at(campaignapi, campaign.id()).put(jsonOf(campaign));

		assertForbidden(response);

		user = stage(someUser().grant(manage_campaigns.on(campaign.id())));

		service.login(user);

		response = at(campaignapi, campaign.id()).put(jsonOf(campaign));

		assertSuccessful(response);

	}

	@Test
	public void removing_campaigns_requires_privileges() {

		Campaign campaign = stage(someCampaign());

		User user = stage(someUser());

		service.login(user);

		Response response = at(campaignapi, campaign.id()).delete();

		assertForbidden(response);

		user = stage(someUser().grant(manage_campaigns.on(campaign.id())));

		service.login(user);

		response = at(campaignapi, campaign.id()).delete();

		assertSuccessful(response);

	}

	/////// helpers

	static GenericType<List<CampaignAndSummary>> collofcampaigns = new GenericType<List<CampaignAndSummary>>() {
	};

}
