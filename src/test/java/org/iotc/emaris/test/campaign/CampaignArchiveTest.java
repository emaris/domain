package org.iotc.emaris.test.campaign;

import static apprise.backend.Exceptions.sneak;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.campaign.Testmate.stage;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.iotc.emaris.test.partyinstance.Fixture.somePartyInstance;
import static org.iotc.emaris.test.partyinstance.Testmate.stage;
import static org.iotc.emaris.test.productinstance.Fixture.someProductInstance;
import static org.iotc.emaris.test.productinstance.Testmate.stage;
import static org.iotc.emaris.test.requirementinstance.Fixture.someRequirementInstance;
import static org.iotc.emaris.test.requirementinstance.Testmate.stage;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.function.Function;

import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.Campaign.State;
import org.iotc.emaris.campaign.archive.CampaignArchiveDao;
import org.iotc.emaris.campaign.archive.Dtos.ArchiveEntry;
import org.iotc.emaris.campaign.data.CampaignService;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.iotc.emaris.productinstance.ProductInstance;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import test.apprise.backend.Testbase;

public class CampaignArchiveTest extends Testbase {

    @Inject
    CampaignService campaigns;

    @Inject
    CampaignArchiveDao dao;

    @Inject
    ObjectMapper mapper;

    Function<JsonNode,ArchiveEntry> convert = sneak( (JsonNode e) -> mapper.convertValue(e, ArchiveEntry.class));

    @Test
    public void can_archive_campaigns() {

        Campaign campaign = stage(someCampaign());

        RequirementInstance req1 = someRequirementInstance().campaign(campaign.id());
        RequirementInstance req2 = someRequirementInstance().campaign(campaign.id());

        stage(req1,req2);

        ProductInstance prod1 = someProductInstance().campaign(campaign.id());
        ProductInstance prod2 = someProductInstance().campaign(campaign.id());

        stage(prod1,prod2);

        PartyInstance party1 = somePartyInstance().campaign(campaign.id());
        PartyInstance party2 = somePartyInstance().campaign(campaign.id());

        stage(party1,party2);
        
        EventInstance event1 = someEventInstance().campaign(campaign.id());
        EventInstance event2 = someEventInstance().source(event1.source()).campaign(campaign.id());

        stage(event1,event2);

        campaign.state(State.archived);
        
        campaigns.update(campaign);
        
        List<ArchiveEntry> entries = dao.fetch(campaign.id()).stream().map(convert).collect((toList()));

        assertTrue( List.of(req1,req2).stream().allMatch( e -> entries.stream().anyMatch(ee->ee.id().equals(e.source()))));
        assertTrue( List.of(prod1,prod2).stream().allMatch( e -> entries.stream().anyMatch(ee->ee.id().equals(e.source()))));
        assertTrue( List.of(party1,party2).stream().allMatch( e -> entries.stream().anyMatch(ee->ee.id().equals(e.source()))));
        assertTrue( List.of(event1,event2).stream().allMatch( e -> entries.stream().anyMatch(ee->ee.id().equals(e.source()))));

    }
  


   
}
