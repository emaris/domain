package org.iotc.emaris.test.campaign;

import static org.iotc.emaris.campaign.Campaign.Predefined.guarded;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.listOf;
import static test.apprise.backend.Testmate.setOf;

import org.iotc.emaris.campaign.Campaign;
import org.junit.Test;

public class CampaignTest {

    @Test
    public void campaigns_roundtrip() {

        assertRoundTrips(someCampaign());
    }

    @Test
    public void changes_can_be_copied() {

        Campaign campaign = someCampaign().predefined(true).guarded(true);

        Campaign updated = campaign.copy();

        assertEquals(campaign, updated);
    }

    @Test
    public void changes_are_controlled() {

        Campaign campaign = someCampaign().guarded(true);
        Campaign original = campaign.copy();

        Campaign updated = campaign.copy();

        updated.tags(setOf("anytag"));
        campaign.updateWith(updated);
        assertEquals(original, campaign);

        // some changes are allowed

        updated.name().inDefaultLanguage("changed");

        campaign.updateWith(updated);

        assertNotEquals(original, campaign);

    }

    @Test
    public void can_change_guarded_along_with_other_properties() {

        Campaign campaign = someCampaign().guarded(true);
        Campaign updated = campaign.copy();

        updated.guarded(false);
        updated.tags(listOf("tag"));

        campaign.updateWith(updated);

        assertFalse(campaign.guarded());
        assertEquals(campaign.state(), updated.state());
    }

    @Test
    public void cannot_change_predefined_properties() {

        Campaign campaign = someCampaign().guarded(true).predefine(guarded);
        Campaign updated = campaign.copy();

        updated.guarded(false);

        campaign.updateWith(updated);

        assertTrue(campaign.guarded());
    }


}
