package org.iotc.emaris.test.campaign;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;
import static org.iotc.emaris.campaign.api.ApiConstants.campaignapi;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static test.apprise.backend.iam.tag.Testmate.stageTagsOf;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.campaign.data.CampaignService;

import apprise.backend.data.Events.DatabaseReady;;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes @Priority(LIBRARY_BEFORE) DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    CampaignService service;

    @Inject
    CampaignDao dao;

    public static Campaign stageDepsOf(Campaign campaign) {

        stageTagsOf(campaign);

        if (campaign.lineage().isPresent())
            stage(someCampaign()
                    .id(campaign.lineage().get())
                    .lineage(null)
                    .tags(emptyList()));

        return campaign;

    }

    public static Campaign stage(Campaign campaign) {

        if (_this.dao.lookup(campaign.id()).isPresent())
            return campaign;

        stageDepsOf(campaign);

        return _this.service.add(campaign);

    }

    public static String routeOf(String campaign) {

        return format("%s/%s", campaignapi, campaign);

    }

    public static String routeOf(Campaign campaign) {

        return routeOf(campaign.id());

    }

    public static String campaignRouteOf(CampaignInstance<?> instance) {

        return routeOf(instance.campaign());

    }

}
