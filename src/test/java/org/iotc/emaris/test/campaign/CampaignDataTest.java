package org.iotc.emaris.test.campaign;

import static org.iotc.emaris.campaign.data.CampaignFilter.filter;
import static org.iotc.emaris.campaign.data.CampaignFilter.withNoConditions;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.campaign.Testmate.stage;
import static org.iotc.emaris.test.campaign.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.Fixture.someMultilingual;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.Campaign.State;
import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.campaign.data.CampaignService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class CampaignDataTest extends Testbase {

    @Inject
    CampaignDao dao;

    @Inject
    CampaignService service;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        Campaign added;
        Campaign updated;
        Campaign removed;

        void onAdd(@Observes @Added Campaign campaign) {
            this.added = campaign;
        }

        void onUpdate(@Observes @Updated Campaign campaign) {
            this.updated = campaign;
        }

        void onRemove(@Observes @Removed Campaign campaign) {
            this.removed = campaign;
        }
    }

    @Test
    public void can_add_and_retrieve_campaigns() {

        Campaign campaign = someCampaign();

        stageDepsOf(campaign);

        List<Campaign> campaigns = dao.all(withNoConditions());

        assertFalse(campaigns.contains(campaign));

        service.add(campaign);

        campaigns = dao.all(withNoConditions());

        assertTrue(campaigns.contains(campaign));

        Campaign retrieved = dao.get(campaign.id());

        assertEquals(campaign, retrieved);

    }

    @Test
    public void can_observe_campaign_creation() {

        Campaign campaign = stage(someCampaign());

        assertEquals(campaign, observed.added());

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_campaigns() {

        Campaign campaign = stage(someCampaign());

        service.add(campaign);

    }

    @Test(expected = ValidationException.class)
    public void cannot_add_invalid_campaigns() {

        Campaign campaign = someCampaign();

        campaign.name().inDefaultLanguage(null);

        stage(campaign);

    }

    @Test
    public void stages_campaigns_only_if_they_dont_exist_already() {

        Campaign campaign = stage(someCampaign());

        Campaign changed = someCampaign().id(campaign.id());

        stageDepsOf(changed);

        dao.add(changed, true);

        Campaign retrieved = dao.get(campaign.id());

        assertEquals(campaign, retrieved);

    }

    @Test
    public void can_retrieve_all_active_campaigns() {

        Campaign active = someCampaign().state(Campaign.State.active);
        Campaign archived = someCampaign().state(Campaign.State.archived);

        stage(active);
        stage(archived);

        List<Campaign> campaigns = dao.all(filter().state(Campaign.State.active));

        assertTrue(campaigns.contains(active));
        assertFalse(campaigns.contains(archived));

    }

    @Test
    public void can_update_campaigns_without_side_effects_and_observe_it() {

        Campaign campaign = stage(someCampaign()); // this we change
        Campaign anotherCampaign = stage(someCampaign()); // this we dont touch

        List<Campaign> campaigns = dao.all(withNoConditions());

        campaign.name(someMultilingual("changed"));

        assertFalse(campaigns.contains(campaign));

        service.update(campaign);

        campaigns = dao.all(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(campaigns.containsAll(setOf(campaign, anotherCampaign)));

        assertEquals(campaign, observed.updated());
    }

    @Test
    public void cannot_remove_active_guarded_or_predefined_campaigns() {

        try {
            Campaign campaign = stage(someCampaign().predefined(true));
            service.remove(campaign.id());
            fail();
        } catch (ValidationException expected) {
        }

        try {
            Campaign campaign = stage(someCampaign().guarded(true));
            service.remove(campaign.id());
            fail();
        } catch (ValidationException expected) {
        }
    }

    @Test
    public void can_remove_inactive_campaigns_without_side_effects_and_observe_it() {

        Campaign campaign = someCampaign();

        campaign.state(State.archived);

        stage(campaign);

        Campaign otherCampaign = stage(someCampaign());

        service.remove(campaign.id());

        List<Campaign> campaigns = dao.all(withNoConditions());

        assertFalse(campaigns.contains(campaign));
        assertTrue(campaigns.contains(otherCampaign));

        assertEquals(campaign, observed.removed());

    }
}
