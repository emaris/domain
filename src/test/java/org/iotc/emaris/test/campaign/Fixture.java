package org.iotc.emaris.test.campaign;

import static java.lang.String.format;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.campaign.Constants.campaignType;
import static test.apprise.backend.Fixture.someMultilingual;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Testmate.any;

import org.iotc.emaris.campaign.Campaign;

import apprise.backend.model.Id;

public class Fixture {

    public static Campaign someCampaign() {

        String id = Id.mint(campaignPrefix);

        return new Campaign()
                .id(id)
                .name(someMultilingual(format("campaign %s", id)))
                .description(someMultilingual(any("campaign-description")))
                .lineage(Id.mint(campaignPrefix))
                .tags(someTag(campaignType), someTag().type(campaignType));
    }
}
