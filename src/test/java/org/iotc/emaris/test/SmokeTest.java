package org.iotc.emaris.test;

import static apprise.backend.api.ApiConstants.info;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.junit.Assert.assertEquals;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.typeOf;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.junit.Test;

import apprise.backend.data.Staged;
import apprise.backend.tag.Tag;
import apprise.backend.tag.TagCategory;
import apprise.backend.tag.data.TagCategoryDao;
import apprise.backend.tag.data.TagDao;
import test.apprise.backend.Testbase;

public class SmokeTest extends Testbase {

	@Inject
	TagCategoryDao catdao;

	@Inject @Staged
	Instance<TagCategory> predefinedCategories;

	@Inject
	TagDao tagdao;

	@Inject @Staged
	Instance<Tag> predefinedTags;

	@Test
	public void exposes_info() {

		Response response = at(info).get();

		assertEquals(SUCCESSFUL, typeOf(response));
	}


	@Test
	public void predefined_categories_and_tags_are_staged() {

		predefinedCategories.forEach(c->assertEquals(c, catdao.get(c.id())));
		predefinedTags.forEach(t->assertEquals(t, tagdao.get(t.id())));

	}
}
