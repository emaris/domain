package org.iotc.emaris.test.requirementinstance;

import static org.iotc.emaris.requirementinstance.api.ApiConstants.byref;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.requirementinstanceapi;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.remove;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.search;
import static org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.requirementinstance.Fixture.someRequirementInstance;
import static org.iotc.emaris.test.requirementinstance.Testmate.stage;
import static org.iotc.emaris.test.requirementinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.Q;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.junit.Test;

import test.apprise.backend.Testbase;

public class RequirementInstanceApiTest extends Testbase {

    @Test
    public void can_add_retrieve_update_and_remove_instances() {

        RequirementInstance instance = someRequirementInstance();

        stageDepsOf(instance);

        Response response = at(requirementinstanceapi).post(jsonOf(listOf(instance)));

        assertSuccessful(response);

        RequirementInstance fetched = at(requirementinstanceapi, instance.id()).get(RequirementInstance.class);

        assertEquals(instance, fetched);

        List<RequirementInstance> instances = at(requirementinstanceapi, search).post(jsonOf(withNoConditions()))
                .readEntity(collofinstances);

        assertTrue(instances.contains(instance));

        // some changes.
        instance.properties().set(String.class, "changed").withDefault();

        response = at(requirementinstanceapi, instance.id()).put(jsonOf(instance));

        assertSuccessful(response);

        fetched = at(requirementinstanceapi, instance.id()).get(RequirementInstance.class);

        assertEquals(instance, fetched);

        response = at(requirementinstanceapi, instance.id()).delete();

        assertSuccessful(response);

        response = at(requirementinstanceapi, instance.id()).get();

        assertFailed(response);

    }

    @Test
    public void can_remove_instances_in_bulk() {

        RequirementInstance instance1 = someRequirementInstance();
        RequirementInstance instance2 = someRequirementInstance();
        RequirementInstance instance3 = someRequirementInstance();

        stage(instance1, instance2, instance3);

        Response response = at(requirementinstanceapi, remove).post(jsonOf(listOf(instance1.id(), instance2.id())));

        assertSuccessful(response);

        List<RequirementInstance> instances = at(requirementinstanceapi).get(collofinstances);

        assertFalse(instances.contains(instance1));
        assertFalse(instances.contains(instance2));
        assertTrue(instances.contains(instance3));

    }

    @Test
    public void can_retrieve_instances_byref() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        RequirementInstance retrieved = at(requirementinstanceapi, instance.ref(), Q(byref, true)).get(RequirementInstance.class);

        assertEquals(instance, retrieved);
    }

    @Test
    public void can_retrieve_instance_samples() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        List<RequirementInstance> retrieved = at(requirementinstanceapi).get(collofinstances);

        assertTrue(retrieved.contains(instance));
    }

    @Test
    public void can_retrieve_instances_directly_by_campaign() {

        RequirementInstance instance1 = someRequirementInstance();
        RequirementInstance instance2 = someRequirementInstance();

        stage(instance1,instance2);

        List<RequirementInstance> retrieved = at(requirementinstanceapi, campaign, instance1.campaign()).get(collofinstances);

        assertTrue(retrieved.contains(instance1));
        assertFalse(retrieved.contains(instance2));
    }

    /////// helpers

    static GenericType<List<RequirementInstance>> collofinstances = new GenericType<List<RequirementInstance>>() {
    };
}
