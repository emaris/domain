package org.iotc.emaris.test.requirementinstance;

import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.requirement.Constants.requirementPrefix;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.requirementinstance.Constants.requirementinstancePrefix;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Fixture.someTagExpession;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.requirementinstance.RequirementInstance;

import apprise.backend.model.Bag;
import apprise.backend.model.Id;

public class Fixture {

    public static RequirementInstance someRequirementInstance() {

        return new RequirementInstance()
                .id(Id.mint(requirementinstancePrefix))
                .source(Id.mint(requirementPrefix))
                .campaign(Id.mint(campaignPrefix))
                .lineage(new CampaignInstance.InstanceRef(Id.mint(campaignPrefix), Id.mint(requirementPrefix)))
                .audience(someTagExpession(tenantType))
                .userProfile(someTagExpession(userType))
                .tags(someTag(requirementType), someTag().type(requirementType))
                .properties(new Bag().set("prop","test").withDefault());

    }
    
}
