package org.iotc.emaris.test.requirementinstance;

import static org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter.filter;
import static org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.requirementinstance.Fixture.someRequirementInstance;
import static org.iotc.emaris.test.requirementinstance.Testmate.stage;
import static org.iotc.emaris.test.requirementinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class RequirementInstanceDataTest extends Testbase {

    @Inject
    RequirementInstanceService service;

    @Inject
    RequirementInstanceDao dao;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        List<RequirementInstance> addedBulk;
        RequirementInstance updated;
        RemovedInstanceEvents.Event<?> removedEvent;

        void onAdd(@Observes @Added List<RequirementInstance> instances) {
            this.addedBulk = instances;
        }

        void onUpdate(@Observes @Updated RequirementInstance instance) {
            this.updated = instance;
        }
        void onRemove(@Observes RemovedInstanceEvents.Event<?> event) {
            this.removedEvent = event;
        }
    }

    @Test
    public void instances_can_added_and_retrieved() {

        RequirementInstance instance1 = someRequirementInstance();
        RequirementInstance instance2 = someRequirementInstance();

        stageDepsOf(instance1);
        stageDepsOf(instance2);

        service.add(listOf(instance1, instance2));

        assertEquals(instance1, dao.get(instance1.id()));
        assertEquals(instance2, dao.get(instance2.id()));

    }

    @Test
    public void instances_can_retrieve_instances_by_ref() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        assertEquals(instance, dao.get(instance.ref()));

    }

    @Test
    public void can_observe_requirement_creation() {

        List<RequirementInstance> instances = stage(someRequirementInstance());

        assertEquals(instances, observed.addedBulk());

    }

    @Test
    public void cannot_add_invalid_instances() {

        try {
            stage(someRequirementInstance().campaign(null));
            fail();
        } catch (ValidationException expected) {
        }

    }

    @Test
    public void cannot_add_instances_outside_campaign() {

        try {
            RequirementInstance instance = someRequirementInstance();
            stageDepsOf(instance);
            service.add(listOf(instance.campaign("another")));
            //fail();
        } catch (Exception expected) {

            show(expected);
        }

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_instances() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        RequirementInstance duplicate = someRequirementInstance().id(instance.id());

        service.add(listOf(duplicate));

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_instances_with_same_ref() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        RequirementInstance duplicate = someRequirementInstance().campaign(instance.campaign()).source(instance.source());

        service.add(listOf(duplicate));

    }


    @Test
    public void can_retrieve_instances_in_campaign() {

        RequirementInstance instance1 = someRequirementInstance();
        RequirementInstance instance2 = someRequirementInstance();

        stage(instance1, instance2);

        List<RequirementInstance> instances = dao.allMatching(filter().campaign(instance1.campaign()));

        assertTrue(instances.contains(instance1));
        assertFalse(instances.contains(instance2));

    }

    @Test
    public void can_update_instances_without_side_effects_and_observe_it() {

        RequirementInstance instance1 = someRequirementInstance(); // this we change
        RequirementInstance instance2 = someRequirementInstance(); // this we dont touch

        stage(instance1, instance2);

        List<RequirementInstance> instances = dao.allMatching(withNoConditions());

        instance1.properties().set(String.class,"changed").withDefault();

        assertFalse(instances.contains(instance1));

        service.update(instance1);

        instances = dao.allMatching(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(instances.containsAll(setOf(instance1, instance2)));

        assertEquals(instance1, observed.updated());
    }


    @Test
    public void can_remove_instances_without_side_effects_and_observe_it() {

        RequirementInstance requirementinstance = someRequirementInstance();
        RequirementInstance otherRequirementInstance = someRequirementInstance();

        stage(requirementinstance,otherRequirementInstance);
        
        service.remove(requirementinstance.id());

        List<RequirementInstance> requirementinstances = dao.allMatching(withNoConditions());

        assertFalse(requirementinstances.contains(requirementinstance));
        assertTrue(requirementinstances.contains(otherRequirementInstance));

        assertEquals(List.of(requirementinstance), observed.removedEvent().instances());

    }


    @Test
    public void can_remove_multiple_instances_without_side_effects_and_observe_it() {

        RequirementInstance requirementinstance = someRequirementInstance();
        RequirementInstance otherRequirementInstance = someRequirementInstance();
        RequirementInstance yetAnotherRequirementInstance = someRequirementInstance();

        stage(requirementinstance,otherRequirementInstance,yetAnotherRequirementInstance);
        
        service.remove(listOf(requirementinstance.id(),otherRequirementInstance.id()));

        List<RequirementInstance> requirementinstances = dao.allMatching(withNoConditions());

        assertFalse(requirementinstances.containsAll(listOf(requirementinstance,otherRequirementInstance)));
        assertTrue(requirementinstances.contains(yetAnotherRequirementInstance));

        assertEquals(listOf(requirementinstance,otherRequirementInstance), observed.removedEvent().instances());

    }
}

