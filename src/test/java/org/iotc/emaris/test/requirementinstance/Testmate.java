package org.iotc.emaris.test.requirementinstance;

import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.iotc.emaris.test.requirementinstance.Fixture.someRequirementInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceService;

import apprise.backend.data.Events.DatabaseReady;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    RequirementInstanceService service;

    @Inject
    RequirementInstanceDao dao;

    public static RequirementInstance stageDepsOf(RequirementInstance instance) {

        test.apprise.backend.iam.tag.Testmate.stageTagsOf(instance);
        org.iotc.emaris.test.campaign.Testmate.stage(someCampaign().id(instance.campaign()));
        org.iotc.emaris.test.requirement.Testmate.stage(someRequirement().id(instance.source()).lineage(new ArrayList<>()));

        
        if (instance.lineage().isPresent()) {

            InstanceRef ref = instance.lineage().get();

            stage(someRequirementInstance()
                    .campaign(ref.campaign())
                    .source(ref.source())
                    .lineage(null)
            );
        }

        return instance;
    }

    public static List<RequirementInstance> stage(RequirementInstance ... instances) {

        List<RequirementInstance> newinstances = Stream.of(instances)
                .filter(i -> _this.dao.lookup(i.id()).isEmpty())
                .peek(Testmate::stageDepsOf)
                .collect(toList());

        return _this.service.add(newinstances);

    }
}
