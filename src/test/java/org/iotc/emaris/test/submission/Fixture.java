package org.iotc.emaris.test.submission;

import static apprise.backend.iam.Constants.tenantPrefix;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.requirement.Constants.requirementPrefix;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.submission.Constants.submissionPrefix;
import static org.iotc.emaris.submission.Constants.trailPrefix;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.SubmissionContent;
import org.iotc.emaris.submission.Trail;

import apprise.backend.model.Bag;
import apprise.backend.model.Id;

public class Fixture {

    public static Trail.Key someTrailKey() {

        return new Trail.Key().campaign(Id.mint(campaignPrefix))
                .party(Id.mint(tenantPrefix))
                // could be a product instead.
                .asset(Id.mint(requirementPrefix))  
                .assetType(requirementType);     
    }

    public static Trail someTrail() {

        Trail trail = new Trail()
                .id(Id.mint(trailPrefix))
                .key(someTrailKey())
                .properties(new Bag().set("prop", "test").withDefault());

        //trail.submissions(listOf(someSubmission(trail),someSubmission(trail)));

        return trail;

    }

    public static Trail someTrailForParty(String party) {

       Trail trail = someTrail();

       trail.key().party(party);

       return trail;

    }


    public static Submission someSubmission() {

        return someSubmissionIn(someTrail());
    }

    public static Submission someSubmissionIn(Trail trail) {

        return new Submission()
            .id(Id.mint(submissionPrefix))
            .content(someSubmissionContent())
            .trail(trail.id());
    }

    public static SubmissionContent someSubmissionContent() {

        return new SubmissionContent()
                        .data(new Bag().set("pref","test").withDefault());
    }
}
