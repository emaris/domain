package org.iotc.emaris.test.submission;

import static apprise.backend.iam.Rights.manage_tenants;
import static org.iotc.emaris.campaign.stage.StagedData.campaignEnd;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;
import static org.iotc.emaris.submission.data.AccessMode.stateless;
import static org.iotc.emaris.submission.data.SubmissionFilter.noConditions;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.iotc.emaris.test.submission.Fixture.someSubmission;
import static org.iotc.emaris.test.submission.Fixture.someSubmissionContent;
import static org.iotc.emaris.test.submission.Fixture.someSubmissionIn;
import static org.iotc.emaris.test.submission.Fixture.someTrail;
import static org.iotc.emaris.test.submission.Testmate.stage;
import static org.iotc.emaris.test.submission.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.iam.Fixture.someUserIn;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.time.OffsetDateTime;
import java.time.Period;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Submission.SubmissionState;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.SubmissionDao;
import org.iotc.emaris.submission.data.SubmissionService;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.TenantRef;
import apprise.backend.iam.model.User;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class SubmissionDataTest extends Testbase {

    @Inject
    SubmissionDao dao;

    @Inject
    SubmissionService service;

    @Inject
    EventObserver observed;

    @Inject
    LoginService loginservice;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        Submission added;
        Submission updated;
        Submission removed;

        void onAdd(@Observes @Added Submission submission) {
            this.added = submission;
        }

        void onUpdate(@Observes @Updated Submission submission) {
            this.updated = submission;
        }

        void onRemove(@Observes @Removed Submission submission) {
            this.removed = submission;
        }
    }

    @Test
    public void can_add_and_retrieve_submissions() {

        Submission submission = someSubmission();

        stageDepsOf(submission);

        List<Submission> submissions = dao.allMatching(noConditions());

        assertFalse(submissions.contains(submission));

        service.add(submission);

        submissions = dao.allMatching(noConditions());

        assertTrue(submissions.contains(submission));

        Submission retrieved = dao.get(submission.id(), stateless);

        assertEquals(submission, retrieved);

    }

    @Test
    public void can_observe_submission_creation() {

        Submission submission = stage(someSubmission());

        assertEquals(submission, observed.added());

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_submissions() {

        Submission submission = stage(someSubmission());

        service.add(submission);

    }

    @Test(expected = ValidationException.class)
    public void cannot_add_invalid_submissions() {

        Submission submission = someSubmission();

        submission.content(null);

        stage(submission);

    }

    @Test
    public void can_update_submissions_without_side_effects_and_observe_it() {

        Submission submission = stage(someSubmission()); // this we change
        Submission anotherSubmission = stage(someSubmission()); // this we dont touch

        List<Submission> submissions = dao.allMatching(noConditions());

        submission.state(SubmissionState.submitted);
        submission.lifecycle().compliance().state("super");
        submission.lifecycle().compliance().message("super indeed");
        submission.content(someSubmissionContent());

        assertFalse(submissions.contains(submission));

        service.update(submission);

        submissions = dao.allMatching(noConditions());

        // first has the changes, second is untouched.
        assertTrue(submissions.containsAll(setOf(submission,anotherSubmission)));

        assertEquals(submission, observed.updated());
    }

    @Test
    public void can_remove_submissions_without_side_effects_and_observe_it() {

        Submission submission = stage(someSubmission());
        Submission otherSubmission = stage(someSubmission());

        service.remove(submission.id());

        List<Submission> submissions = dao.allMatching(noConditions());

        assertFalse(submissions.contains(submission));
        assertTrue(submissions.contains(otherSubmission));

        assertEquals(submission, observed.removed());

    }

    @Test
    public void all_users_can_retrieve_their_own_submissions() {

        Trail trail = stage(someTrail());

        Submission submission = stage(someSubmissionIn(trail));

        User user = stage(someUserIn(new TenantRef(trail.key().party())));

        loginservice.login(user);

        assertTrue(dao.lookup(submission.id(), stateless).isPresent());

        List<Submission> submissions = dao.allMatching(noConditions());

        assertTrue(submissions.contains(submission));

    }

    @Test
    public void all_users_can_retrieve_submissions_they_manage() {

        Trail trail = stage(someTrail());

        Submission submission = stage(someSubmissionIn(trail));

        String party = trail.key().party();

        User user = stage(someUserIn(new TenantRef(party)).grant(manage_tenants.on(party)));

        loginservice.login(user);

        assertTrue(dao.lookup(submission.id(), stateless).isPresent());

        List<Submission> submissions = dao.allMatching(noConditions());

        assertTrue(submissions.contains(submission));

    }

    @Test
    public void users_cannot_retrieve_submissions_they_dont_manage() {

        User user = stage(someUserIn(new TenantRef("someparty")));

        Submission submission = stage(someSubmission());

        loginservice.login(user);

        assertFalse(dao.lookup(submission.id(), stateless).isPresent());

        List<Submission> tenants = dao.allMatching(noConditions());

        assertFalse(tenants.contains(submission));

    }

    @Test
    public void submission_dates_can_be_changed() {

        Submission submission = stage(someSubmission().state(SubmissionState.submitted));

        var date = OffsetDateTime.now().minus(Period.ofDays(1));

        submission.lifecycle().lastSubmitted(date);

        Submission updated = service.update(submission);

        assertEquals(date, updated.lifecycle().lastSubmitted());

    }


    @Test
    public void cannot_submit_after_campaign_lifecycle() {

        var trail =  stage(someTrail());

        var campaign = trail.key().campaign();

        var now = OffsetDateTime.now();

        var start = someEventInstance().campaign(campaign).source(campaignStart.id()).target(campaign).date(EventDate.absolute(now.minusDays(1)));
        var end = someEventInstance().campaign(campaign).source(campaignEnd.id()).target(campaign).date(EventDate.absolute(now.plusDays(1)));

        stage(start,end);

        var submission = someSubmissionIn(trail);
        
        submission.lifecycle().state(SubmissionState.draft);

        var added = service.add(submission);

        added.lifecycle().state(SubmissionState.submitted);

        try {
            added.lifecycle().lastSubmitted(now.minusDays(2));
            service.update(added);
            fail();
        }
        catch(IllegalStateException thrown) {}

        try {
            added.lifecycle().lastSubmitted(now.plusDays(2));
            service.update(added);
            fail();
        }
        catch(IllegalStateException thrown) {}

        added.lifecycle().lastSubmitted(now);
        service.update(added);


    }

}
