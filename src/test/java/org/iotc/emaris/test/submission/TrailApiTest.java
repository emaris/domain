package org.iotc.emaris.test.submission;

import static apprise.backend.iam.Rights.manage_tenants;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.submission.api.ApiConstants.published;
import static org.iotc.emaris.submission.api.ApiConstants.trailapi;
import static org.iotc.emaris.submission.data.TrailFilter.noConditions;
import static org.iotc.emaris.test.submission.Fixture.someSubmission;
import static org.iotc.emaris.test.submission.Fixture.someSubmissionIn;
import static org.iotc.emaris.test.submission.Fixture.someTrail;
import static org.iotc.emaris.test.submission.Testmate.stage;
import static org.iotc.emaris.test.submission.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertForbidden;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;
import static test.apprise.backend.iam.Fixture.someUser;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Submission.SubmissionState;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.TrailService;
import org.iotc.emaris.submission.data.TrailService.TrailAndSubmission;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class TrailApiTest extends Testbase {

	@Inject
	LoginService service;

	@Test
	public void trails_can_be_created_retrieved_updated_and_removed() {

		Trail staged = stageDepsOf(someTrail()); // need only a key, but also staged deps.

		Submission submission = someSubmission();

		Response response = at(trailapi).post(jsonOf(new  TrailService.KeyAndSubmission(staged.key(), submission)));

		assertSuccessful(response);

		Trail trail = response.readEntity(TrailAndSubmission.class).trail();

		Trail fetched = at(trailapi, trail.id()).get(Trail.class);

		assertEquals(trail.id(), fetched.id());

		Collection<Trail> trails = at(trailapi).get(colloftrails);

		assertTrue(trails.stream().map(t->t.id()).collect(toList()).contains(trail.id()));

		response = at(trailapi, trail.id()).put(jsonOf(trail));

		assertSuccessful(response);

		fetched = at(trailapi, trail.id()).get(Trail.class);

		assertEquals(trail.id(), fetched.id());

	}

	@Test
	public void updating_trails_requires_privileges() {

		Trail trail = stage(someTrail());

		User user = stage(someUser());

		service.login(user);

		Response response = at(trailapi, trail.id()).put(jsonOf(trail));

		assertForbidden(response);

		user = stage(someUser().grant(manage_tenants.on(trail.key().party())));

		service.login(user);

		response = at(trailapi, trail.id()).put(jsonOf(trail));

		assertSuccessful(response);

	}


	@Test
	public void all_users_can_fetch_published_closure() {

		Trail trail = stage(someTrail());

        Submission publishedSubmission = someSubmissionIn(trail);

        publishedSubmission.lifecycle().state(SubmissionState.published);

        stage(publishedSubmission);

		User user = stage(someUser());

        service.login(user);
		
		Response response = at(trailapi, published).post(jsonOf(noConditions()));

		assertSuccessful(response);

	}



	/////// helpers

	static GenericType<List<Trail>> colloftrails = new GenericType<List<Trail>>() {
	};

}
