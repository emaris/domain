package org.iotc.emaris.test.submission;

import static java.util.Collections.emptySet;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.submission.data.AccessMode.stateless;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.iotc.emaris.test.submission.Fixture.someTrail;

import java.util.ArrayList;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.SubmissionDao;
import org.iotc.emaris.submission.data.SubmissionService;
import org.iotc.emaris.submission.data.TrailDao;
import org.iotc.emaris.submission.data.TrailService;

import apprise.backend.data.Events.DatabaseReady;
import test.apprise.backend.iam.Fixture;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    TrailService trailService;

    @Inject
    TrailDao trailDao;

    @Inject
    SubmissionService submissionService;

    @Inject
    SubmissionDao submissionDao;

    public static Trail stageDepsOf(Trail trail) {

        org.iotc.emaris.test.campaign.Testmate.stage(someCampaign().id(trail.key().campaign()));
        test.apprise.backend.iam.admin.Testmate.stage(Fixture.someTenant().id(trail.key().party()));

        if (requirementType.equals(trail.key().assetType()))
            org.iotc.emaris.test.requirement.Testmate
                    .stage(someRequirement().id(trail.key().asset()).tags(emptySet()).lineage(new ArrayList<>()));

        if (productType.equals(trail.key().assetType()))
            org.iotc.emaris.test.product.Testmate
                    .stage(someProduct().id(trail.key().asset()).tags(emptySet()));

        return trail;
    }

    public static Trail stage(Trail trail) {

        if (_this.trailDao.lookup(trail.id()).isPresent())
            return trail;

        stageDepsOf(trail);

        return _this.trailService.add(trail);
    }

    public static Trail stageDepsOf(Submission submission) {

        return stage(someTrail().id(submission.trail()));
    }

    public static Submission stage(Submission submission) {

        if (_this.submissionDao.lookup(submission.id(), stateless).isPresent())
            return submission;

        stageDepsOf(submission);

        return _this.submissionService.add(submission);
    }
}
