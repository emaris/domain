package org.iotc.emaris.test.submission;

import static java.util.Collections.emptyList;
import static org.iotc.emaris.test.submission.Fixture.someSubmission;
import static org.iotc.emaris.test.submission.Fixture.someTrail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static test.apprise.backend.Testmate.assertRoundTrips;

import org.iotc.emaris.submission.Submission;
import org.junit.Test;

public class SubmissionTest {

    @Test
    public void trails_roundtrip() {

        assertRoundTrips(someTrail());
    }

    @Test
    public void submissions_roundtrip() {

        assertRoundTrips(someSubmission());
    }

    @Test
    public void submissions_with_compliance_roundtrip() {

        Submission submission = someSubmission();

        submission.lifecycle().compliance().state("somestate");

        assertRoundTrips(submission);
    }

    @Test
    public void changes_are_controlled() {

        Submission submission = someSubmission();
        Submission original = submission.copy();
        Submission updated = submission.copy();

        updated.id("newid");
        submission.updateWith(updated);
        assertEquals(original, submission);

        updated.trail("newtrail");
        submission.updateWith(updated);
        assertEquals(original, submission);

        // some changes are allowed


        updated.content().data().set(String.class,"anyprop").withDefault();
        submission.updateWith(updated);
        assertNotEquals(original, submission);

        updated.content().resources().put("newval", emptyList());
        submission.updateWith(updated);
        assertNotEquals(original, submission);

        updated.lifecycle().compliance().state("anystate");
        submission.updateWith(updated);
        assertNotEquals(original, submission);

    }
}
