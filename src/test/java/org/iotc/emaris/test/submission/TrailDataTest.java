package org.iotc.emaris.test.submission;

import static apprise.backend.iam.Rights.manage_tenants;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.submission.data.AccessMode.stateless;
import static org.iotc.emaris.submission.data.TrailFilter.filter;
import static org.iotc.emaris.submission.data.TrailFilter.noConditions;
import static org.iotc.emaris.test.requirementinstance.Fixture.someRequirementInstance;
import static org.iotc.emaris.test.requirementinstance.Testmate.stage;
import static org.iotc.emaris.test.submission.Fixture.someSubmissionIn;
import static org.iotc.emaris.test.submission.Fixture.someTrail;
import static org.iotc.emaris.test.submission.Fixture.someTrailForParty;
import static org.iotc.emaris.test.submission.Testmate.stage;
import static org.iotc.emaris.test.submission.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.iam.Fixture.someTenant;
import static test.apprise.backend.iam.Fixture.someUserIn;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.util.List;

import javax.inject.Inject;

import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceService;
import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Submission.SubmissionState;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.SubmissionDao;
import org.iotc.emaris.submission.data.TrailDao;
import org.iotc.emaris.submission.data.TrailService;
import org.iotc.emaris.submission.data.TrailService.TrailAndSubmission;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.Tenant;
import apprise.backend.iam.model.TenantRef;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class TrailDataTest extends Testbase {

    @Inject
    TrailService service;

    @Inject
    TrailDao dao;

    @Inject
    SubmissionDao submissions;

    @Inject
    LoginService loginservice;

    @Inject
    RequirementInstanceService requirementinstances;

    @Test
    public void trails_can_be_added_and_retrieved() {

        Trail trail = someTrail();

        stageDepsOf(trail);

        service.add(trail);

        assertEquals(trail, dao.get(trail.id()));

    }

    @Test
    public void cannot_add_trails_with_incomplete_key() {

        Trail trail = someTrail();

        stageDepsOf(trail);

        try {
            trail.key().campaign("another");
            service.add(trail);

            fail();

        } catch (Exception expected) {
        }

        try {
            trail.key().party("another");
            service.add(trail);
            fail();
        } catch (Exception expected) {
        }

        try {
            trail.key().asset("another");
            service.add(trail);
            fail();
        } catch (Exception expected) {
        }

        try {
            trail.key().assetType("another");
            service.add(trail);
            fail();
        } catch (Exception expected) {
        }
    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_trails() {

        Trail trail = stage(someTrail());

        Trail duplicate = someTrail().id(trail.id());

        service.add(duplicate);

    }

    @Test
    public void can_add_trails_along_with_a_submission() {

        Trail trail = someTrail();
        Submission submission = someSubmissionIn(trail);

        stageDepsOf(trail);

        TrailAndSubmission ts = service.addWith(new TrailService.KeyAndSubmission(trail.key(), submission));

        assertEquals(trail.key(), dao.get(ts.trail().id()).key());
        assertEquals(submission, submissions.get(ts.submission().id(), stateless));

    }

    @Test
    public void can_retrieve_trails_in_campaign() {

        Trail trail = stage(someTrail());
        Trail anotherTrail = stage(someTrail());

        List<Trail> trails = dao.allMatching(filter().campaign(trail.key().campaign()));

        assertTrue(trails.contains(trail));
        assertFalse(trails.contains(anotherTrail));

    }

    @Test
    public void can_update_trail_without_side_effects() {

        Trail trail = stage(someTrail()); // this we change
        Trail otherTrail = stage(someTrail()); // this we dont touch

        List<Trail> trails = dao.allMatching(noConditions());

        trail.properties().set(String.class, "changed").withDefault();

        assertFalse(trails.contains(trail));

        service.update(trail);

        trails = dao.allMatching(noConditions());

        // first has the changes, second is untouched.
        assertTrue(trails.containsAll(setOf(trail, otherTrail)));

    }

    @Test
    public void updating_a_trail_that_doesnt_exist_creates_it() {

        Trail trail = someTrail();

        stageDepsOf(trail);

        service.update(trail);

        assertEquals(trail, dao.get(trail.id()));

    }

    @Test
    public void users_cannot_retrieve_trails_in_other_tenants() {

        Trail trail = stage(someTrail());

        User user = stage(someUserIn(new TenantRef("someparty")));

        loginservice.login(user);

        assertFalse(dao.lookup(trail.id()).isPresent());

        List<Trail> trails = dao.allMatching(noConditions());

        assertFalse(trails.contains(trail));

    }

    @Test
    public void all_users_can_retrieve_their_own_trails() {

        Trail trail = stage(someTrail());

        String party = trail.key().party();

        User user = stage(someUserIn(new TenantRef(party)));

        loginservice.login(user);

        assertTrue(dao.lookup(trail.id()).isPresent());

        List<Trail> trails = dao.allMatching(noConditions());

        assertTrue(trails.contains(trail));

    }

    @Test
    public void all_users_can_retrieve_trails_they_manage() {

        String party = "someparty";
        User user = stage(someUserIn(new TenantRef(party)).grant(manage_tenants.on(party)));

        Trail trail = stage(someTrailForParty(party));

        loginservice.login(user);

        assertTrue(dao.lookup(trail.id()).isPresent());

        List<Trail> trails = dao.allMatching(noConditions());

        assertTrue(trails.contains(trail));

    }

    @Test
    public void users_cannot_retrieve_trails_they_dont_manage() {

        Tenant tenant = someTenant();

        User user = stage(someUserIn(tenant));

        Trail trail = stage(someTrail());

        loginservice.login(user);

        assertFalse(dao.lookup(trail.id()).isPresent());

        List<Trail> trails = dao.allMatching(noConditions());

        assertFalse(trails.contains(trail));

    }

    @Test
    public void users_can_retrieve_trails_published_submisions_in_trails_they_dont_manage() {

       Trail trail = stage(someTrail());

        Submission submission = stage(someSubmissionIn(trail));
        Submission publishedSubmission = someSubmissionIn(trail);

        publishedSubmission.lifecycle().state(SubmissionState.published);

        stage(publishedSubmission);

        Tenant tenant = someTenant();

        User user = stage(someUserIn(tenant));

        loginservice.login(user);

        List<Trail> trails = dao.allPublished(noConditions());

        Trail match = trails.stream().filter(t -> t.id().equals(trail.id())).findFirst().get();

        assertNotNull(match);

        assertFalse(match.submissions().contains(submission));
        assertTrue(match.submissions().contains(publishedSubmission));

    }

    @Test
    public void trails_are_removed_along_with_their_assets() {

        RequirementInstance instance = someRequirementInstance();

        stage(instance);

        Trail trail = someTrail();

        trail.key().campaign(instance.campaign()).asset(instance.source()).assetType(requirementType);

        stage(trail);

        Trail trailInSameCampaign = someTrail();

        trail.key().campaign(instance.campaign());

        stage(trailInSameCampaign);

        Trail trailInOtherCampaign = stage(someTrail());

        assertTrue(dao.lookup(trail.id()).isPresent());
        assertTrue(dao.lookup(trailInSameCampaign.id()).isPresent());
        assertTrue(dao.lookup(trailInOtherCampaign.id()).isPresent());

        requirementinstances.remove(instance.id());

        assertFalse(dao.lookup(trail.id()).isPresent());
        assertTrue(dao.lookup(trailInSameCampaign.id()).isPresent());
        assertTrue(dao.lookup(trailInOtherCampaign.id()).isPresent());

    }

}
