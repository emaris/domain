package org.iotc.emaris.test.submission;

import static org.iotc.emaris.submission.Submission.SubmissionState.submitted;
import static org.iotc.emaris.submission.api.ApiConstants.submissionapi;
import static org.iotc.emaris.test.submission.Fixture.someSubmission;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.submission.Submission;
import org.junit.Test;

import test.apprise.backend.Testbase;

public class SubmissionApiTest extends Testbase {

	@Test
	public void submissions_can_be_created_retrieved_updated_and_removed() {

		Submission submission = someSubmission(); // need only a key, but also staged deps.

		Testmate.stageDepsOf(submission);

		Response response = at(submissionapi).post(jsonOf(submission));

		assertSuccessful(response);

		Submission fetched = at(submissionapi, submission.id()).get(Submission.class);

		assertEquals(submission, fetched);

		Collection<Submission> submissions = at(submissionapi).get(collofsubmissions);

		assertTrue(submissions.contains(submission));

		submission.state(submitted);

		response = at(submissionapi, submission.id()).put(jsonOf(submission));

		assertSuccessful(response);

		fetched = at(submissionapi, submission.id()).get(Submission.class);

		assertEquals(submission, fetched);

		response = at(submissionapi,submission.id()).delete();

		assertSuccessful(response);

		response = at(submissionapi, submission.id()).get();

		assertFailed(response);

	}

	// @Test
	// public void updating_submissions_requires_privileges() {

	// 	Submission submission = stage(someSubmission());

	// 	User user = stage(someUser());

	// 	service.login(user);

	// 	Response response = at(submissionapi, submission.id()).put(jsonOf(submission));

	// 	assertForbidden(response);

	// 	user = stage(someUser().grant(manage_tenants.on(submission.key().party())));

	// 	service.login(user);

	// 	response = at(submissionapi, submission.id()).put(jsonOf(submission));

	// 	assertSuccessful(response);

	// }

	/////// helpers

	static GenericType<List<Submission>> collofsubmissions = new GenericType<List<Submission>>() {
	};

}
