package org.iotc.emaris.test.event;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.iotc.emaris.event.Rights.manage_events;
import static org.iotc.emaris.event.api.ApiConstants.eventapi;
import static org.iotc.emaris.event.Event.State.inactive;
import static org.iotc.emaris.test.event.Fixture.someEvent;
import static org.iotc.emaris.test.event.Testmate.stage;
import static org.iotc.emaris.test.event.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertForbidden;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;
import static test.apprise.backend.api.Apimate.statusOf;
import static test.apprise.backend.iam.Fixture.someUser;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.event.Event;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class EventApiTest extends Testbase {

	@Inject
	LoginService service;

	@Test
	public void events_can_be_created_retrieved_updated_and_removed() {

		Event event = someEvent();

		stageDepsOf(event);

		Response response = at(eventapi, event.id()).get();

		assertEquals(NOT_FOUND, statusOf(response));

		response = at(eventapi).post(jsonOf(event));

		assertSuccessful(response);

		Event fetched = at(eventapi, event.id()).get(Event.class);

		assertEquals(event, fetched);

		Collection<Event> events = at(eventapi).get(collofevents);

		assertTrue(events.contains(event));

		// some changes.
		event.description().inDefaultLanguage("changed");
		event.lifecycle().state(inactive); // lets us delete it later.

		response = at(eventapi, event.id()).put(jsonOf(event));

		assertSuccessful(response);

		fetched = at(eventapi, event.id()).get(Event.class);

		assertEquals(event, fetched);

		response = at(eventapi, event.id()).delete();

		assertSuccessful(response);

		response = at(eventapi, event.id()).get();

		assertFailed(response);
	}

	@Test
	public void creating_events_requires_privileges() {

		Event event = someEvent();

		stageDepsOf(event);

		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(eventapi).post(jsonOf(event));
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_events));
		
		service.login(user);
		
		response = at(eventapi).post(jsonOf(event));
		
		assertSuccessful(response);
		
	}

	@Test
	public void updating_events_requires_privileges() {

		Event event = stage(someEvent());

		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(eventapi,event.id()).put(jsonOf(event));
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_events.on(event.id())));
		
		service.login(user);
		
		response = at(eventapi,event.id()).put(jsonOf(event));
		
		assertSuccessful(response);
		
	}


	@Test
	public void removing_events_requires_privileges() {
		
		Event event = stage(someEvent().state(inactive));
		
		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(eventapi,event.id()).delete();
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_events.on(event.id())));
		
		service.login(user);
		
		response = at(eventapi, event.id()).delete();
		
		assertSuccessful(response);
		
	}

	
	/////// helpers
	
	static GenericType<List<Event>> collofevents = new GenericType<List<Event>>(){};
	
}
