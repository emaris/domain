package org.iotc.emaris.test.event;

import static java.lang.String.format;
import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static test.apprise.backend.Fixture.someMultilingual;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.any;

import org.iotc.emaris.event.Event;

import apprise.backend.model.Id;

public class Fixture {

    public static Event someEvent() {

        String id = Id.mint(eventPrefix);

        return new Event()
                .id(id)
                .name(someMultilingual(format("event %s", id)))
                //.managedTypes()
                .managed(true)
                .managedExpression(someTagExpession(eventType))
                .description(someMultilingual(any("event-description")))
                .tags(someTag(eventType), someTag().type(requirementType));
    }
}
