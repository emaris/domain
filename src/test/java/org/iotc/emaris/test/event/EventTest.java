package org.iotc.emaris.test.event;

import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.event.Event.Predefined.guarded;
import static org.iotc.emaris.event.Event.State.active;
import static org.iotc.emaris.event.Event.State.inactive;
import static org.iotc.emaris.test.event.Fixture.someEvent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.listOf;
import static test.apprise.backend.Testmate.setOf;

import java.util.List;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.Event.Predefined;
import org.junit.Test;

public class EventTest {

    @Test
    public void events_roundtrip() {

        assertRoundTrips(someEvent().predefine(Predefined.cardinality));
    }

    @Test
    public void changes_can_be_copied() {

        Event event = someEvent().predefined(true).guarded(true);

        Event updated = event.copy();

        assertEquals(event, updated);
    }

    @Test
    public void changes_are_controlled() {

        Event event = someEvent().guarded(true);
        Event original = event.copy();

        Event updated = event.copy();

        updated.managed(!original.managed());
        event.updateWith(updated);
        assertEquals(original, event);

        updated.managedTypes(listOf("any"));
        event.updateWith(updated);
        assertEquals(original, event);

        updated.managedExpression(someTagExpession(eventType));
        event.updateWith(updated);
        assertEquals(original, event);

        updated.tags(setOf("anytag"));
        event.updateWith(updated);
        assertEquals(original, event);

        updated.state(Event.State.active);
        event.updateWith(updated);
        assertEquals(original, event);

        // some changes are allowed

        updated.name().inDefaultLanguage("changed");

        event.updateWith(updated);

        assertNotEquals(original, event);

    }

    @Test
    public void can_change_guarded_along_with_other_properties() {

        Event event = someEvent().guarded(true);
        Event updated = event.copy();

        updated.guarded(false);
        updated.state(active);

        event.updateWith(updated);

        assertFalse(event.guarded());
        assertEquals(event.state(), updated.state());
    }

    @Test
    public void cannot_change_predefined_properties() {

        Event event = someEvent().guarded(true).predefine(guarded);
        Event updated = event.copy();

        updated.guarded(false);

        event.updateWith(updated);

        assertTrue(event.guarded());
    }

    @Test
    public void can_change_unguarded_predefined() {

        Event event = someEvent().predefined(true).guarded(false).state(inactive);
        Event updated = event.copy();

       updated.guarded(true);

        event.updateWith(updated);
        assertTrue(event.guarded());

        updated.guarded(false);
        event.state(active);
      
        event.updateWith(updated);

        assertFalse(event.guarded());
    }


    @Test
    public void managed_types_requires_a_metaevent() {

        Event event = someEvent().type("some").managedTypes(listOf("some"));

        List<String> errors = event.validate();
      
       assertFalse(errors.isEmpty());
    }

}
