package org.iotc.emaris.test.event;

import static org.iotc.emaris.event.Event.State.active;
import static org.iotc.emaris.event.Event.State.inactive;
import static org.iotc.emaris.event.data.EventFilter.filter;
import static org.iotc.emaris.event.data.EventFilter.withNoConditions;
import static org.iotc.emaris.test.event.Fixture.someEvent;
import static org.iotc.emaris.test.event.Testmate.stage;
import static org.iotc.emaris.test.event.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.Fixture.someMultilingual;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.EventDao;
import org.iotc.emaris.event.data.EventService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class EventDataTest extends Testbase {

    @Inject
    EventService service;

    @Inject
    EventDao dao;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        Event added;
        Event updated;
        Event removed;

        void onAdd(@Observes @Added Event event) {
            this.added = event;
        }

        void onUpdate(@Observes @Updated Event event) {
            this.updated = event;
        }

        void onRemove(@Observes @Removed Event event) {
            this.removed = event;
        }
    }

    @Test
    public void can_add_and_retrieve_events() {

        Event event = someEvent();

        stageDepsOf(event);

        List<Event> events = dao.all(withNoConditions());

        assertFalse(events.contains(event));

        service.add(event);

        events = dao.all(withNoConditions());

        assertTrue(events.contains(event));

        Event retrieved = dao.get(event.id());

        assertEquals(event, retrieved);

    }

    @Test
    public void can_observe_event_creation() {

        Event event = stage(someEvent());
        assertEquals(event, observed.added());

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_events() {

        Event event = stage(someEvent());

        service.add(event);

    }

    @Test(expected = ValidationException.class)
    public void cannot_add_invalid_events() {

        Event event = someEvent();

        event.name().inDefaultLanguage(null);

        stage(event);

    }

    @Test
    public void stages_events_only_if_they_dont_exist_already() {

        Event event = stage(someEvent());

        Event changed = someEvent().id(event.id());

        stageDepsOf(changed);

        dao.add(changed, true);

        Event retrieved = dao.get(event.id());

        assertEquals(event, retrieved);

    }

    @Test
    public void can_retrieve_all_active_events() {

        Event active = someEvent().state(Event.State.active);
        Event inactive = someEvent().state(Event.State.inactive);

        stage(active);
        stage(inactive);

        List<Event> events = dao.all(filter().state(Event.State.active));

        assertTrue(events.contains(active));
        assertFalse(events.contains(inactive));

    }

    @Test
    public void can_update_events_without_side_effects_and_observe_it() {

        Event event = stage(someEvent()); // this we change
        Event anotherEvent = stage(someEvent()); // this we dont touch

        List<Event> events = dao.all(withNoConditions());

        event.name(someMultilingual("changed"));

        assertFalse(events.contains(event));

        service.update(event);

        events = dao.all(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(events.containsAll(setOf(event, anotherEvent)));

        assertEquals(event, observed.updated());
    }

    @Test
    public void cannot_remove_active_guarded_or_predefined_events() {

        try {
            Event event = stage(someEvent().state(active));
            service.remove(event.id());
            fail();
        } catch (ValidationException expected) {
        }

        try {
            Event event = stage(someEvent().predefined(true));
            service.remove(event.id());
            fail();
        } catch (ValidationException expected) {
        }

        try {
            Event event = stage(someEvent().guarded(true));
            service.remove(event.id());
            fail();
        } catch (ValidationException expected) {
        }
    }

    @Test
    public void can_remove_inactive_events_without_side_effects_and_observe_it() {

        Event event = someEvent();

        event.state(inactive);

        stage(event);

        Event otherEvent = stage(someEvent());

        service.remove(event.id());

        List<Event> events = dao.all(withNoConditions());

        assertFalse(events.contains(event));
        assertTrue(events.contains(otherEvent));

        assertEquals(event, observed.removed());

    }
}
