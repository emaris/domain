package org.iotc.emaris.test.event;

import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;
import static test.apprise.backend.iam.tag.Testmate.stageTagsOf;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.EventDao;
import org.iotc.emaris.event.data.EventService;

import apprise.backend.data.Events.DatabaseReady;;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes @Priority(LIBRARY_BEFORE) DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    EventService service;

    @Inject
    EventDao dao;

    public static Event stageDepsOf(Event event) {

        stageTagsOf(event);

        return event;

    }

    public static Event stage(Event event) {

        if (_this.dao.lookup(event.id()).isPresent())
            return event;

        stageDepsOf(event);

        return _this.service.add(event);

    }

}
