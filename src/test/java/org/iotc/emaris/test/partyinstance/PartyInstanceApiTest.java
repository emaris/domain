package org.iotc.emaris.test.partyinstance;

import static org.iotc.emaris.partyinstance.api.ApiConstants.byref;
import static org.iotc.emaris.partyinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.partyinstance.api.ApiConstants.partyinstanceapi;
import static org.iotc.emaris.partyinstance.api.ApiConstants.remove;
import static org.iotc.emaris.partyinstance.api.ApiConstants.search;
import static org.iotc.emaris.partyinstance.data.PartyInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.partyinstance.Fixture.somePartyInstance;
import static org.iotc.emaris.test.partyinstance.Testmate.stage;
import static org.iotc.emaris.test.partyinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.Q;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.partyinstance.PartyInstance;
import org.junit.Test;

import test.apprise.backend.Testbase;

public class PartyInstanceApiTest extends Testbase {

    @Test
    public void can_add_retrieve_update_and_remove_instances() {

        PartyInstance instance = somePartyInstance();

        stageDepsOf(instance);

        Response response = at(partyinstanceapi).post(jsonOf(listOf(instance)));

        assertSuccessful(response);

        PartyInstance fetched = at(partyinstanceapi, instance.id()).get(PartyInstance.class);

        assertEquals(instance, fetched);

        List<PartyInstance> instances = at(partyinstanceapi, search).post(jsonOf(withNoConditions()))
                .readEntity(collofinstances);

        assertTrue(instances.contains(instance));

        // some changes.
        instance.properties().set(String.class, "changed").withDefault();

        response = at(partyinstanceapi, instance.id()).put(jsonOf(instance));

        assertSuccessful(response);

        fetched = at(partyinstanceapi, instance.id()).get(PartyInstance.class);

        assertEquals(instance, fetched);

        response = at(partyinstanceapi, instance.id()).delete();

        assertSuccessful(response);

        response = at(partyinstanceapi, instance.id()).get();

        assertFailed(response);

    }

    @Test
    public void can_remove_instances_in_bulk() {

        PartyInstance instance1 = somePartyInstance();
        PartyInstance instance2 = somePartyInstance();
        PartyInstance instance3 = somePartyInstance();

        stage(instance1, instance2, instance3);

        Response response = at(partyinstanceapi, remove).post(jsonOf(listOf(instance1.id(), instance2.id())));

        assertSuccessful(response);

        List<PartyInstance> instances = at(partyinstanceapi).get(collofinstances);

        assertFalse(instances.contains(instance1));
        assertFalse(instances.contains(instance2));
        assertTrue(instances.contains(instance3));

    }

    @Test
    public void can_retrieve_instances_byref() {

        PartyInstance instance = somePartyInstance();

        stage(instance);

        PartyInstance retrieved = at(partyinstanceapi, instance.ref(), Q(byref, true)).get(PartyInstance.class);

        assertEquals(instance, retrieved);
    }

    @Test
    public void can_retrieve_instance_samples() {

        PartyInstance instance = somePartyInstance();

        stage(instance);

        List<PartyInstance> retrieved = at(partyinstanceapi).get(collofinstances);

        assertTrue(retrieved.contains(instance));
    }

    @Test
    public void can_retrieve_instances_directly_by_campaign() {

        PartyInstance instance1 = somePartyInstance();
        PartyInstance instance2 = somePartyInstance();

        stage(instance1,instance2);

        List<PartyInstance> retrieved = at(partyinstanceapi, campaign, instance1.campaign()).get(collofinstances);

        assertTrue(retrieved.contains(instance1));
        assertFalse(retrieved.contains(instance2));
    }

    /////// helpers

    static GenericType<List<PartyInstance>> collofinstances = new GenericType<List<PartyInstance>>() {
    };
}
