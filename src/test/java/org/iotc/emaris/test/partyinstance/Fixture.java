package org.iotc.emaris.test.partyinstance;

import static apprise.backend.iam.Constants.tenantPrefix;
import static apprise.backend.iam.Constants.tenantType;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.partyinstance.Constants.partyinstancePrefix;
import static test.apprise.backend.Fixture.someTag;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.partyinstance.PartyInstance;

import apprise.backend.model.Bag;
import apprise.backend.model.Id;

public class Fixture {

    public static PartyInstance somePartyInstance() {

        return new PartyInstance()
                .id(Id.mint(partyinstancePrefix))
                .source(Id.mint(tenantPrefix))
                .campaign(Id.mint(campaignPrefix))
                .lineage(new CampaignInstance.InstanceRef(Id.mint(campaignPrefix), Id.mint(tenantPrefix)))
                .tags(someTag(tenantType), someTag().type(tenantType))
                .properties(new Bag().set("prop","test").withDefault());

    }
    
}
