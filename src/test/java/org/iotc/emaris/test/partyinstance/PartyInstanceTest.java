package org.iotc.emaris.test.partyinstance;

import static org.iotc.emaris.test.partyinstance.Fixture.somePartyInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.setOf;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.junit.Test;

import apprise.backend.validation.Exceptions.ValidationException;

public class PartyInstanceTest {

    @Test
    public void partyinstances_roundtrip() {

        assertRoundTrips(somePartyInstance());
    }

    @Test
    public void changes_can_be_copied() {

        PartyInstance partyinstance = somePartyInstance();

        PartyInstance updated = partyinstance.copy();

        assertEquals(partyinstance.campaign(), updated.campaign());
    }

    @Test
    public void changes_are_controlled() {

        PartyInstance instance = somePartyInstance();
        PartyInstance original = instance.copy();
        PartyInstance updated = instance.copy();

        updated.id("newid");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.campaign("newcampaign");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.source("newsource");
        instance.updateWith(updated);
        assertEquals(original, instance);

        // some changes are accepted

        updated.lineage(new CampaignInstance.InstanceRef("campaign", "source"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);

        updated.tags(setOf("anytag"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);


    }


    @Test
    public void some_fields_are_mandatory() {

        try {
            somePartyInstance().id(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}

        try {
            somePartyInstance().campaign(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}

    }

}
