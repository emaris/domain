package org.iotc.emaris.test.partyinstance;

import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.partyinstance.Fixture.somePartyInstance;
import static test.apprise.backend.iam.Fixture.someTenant;

import java.util.List;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.iotc.emaris.partyinstance.data.PartyInstanceDao;
import org.iotc.emaris.partyinstance.data.PartyInstanceService;

import apprise.backend.data.Events.DatabaseReady;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    PartyInstanceService service;

    @Inject
    PartyInstanceDao dao;

    public static PartyInstance stageDepsOf(PartyInstance instance) {

        test.apprise.backend.iam.tag.Testmate.stageTagsOf(instance);
        org.iotc.emaris.test.campaign.Testmate.stage(someCampaign().id(instance.campaign()));
        test.apprise.backend.iam.admin.Testmate.stage(someTenant().id(instance.source()));

        if (instance.lineage().isPresent()) {

            InstanceRef ref = instance.lineage().get();

            stage(somePartyInstance()
                    .campaign(ref.campaign())
                    .source(ref.source())
                    .lineage(null)
            );
        }

        return instance;
    }

    public static List<PartyInstance> stage(PartyInstance ... instances) {

        List<PartyInstance> newinstances = Stream.of(instances)
                .filter(i -> _this.dao.lookup(i.id()).isEmpty())
                .peek(Testmate::stageDepsOf)
                .collect(toList());

        return _this.service.add(newinstances);

    }
}
