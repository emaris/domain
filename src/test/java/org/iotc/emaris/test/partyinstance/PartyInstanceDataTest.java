package org.iotc.emaris.test.partyinstance;

import static org.iotc.emaris.partyinstance.data.PartyInstanceFilter.filter;
import static org.iotc.emaris.partyinstance.data.PartyInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.partyinstance.Fixture.somePartyInstance;
import static org.iotc.emaris.test.partyinstance.Testmate.stage;
import static org.iotc.emaris.test.partyinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.iotc.emaris.partyinstance.data.PartyInstanceDao;
import org.iotc.emaris.partyinstance.data.PartyInstanceService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class PartyInstanceDataTest extends Testbase {

    @Inject
    PartyInstanceService service;

    @Inject
    PartyInstanceDao dao;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        List<PartyInstance> addedBulk;
        PartyInstance updated;
        RemovedInstanceEvents.Event<PartyInstance> removedEvent;

        void onAdd(@Observes @Added List<PartyInstance> instances) {
            this.addedBulk = instances;
        }

        void onUpdate(@Observes @Updated PartyInstance instance) {
            this.updated = instance;
        }

        void onRemove(@Observes RemovedInstanceEvents.Event<PartyInstance> event) {
            this.removedEvent = event;
        }
    }

    @Test
    public void instances_can_added_and_retrieved() {

        PartyInstance instance1 = somePartyInstance();
        PartyInstance instance2 = somePartyInstance();

        stageDepsOf(instance1);
        stageDepsOf(instance2);

        service.add(listOf(instance1, instance2));

        assertEquals(instance1, dao.get(instance1.id()));
        assertEquals(instance2, dao.get(instance2.id()));

    }

    @Test
    public void instances_can_retrieve_instances_by_ref() {

        PartyInstance instance = somePartyInstance();

        stage(instance);

        assertEquals(instance, dao.get(instance.ref()));

    }

    @Test
    public void can_observe_requirement_creation() {

        List<PartyInstance> instances = stage(somePartyInstance());

        assertEquals(instances, observed.addedBulk());

    }

    @Test
    public void cannot_add_invalid_instances() {

        try {
            stage(somePartyInstance().campaign(null));
            fail();
        } catch (ValidationException expected) {
        }

    }

    @Test
    public void cannot_add_instances_outside_campaign() {

        try {
            PartyInstance instance = somePartyInstance();
            stageDepsOf(instance);
            service.add(listOf(instance.campaign("another")));
            fail();
        } catch (Exception expected) {
        }

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_instances() {

        PartyInstance instance = somePartyInstance();

        stage(instance);

        PartyInstance duplicate = somePartyInstance().id(instance.id());

        service.add(listOf(duplicate));

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_instances_with_same_ref() {

        PartyInstance instance = somePartyInstance();

        stage(instance);

        PartyInstance duplicate = somePartyInstance().campaign(instance.campaign()).source(instance.source());

        service.add(listOf(duplicate));

    }


    @Test
    public void can_retrieve_instances_in_campaign() {

        PartyInstance instance1 = somePartyInstance();
        PartyInstance instance2 = somePartyInstance();

        stage(instance1, instance2);

        List<PartyInstance> instances = dao.allMatching(filter().campaign(instance1.campaign()));

        assertTrue(instances.contains(instance1));
        assertFalse(instances.contains(instance2));

    }

    @Test
    public void can_update_instances_without_side_effects_and_observe_it() {

        PartyInstance instance1 = somePartyInstance(); // this we change
        PartyInstance instance2 = somePartyInstance(); // this we dont touch

        stage(instance1, instance2);

        List<PartyInstance> instances = dao.allMatching(withNoConditions());

        instance1.properties().set(String.class,"changed").withDefault();

        assertFalse(instances.contains(instance1));

        service.update(instance1);

        instances = dao.allMatching(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(instances.containsAll(setOf(instance1, instance2)));

        assertEquals(instance1, observed.updated());
    }


    @Test
    public void can_remove_instances_without_side_effects_and_observe_it() {

        PartyInstance partyinstance = somePartyInstance();
        PartyInstance otherPartyInstance = somePartyInstance();

        stage(partyinstance,otherPartyInstance);
        
        service.remove(partyinstance.id());

        List<PartyInstance> partyinstances = dao.allMatching(withNoConditions());

        assertFalse(partyinstances.contains(partyinstance));
        assertTrue(partyinstances.contains(otherPartyInstance));

        assertEquals(listOf(partyinstance), observed.removedEvent().instances());

    }


    @Test
    public void can_remove_multiple_instances_without_side_effects_and_observe_it() {

        PartyInstance partyinstance = somePartyInstance();
        PartyInstance otherPartyInstance = somePartyInstance();
        PartyInstance yetAnotherPartyInstance = somePartyInstance();

        stage(partyinstance,otherPartyInstance,yetAnotherPartyInstance);
        
        service.remove(listOf(partyinstance.id(),otherPartyInstance.id()));

        List<PartyInstance> partyinstances = dao.allMatching(withNoConditions());

        assertFalse(partyinstances.containsAll(listOf(partyinstance,otherPartyInstance)));
        assertTrue(partyinstances.contains(yetAnotherPartyInstance));

        assertEquals(listOf(partyinstance,otherPartyInstance), observed.removedEvent().instances());

    }
}

