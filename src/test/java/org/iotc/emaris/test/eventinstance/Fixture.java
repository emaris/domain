package org.iotc.emaris.test.eventinstance;

import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.campaign.Constants.campaignType;
import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.eventinstance.Constants.eventinstancePrefix;
import static org.iotc.emaris.eventinstance.EventDate.RelativeDate.Direction.after;
import static org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod.Unit.days;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static test.apprise.backend.Fixture.someTag;

import java.time.Duration;
import java.time.OffsetDateTime;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod;
import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.model.Bag;
import apprise.backend.model.Id;

public class Fixture {

    public static EventInstance someEventInstance() {

        return new EventInstance()
                .id(Id.mint(eventinstancePrefix))
                .source(Id.mint(eventPrefix))
                .campaign(Id.mint(campaignPrefix))
                .lineage(new CampaignInstance.InstanceRef(Id.mint(campaignPrefix), Id.mint(eventPrefix)))
                .tags(someTag(eventType))
                .notifiedOn(OffsetDateTime.now().minus(Duration.ofDays(2)))
                .properties(new Bag().set("prop","test").withDefault())

                .type(campaignType)
                .target(someCampaign().id())
                .date(EventDate.relativeTo(Id.mint(eventinstancePrefix))
                        .period(new RelativePeriod().direction(after).number(10).unit(days)));

    }

}
