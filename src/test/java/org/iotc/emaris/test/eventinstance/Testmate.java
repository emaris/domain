package org.iotc.emaris.test.eventinstance;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.event.Fixture.someEvent;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static test.apprise.backend.Testmate.listOf;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.eventinstance.data.EventInstanceService;

import apprise.backend.data.Events.DatabaseReady;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    EventInstanceService service;

    @Inject
    EventInstanceDao dao;

    public static EventInstance stageDepsOf(EventInstance instance) {

        test.apprise.backend.iam.tag.Testmate.stageTagsOf(instance);
        org.iotc.emaris.test.campaign.Testmate.stage(someCampaign().id(instance.campaign()));
        org.iotc.emaris.test.event.Testmate.stage(someEvent().id(instance.source()));

        if (instance.lineage().isPresent()) {

            InstanceRef ref = instance.lineage().get();

            stage(someEventInstance()
                    .campaign(ref.campaign())
                    .source(ref.source())
                    .lineage(null)
                    .tags(emptyList())
                    .date(EventDate.absolute(OffsetDateTime.now()))
            );
        }


        if (instance.date().isPresent() && instance.date().get().isRelative()){

            stage(someEventInstance()
                    .id(instance.date().get().asRelative().target())
                    .campaign(instance.campaign())
                    .lineage(null)
                    .tags(emptyList())
                    .date(EventDate.absolute(OffsetDateTime.now()))
            );
        }

        return instance;
    }

    public static List<EventInstance> stage(EventInstance ... instances) {

        return Stream.of(instances)
                .filter(i -> _this.dao.lookup(i.id()).isEmpty())
                .peek(Testmate::stageDepsOf)
                .peek(i->_this.service.add(listOf(i)))
                .collect(toList());

     

    }
}
