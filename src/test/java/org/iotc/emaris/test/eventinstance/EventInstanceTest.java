package org.iotc.emaris.test.eventinstance;

import static org.iotc.emaris.eventinstance.EventDate.absolute;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.setOf;

import java.time.OffsetDateTime;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.eventinstance.EventInstance;
import org.junit.Test;

import apprise.backend.validation.Exceptions.ValidationException;

public class EventInstanceTest {

    @Test
    public void eventinstances_roundtrip() {

        assertRoundTrips(someEventInstance());
        assertRoundTrips(someEventInstance().date(absolute(OffsetDateTime.now())));
    }

    @Test
    public void changes_can_be_copied() {

        EventInstance eventinstance = someEventInstance();

        EventInstance updated = eventinstance.copy();

        assertEquals(eventinstance.campaign(), updated.campaign());
    }

    @Test
    public void changes_are_controlled() {

        EventInstance instance = someEventInstance();
        EventInstance original = instance.copy();
        EventInstance updated = instance.copy();

        updated.id("newid");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.campaign("newcampaign");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.source("newsource");
        instance.updateWith(updated);
        assertEquals(original, instance);

        // some changes are accepted

        updated.lineage(new CampaignInstance.InstanceRef("campaign", "source"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);

        updated.tags(setOf("anytag"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);


    }


    @Test
    public void some_fields_are_mandatory() {

        try {
            someEventInstance().id(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}

        try {
            someEventInstance().campaign(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}
    }

}
