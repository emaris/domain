package org.iotc.emaris.test.eventinstance;

import static java.time.OffsetDateTime.now;
import static org.iotc.emaris.campaign.Constants.campaignType;
import static org.iotc.emaris.campaign.stage.StagedData.campaignEnd;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.event.stage.StagedData.reminder;
import static org.iotc.emaris.eventinstance.EventDate.absolute;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.requirement.stage.StagedData.requirement_deadline;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.campaign.Testmate.stage;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.iotc.emaris.test.product.Testmate.stage;
import static org.iotc.emaris.test.requirement.Fixture.someRequirement;
import static org.iotc.emaris.test.requirement.Testmate.stage;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate.Direction;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate.RelativePeriod.Unit;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.message.MessageResolver;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.requirement.Requirement;
import org.junit.Test;

import apprise.backend.message.model.Message;
import test.apprise.backend.Testbase;

public class EventInstanceMessageTest extends Testbase {

    @Inject
    MessageResolver.Instance resolver;

    @Test
    public void can_resolve_campaign_events() {

        Campaign campaign = stage(someCampaign());

        EventInstance start = someEventInstance().campaign(campaign.id()).type(campaignType).source(campaignStart.id());
        EventInstance end = someEventInstance().campaign(campaign.id()).type(campaignType).source(campaignEnd.id());

        stage(start,end);

        Message message = resolver.messageFor(start);

        show(message);

        assertNotNull(message);

        message = resolver.messageFor(end);

        show(message);

        assertNotNull(message);
    }


    @Test
    public void can_resolve_deadlines() {

        Campaign campaign = stage(someCampaign());
        Requirement requirement = stage(someRequirement());
        Product product = stage(someProduct());

        EventInstance rDeadline = someEventInstance().campaign(campaign.id()).type(requirementType).source(requirement_deadline.id()).target(requirement.id());
        EventInstance pDeadline = someEventInstance().campaign(campaign.id()).type(productType).source(requirement_deadline.id()).target(product.id());
       
        stage(rDeadline,pDeadline);

        Message message = resolver.messageFor(rDeadline);

        show(message);

        assertNotNull(message);

        message = resolver.messageFor(pDeadline);

        show(message);

        assertNotNull(message);
    }

    @Test
    public void can_resolve_reminders() {

        Campaign campaign = stage(someCampaign());
        Requirement requirement = stage(someRequirement());
       
        EventInstance rDeadline = someEventInstance().campaign(campaign.id()).type(requirementType).source(requirement_deadline.id()).target(requirement.id()).date(absolute(now()));
        EventInstance early = someEventInstance().campaign(campaign.id()).type(eventType).source(reminder.id()).target(rDeadline.id()).date(absolute(now().plusDays(2).plusHours(5)));
        EventInstance late = someEventInstance().campaign(campaign.id()).type(eventType).source(reminder.id()).target(rDeadline.id()).date(EventDate.relativeTo(rDeadline).period(new RelativePeriod(1,Unit.days,Direction.before)));
       
        stage(rDeadline,early,late);

        Message message = resolver.messageFor(early);

        show(message);

        assertNotNull(message);

        message = resolver.messageFor(late);

        show(message);

        assertNotNull(message);
    }
    

  
}
