package org.iotc.emaris.test.eventinstance;

import static org.iotc.emaris.eventinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.eventinstance.api.ApiConstants.eventinstanceapi;
import static org.iotc.emaris.eventinstance.api.ApiConstants.remove;
import static org.iotc.emaris.eventinstance.api.ApiConstants.search;
import static org.iotc.emaris.eventinstance.data.EventInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.iotc.emaris.test.eventinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.eventinstance.EventDate.AbsoluteDate;
import org.iotc.emaris.eventinstance.EventInstance;
import org.junit.Test;

import test.apprise.backend.Testbase;

public class EventInstanceApiTest extends Testbase {

    @Test
    public void can_add_retrieve_update_and_remove_instances() {

        EventInstance instance = someEventInstance();

        stageDepsOf(instance);

        Response response = at(eventinstanceapi).post(jsonOf(listOf(instance)));

        assertSuccessful(response);

        EventInstance fetched = at(eventinstanceapi, instance.id()).get(EventInstance.class);

        assertEquals(instance, fetched);

        List<EventInstance> instances = at(eventinstanceapi, search).post(jsonOf(withNoConditions()))
                .readEntity(collofinstances);

        assertTrue(instances.contains(instance));

        // some changes.
        instance.properties().set(String.class, "changed").withDefault();

        response = at(eventinstanceapi, instance.id()).put(jsonOf(instance));

        assertSuccessful(response);

        fetched = at(eventinstanceapi, instance.id()).get(EventInstance.class);

        assertEquals(instance, fetched);

        response = at(eventinstanceapi, instance.id()).delete();

        assertSuccessful(response);

        response = at(eventinstanceapi, instance.id()).get();

        assertFailed(response);

    }

    @Test
    public void can_add_retrieve_update_and_remove_instances_with_no_absolute_date() {

        EventInstance instance = someEventInstance().date(new AbsoluteDate().none(true));

        stageDepsOf(instance);

        Response response = at(eventinstanceapi).post(jsonOf(listOf(instance)));

        assertSuccessful(response);

        EventInstance fetched = at(eventinstanceapi, instance.id()).get(EventInstance.class);

        assertEquals(instance, fetched);

        List<EventInstance> instances = at(eventinstanceapi, search).post(jsonOf(withNoConditions()))
                .readEntity(collofinstances);

        assertTrue(instances.contains(instance));

        // some changes.
        instance.properties().set(String.class, "changed").withDefault();

        response = at(eventinstanceapi, instance.id()).put(jsonOf(instance));

        assertSuccessful(response);

        fetched = at(eventinstanceapi, instance.id()).get(EventInstance.class);

        assertEquals(instance, fetched);

        response = at(eventinstanceapi, instance.id()).delete();

        assertSuccessful(response);

        response = at(eventinstanceapi, instance.id()).get();

        assertFailed(response);

    }

    @Test
    public void can_remove_instances_in_bulk() {

        EventInstance instance1 = someEventInstance();
        EventInstance instance2 = someEventInstance();
        EventInstance instance3 = someEventInstance();

        stage(instance1, instance2, instance3);

        Response response = at(eventinstanceapi, remove).post(jsonOf(listOf(instance1.id(), instance2.id())));

        assertSuccessful(response);

        List<EventInstance> instances = at(eventinstanceapi).get(collofinstances);

        assertFalse(instances.contains(instance1));
        assertFalse(instances.contains(instance2));
        assertTrue(instances.contains(instance3));

    }

    @Test
    public void can_retrieve_instance_samples() {

        EventInstance instance = someEventInstance();

        stage(instance);

        List<EventInstance> retrieved = at(eventinstanceapi).get(collofinstances);

        assertTrue(retrieved.contains(instance));
    }

    @Test
    public void can_retrieve_instances_directly_by_campaign() {

        EventInstance instance1 = someEventInstance();
        EventInstance instance2 = someEventInstance();

        stage(instance1, instance2);

        List<EventInstance> retrieved = at(eventinstanceapi, campaign, instance1.campaign()).get(collofinstances);

        assertTrue(retrieved.contains(instance1));
        assertFalse(retrieved.contains(instance2));
    }

    /////// helpers

    static GenericType<List<EventInstance>> collofinstances = new GenericType<List<EventInstance>>() {
    };
}
