package org.iotc.emaris.test.eventinstance;

import static java.time.OffsetDateTime.now;
import static org.iotc.emaris.campaign.Constants.campaignType;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;
import static org.iotc.emaris.eventinstance.EventDate.absolute;
import static org.iotc.emaris.eventinstance.data.EventInstanceFilter.filter;
import static org.iotc.emaris.eventinstance.data.EventInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.eventinstance.Fixture.someEventInstance;
import static org.iotc.emaris.test.eventinstance.Testmate.stage;
import static org.iotc.emaris.test.eventinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.eventinstance.EventDate;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.api.Dtos.DetectionOutcome;
import org.iotc.emaris.eventinstance.data.Changes;
import org.iotc.emaris.eventinstance.data.DueDateService;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.eventinstance.data.EventInstanceFilter;
import org.iotc.emaris.eventinstance.data.EventInstanceService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class EventInstanceDataTest extends Testbase {

    @Inject
    EventInstanceService service;

    @Inject
    DueDateService dueDateService;

    @Inject
    EventInstanceDao dao;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        List<EventInstance> addedBulk;
        EventInstance updated;
        RemovedInstanceEvents.Event<EventInstance> removedEvent;

        void onAdd(@Observes @Added List<EventInstance> instances) {
            this.addedBulk = instances;
        }

        void onUpdate(@Observes @Updated EventInstance instance) {
            this.updated = instance;
        }

        void onRemove(@Observes RemovedInstanceEvents.Event<EventInstance> event) {
            this.removedEvent = event;
        }
    }

    @Test
    public void instances_can_added_and_retrieved() {

        EventInstance instance1 = someEventInstance();
        EventInstance instance2 = someEventInstance();

        stageDepsOf(instance1);
        stageDepsOf(instance2);

        service.add(listOf(instance1, instance2));

        assertEquals(instance1, dao.get(instance1.id()));
        assertEquals(instance2, dao.get(instance2.id()));

    }

    @Test
    public void can_observe_requirement_creation() {

        List<EventInstance> instances = stage(someEventInstance());

        assertEquals(instances, observed.addedBulk());

    }

    @Test
    public void cannot_add_invalid_instances() {

        try {
            stage(someEventInstance().campaign(null));
            fail();
        } catch (ValidationException expected) {
        }

    }

    @Test
    public void cannot_add_instances_with_unknown_campaign() {

        try {
            EventInstance instance = someEventInstance();
            stageDepsOf(instance);
            service.add(listOf(instance.campaign("another")));
            //fail();
        } catch (Exception expected) {
        }

    }

   @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_instances() {

        EventInstance instance = someEventInstance();

        stage(instance);

        EventInstance duplicate = someEventInstance().id(instance.id());

        service.add(listOf(duplicate));

    }

    @Test
    public void can_retrieve_instances_in_campaign() {

        EventInstance instance1 = someEventInstance();
        EventInstance instance2 = someEventInstance();

        stage(instance1, instance2);

        List<EventInstance> instances = dao.allMatching(filter().campaign(instance1.campaign()));

        assertTrue(instances.contains(instance1));
        assertFalse(instances.contains(instance2));

    }

    @Test
    public void can_update_instances_without_side_effects_and_observe_it() {

        EventInstance instance1 = someEventInstance(); // this we change
        EventInstance instance2 = someEventInstance(); // this we dont touch

        stage(instance1, instance2);

        List<EventInstance> instances = dao.allMatching(withNoConditions());

        instance1.properties().set(String.class, "changed").withDefault();

        assertFalse(instances.contains(instance1));

        service.update(instance1);

        instances = dao.allMatching(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(instances.containsAll(setOf(instance1, instance2)));

        assertEquals(instance1, observed.updated());
    }

    @Test
    public void can_add_and_update_amd_removedinstances_at_once() {

        EventInstance updatableInstance = someEventInstance();
        EventInstance newinstance = someEventInstance();
        EventInstance removableInstance = someEventInstance();
        EventInstance otherinstance = someEventInstance();

        stage(updatableInstance, removableInstance, otherinstance);

        stageDepsOf(newinstance);

        List<EventInstance> instances = dao.allMatching(withNoConditions());

        updatableInstance.date(EventDate.absolute(OffsetDateTime.now()));

        List<EventInstance> processed = service.update(new Changes().added(listOf(newinstance))
                .updated(listOf(updatableInstance)).removed(listOf(removableInstance.id())));

        assertTrue(processed.containsAll(setOf(updatableInstance, newinstance)));
        assertFalse(processed.contains(removableInstance));

        instances = dao.allMatching(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(instances.containsAll(setOf(updatableInstance, newinstance, otherinstance)));

    }

    @Test
    public void can_remove_instances_without_side_effects_and_observe_it() {

        EventInstance eventinstance = someEventInstance();
        EventInstance otherEventInstance = someEventInstance();

        stage(eventinstance, otherEventInstance);

        service.remove(eventinstance.id());

        List<EventInstance> eventinstances = dao.allMatching(withNoConditions());

        assertFalse(eventinstances.contains(eventinstance));
        assertTrue(eventinstances.contains(otherEventInstance));

        assertEquals(listOf(eventinstance), observed.removedEvent().instances());

    }

    @Test
    public void can_remove_multiple_instances_without_side_effects_and_observe_it() {

        EventInstance eventinstance = someEventInstance();
        EventInstance otherEventInstance = someEventInstance();
        EventInstance yetAnotherEventInstance = someEventInstance();

        stage(eventinstance, otherEventInstance, yetAnotherEventInstance);

        service.remove(listOf(eventinstance.id(), otherEventInstance.id()));

        List<EventInstance> eventinstances = dao.allMatching(withNoConditions());

        assertFalse(eventinstances.containsAll(listOf(eventinstance, otherEventInstance)));
        assertTrue(eventinstances.contains(yetAnotherEventInstance));

        assertEquals(listOf(eventinstance, otherEventInstance), observed.removedEvent().instances());

    }

    @Test
    public void can_detect_and_mark_due_instances_of_open_campaign_in_given_window() {

        // campaign start
        EventInstance start = someEventInstance().source(campaignStart.id())
                .type(campaignType)
                .date(absolute(now().minus(Duration.ofDays(10))));

        start.target(start.campaign());

        stage(start);
        
        // three instances in same campaign
        EventInstance instance1 = someEventInstance().campaign(start.campaign()).notifiedOn(null)
                .date(absolute(now().plus(Duration.ofMinutes(10))));
        EventInstance instance2 = someEventInstance().campaign(start.campaign()).notifiedOn(null)
                .date(absolute(now().minus(Duration.ofMinutes(1))));
        EventInstance instance3 = someEventInstance().campaign(start.campaign()).notifiedOn(null)
                .date(absolute(now().minus(Duration.ofMinutes(5))));

        stage(instance1, instance2, instance3);

        EventInstanceFilter request = EventInstanceFilter.filter().after(now().minus(Duration.ofMinutes(2)));

        DetectionOutcome outcome = dueDateService.detectAndNotify(request);

        assertEquals(1, outcome.detected());

        assertFalse(dao.get(instance1.id()).notifiedOn().isPresent());
        assertTrue(dao.get(instance2.id()).notifiedOn().isPresent());
        assertFalse(dao.get(instance3.id()).notifiedOn().isPresent());

        outcome = dueDateService.detectAndNotify(request);

        assertEquals(0, outcome.detected());

    }

    // @Test
    // public void can_compute_relative_dates_on_update() {

    //     EventInstance eventinstance = someEventInstance().date(absolute(OffsetDateTime.now()));
    //     EventInstance relative1 = someEventInstance().date(relativeTo(eventinstance).period(new RelativePeriod(1,Unit.months,Direction.after)));
    //     EventInstance relative2 = someEventInstance().date(relativeTo(eventinstance).period(new RelativePeriod(1,Unit.months,Direction.before)));
    //     EventInstance metarelative1 = someEventInstance().date(relativeTo(relative1).period(new RelativePeriod(1,Unit.months,Direction.before)));

    //     stage(eventinstance,relative1,relative2,metarelative1);

    //     service.update(relative1);
    // }
}
