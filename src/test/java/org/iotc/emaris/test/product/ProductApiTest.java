package org.iotc.emaris.test.product;

import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.iotc.emaris.product.Rights.manage_products;
import static org.iotc.emaris.product.api.ApiConstants.productapi;
import static org.iotc.emaris.product.Product.State.inactive;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.iotc.emaris.test.product.Testmate.stage;
import static org.iotc.emaris.test.product.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertForbidden;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;
import static test.apprise.backend.api.Apimate.statusOf;
import static test.apprise.backend.iam.Fixture.someUser;
import static test.apprise.backend.iam.admin.Testmate.stage;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.product.Product;
import org.junit.Test;

import apprise.backend.iam.LoginService;
import apprise.backend.iam.model.User;
import test.apprise.backend.Testbase;

public class ProductApiTest extends Testbase {

	@Inject
	LoginService service;

	@Test
	public void products_can_be_created_retrieved_updated_and_removed() {

		Product product = someProduct();

		stageDepsOf(product);

		Response response = at(productapi, product.id()).get();

		assertEquals(NOT_FOUND, statusOf(response));

		response = at(productapi).post(jsonOf(product));

		assertSuccessful(response);

		Product fetched = at(productapi, product.id()).get(Product.class);

		assertEquals(product, fetched);

		Collection<Product> products = at(productapi).get(collofproducts);

		assertTrue(products.stream().map(p->p.id()).collect(toList()).contains(product.id()));

		// some changes.
		product.description().inDefaultLanguage("changed");
		product.lifecycle().state(inactive); // lets us delete it later.

		response = at(productapi, product.id()).put(jsonOf(product));

		assertSuccessful(response);

		fetched = at(productapi, product.id()).get(Product.class);

		// properties are lazily loaded from list, so erase them before comparisons
		product.properties(null);
		fetched.properties(null);

		assertEquals(product, fetched);

		response = at(productapi, product.id()).delete();

		assertSuccessful(response);

		response = at(productapi, product.id()).get();

		assertFailed(response);
	}

	@Test
	public void creating_products_requires_privileges() {

		Product product = someProduct();

		stageDepsOf(product);

		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(productapi).post(jsonOf(product));
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_products));
		
		service.login(user);
		
		response = at(productapi).post(jsonOf(product));
		
		assertSuccessful(response);
		
	}

	@Test
	public void updating_products_requires_privileges() {

		Product product = stage(someProduct());

		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(productapi,product.id()).put(jsonOf(product));
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_products.on(product.id())));
		
		service.login(user);
		
		response = at(productapi,product.id()).put(jsonOf(product));
		
		assertSuccessful(response);
		
	}


	@Test
	public void removing_products_requires_privileges() {
		
		Product product = stage(someProduct().state(inactive));
		
		User user = stage(someUser());
		
		service.login(user);
		
		Response response = at(productapi,product.id()).delete();
		
		assertForbidden(response);
		
		user = stage(someUser().grant(manage_products.on(product.id())));
		
		service.login(user);
		
		response = at(productapi, product.id()).delete();
		
		assertSuccessful(response);
		
	}

	
	/////// helpers
	
	static GenericType<List<Product>> collofproducts = new GenericType<List<Product>>(){};
	
}
