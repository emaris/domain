package org.iotc.emaris.test.product;

import static org.iotc.emaris.product.data.ProductFilter.filter;
import static org.iotc.emaris.product.data.ProductFilter.withNoConditions;
import static org.iotc.emaris.product.Product.State.active;
import static org.iotc.emaris.product.Product.State.inactive;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.iotc.emaris.test.product.Testmate.stage;
import static org.iotc.emaris.test.product.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static test.apprise.backend.Fixture.someMultilingual;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.product.data.ProductService;
import org.iotc.emaris.product.Product;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class ProductDataTest extends Testbase {

    @Inject
    ProductDao dao;

    @Inject
    ProductService service;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        Product added;
        Product updated;
        Product removed;

        void onAdd(@Observes @Added Product product) {
            this.added = product;
        }

        void onUpdate(@Observes @Updated Product product) {
            this.updated = product;
        }

        void onRemove(@Observes @Removed Product product) {
            this.removed = product;
        }
    }

    @Test
    public void can_add_and_retrieve_products() {

        Product product = someProduct();

        stageDepsOf(product);

        List<Product> products = dao.all(withNoConditions());

        assertFalse(products.contains(product));

        service.add(product);

        products = dao.all(withNoConditions());

        assertTrue(products.contains(product));

        Product retrieved = dao.get(product.id());

        assertEquals(product, retrieved);

    }

    @Test
    public void can_observe_product_creation() {

        Product product = stage(someProduct());

        assertEquals(product, observed.added());

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_products() {

        Product product = stage(someProduct());

        service.add(product);

    }

    @Test(expected = ValidationException.class)
    public void cannot_add_invalid_products() {

        Product product = someProduct();

        product.name().inDefaultLanguage(null);

        stage(product);

    }

    @Test
    public void stages_products_only_if_they_dont_exist_already() {

        Product product = stage(someProduct());

        Product changed = someProduct().id(product.id());

        stageDepsOf(changed);

        dao.add(changed, true);

        Product retrieved = dao.get(product.id());

        assertEquals(product, retrieved);

    }

    @Test
    public void can_retrieve_all_active_products() {

        Product active = someProduct().state(Product.State.active);
        Product inactive = someProduct().state(Product.State.inactive);

        stage(active);
        stage(inactive);

        List<Product> products = dao.all(filter().state(Product.State.active));

        assertTrue(products.contains(active));
        assertFalse(products.contains(inactive));

    }

    @Test
    public void can_update_products_without_side_effects_and_observe_it() {

        Product product = stage(someProduct()); // this we change
        Product anotherProduct = stage(someProduct()); // this we dont touch

        List<Product> products = dao.all(withNoConditions());

        product.name(someMultilingual("changed"));

        assertFalse(products.contains(product));

        service.update(product);

        products = dao.all(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(products.containsAll(setOf(product, anotherProduct)));

        assertEquals(product, observed.updated());
    }

    @Test
    public void cannot_remove_active_guarded_or_predefined_products() {

        try {
            Product product = stage(someProduct().state(active));
            service.remove(product.id());
            fail();
        }
        catch(ValidationException expected) {}

        try {
            Product product = stage(someProduct().predefined(true));
            service.remove(product.id());
            fail();
        }
        catch(ValidationException expected) {}

        try {
            Product product = stage(someProduct().guarded(true));
            service.remove(product.id());
            fail();
        }
        catch(ValidationException expected) {}
    }

    @Test
    public void can_remove_inactive_products_without_side_effects_and_observe_it() {

        Product product = someProduct();

        product.state(inactive);

        stage(product);

        Product otherProduct = stage(someProduct());

        service.remove(product.id());

        List<Product> products = dao.all(withNoConditions());

        assertFalse(products.contains(product));
        assertTrue(products.contains(otherProduct));

        assertEquals(product.id(), observed.removed().id());

    }
}
