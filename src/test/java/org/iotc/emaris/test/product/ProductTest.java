package org.iotc.emaris.test.product;

import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static org.iotc.emaris.product.Product.Predefined.guarded;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.setOf;

import org.iotc.emaris.product.Product;
import org.junit.Test;

public class ProductTest {

    @Test
    public void products_roundtrip() {

        assertRoundTrips(someProduct());
    }

    @Test
    public void changes_can_be_copied() {

        Product product = someProduct().predefined(true).guarded(true);

        Product updated = product.copy();

        assertEquals(product, updated);
    }

    @Test
    public void changes_are_controlled() {

        Product product = someProduct().guarded(true);
        Product original = product.copy();
        Product updated = product.copy();

        updated.audience(someTagExpession(tenantType));
        product.updateWith(updated);
        assertEquals(original, product);

        updated.userProfile(someTagExpession(userType));
        product.updateWith(updated);
        assertEquals(original, product);

        updated.tags(setOf("anytag"));
        product.updateWith(updated);
        assertEquals(original, product);

        updated.state(Product.State.active);
        product.updateWith(updated);
        assertEquals(original, product);

        // some changes are allowed

        updated.name().inDefaultLanguage("changed");

        product.updateWith(updated);

        assertNotEquals(original, product);

    }

    @Test
    public void can_change_guarded_along_with_other_properties() {

        Product product = someProduct().guarded(true);
        Product updated = product.copy();

        updated.guarded(false);
        updated.state(Product.State.active);

        product.updateWith(updated);

        assertFalse(product.guarded());
        assertEquals(product.state(), updated.state());
    }

    @Test
    public void cannot_change_predefined_properties() {

        Product event = someProduct().guarded(true).predefine(guarded);
        Product updated = event.copy();

        updated.guarded(false);

        event.updateWith(updated);

        assertTrue(event.guarded());
    }


}
