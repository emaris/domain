package org.iotc.emaris.test.product;

import static apprise.backend.bytestream.Fixture.someBytestreamWith;
import static apprise.backend.bytestream.Fixture.someDocumentWith;
import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static java.lang.String.format;
import static org.iotc.emaris.product.Constants.productPrefix;
import static org.iotc.emaris.product.Constants.productType;
import static test.apprise.backend.Fixture.someMultilingual;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.any;
import static test.apprise.backend.Testmate.listOf;
import static test.apprise.backend.iam.Fixture.someTenant;

import java.util.Set;

import org.iotc.emaris.common.TenantAudience;
import org.iotc.emaris.product.Product;

import apprise.backend.model.Id;

public class Fixture {

        public static Product someProduct() {

                String id = Id.mint(productPrefix);

                Product product = new Product()
                                .id(id)
                                .name(someMultilingual(format("product %s", id)))
                                .lineage(listOf(Id.mint("P")))
                                .description(someMultilingual(any("product-description")))
                                .audience(someTagExpession(tenantType))
                                .audienceList(new TenantAudience()
                                                .includes(Set.of(someTenant().ref(), someTenant().ref()))
                                                .excludes(Set.of(someTenant().ref(), someTenant().ref())))
                                .userProfile(someTagExpession(userType))
                                .tags(someTag(productType), someTag().type(productType));

                product.documents().put("id1", listOf(
                                someDocumentWith(someBytestreamWith("test1.1")),
                                someDocumentWith(someBytestreamWith("test1.2"))));

                product.documents().put("id2", listOf(
                                someDocumentWith(someBytestreamWith("test2.1"))));

                return product;
        }
}
