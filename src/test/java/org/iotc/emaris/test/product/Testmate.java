package org.iotc.emaris.test.product;

import static java.util.Collections.emptyList;
import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;
import static test.apprise.backend.iam.tag.Testmate.stageTagsOf;

import javax.annotation.Priority;
import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.product.Product;
import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.product.data.ProductService;

import apprise.backend.data.Events.DatabaseReady;;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes @Priority(LIBRARY_BEFORE) DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    ProductService service;

    @Inject
    ProductDao dao;

    public static Product stageDepsOf(Product product) {

        stageTagsOf(product);

        product.lineage().forEach(lineage ->

        stage(Fixture.someProduct()
                .id(lineage)
                .lineage(emptyList())
                .tags(emptyList()))

        );


        return product;

    }

    public static Product stage(Product product) {

        if (_this.dao.lookup(product.id()).isPresent())
            return product;

        stageDepsOf(product);

        return _this.service.add(product);

    }

}
