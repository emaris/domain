package org.iotc.emaris.test.productinstance;

import static org.iotc.emaris.productinstance.data.ProductInstanceFilter.filter;
import static org.iotc.emaris.productinstance.data.ProductInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.productinstance.Fixture.someProductInstance;
import static org.iotc.emaris.test.productinstance.Testmate.stage;
import static org.iotc.emaris.test.productinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.productinstance.ProductInstance;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceService;
import org.junit.Test;

import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.validation.Exceptions.ValidationException;
import lombok.Data;
import test.apprise.backend.Testbase;

public class ProductInstanceDataTest extends Testbase {

    @Inject
    ProductInstanceService service;

    @Inject
    ProductInstanceDao dao;

    @Inject
    EventObserver observed;

    @ApplicationScoped
    @Data
    public static class EventObserver {

        List<ProductInstance> addedBulk;
        ProductInstance updated;
        RemovedInstanceEvents.Event<ProductInstance> removedEvent;

        void onAdd(@Observes @Added List<ProductInstance> instances) {
            this.addedBulk = instances;
        }

        void onUpdate(@Observes @Updated ProductInstance instance) {
            this.updated = instance;
        }

        void onRemove(@Observes RemovedInstanceEvents.Event<ProductInstance> event) {
            this.removedEvent = event;
        }
    }

    @Test
    public void instances_can_added_and_retrieved() {

        ProductInstance instance1 = someProductInstance();
        ProductInstance instance2 = someProductInstance();

        stageDepsOf(instance1);
        stageDepsOf(instance2);

        service.add(listOf(instance1, instance2));

        assertEquals(instance1, dao.get(instance1.id()));
        assertEquals(instance2, dao.get(instance2.id()));

    }

    @Test
    public void instances_can_retrieve_instances_by_ref() {

        ProductInstance instance = someProductInstance();

        stage(instance);

        assertEquals(instance, dao.get(instance.ref()));

    }

    @Test
    public void can_observe_product_creation() {

        List<ProductInstance> instances = stage(someProductInstance());

        assertEquals(instances, observed.addedBulk());

    }

    @Test
    public void cannot_add_invalid_instances() {

        try {
            stage(someProductInstance().campaign(null));
            fail();
        } catch (ValidationException expected) {
        }

    }

    @Test
    public void cannot_add_instances_outside_campaign() {

        try {
            ProductInstance instance = someProductInstance();
            stageDepsOf(instance);
            service.add(listOf(instance.campaign("another")));
            fail();
        } catch (Exception expected) {
        }

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_duplicate_instances() {

        ProductInstance instance = someProductInstance();

        stage(instance);

        ProductInstance duplicate = someProductInstance().id(instance.id());

        service.add(listOf(duplicate));

    }

    @Test(expected = IllegalStateException.class)
    public void cannot_add_instances_with_same_ref() {

        ProductInstance instance = someProductInstance();

        stage(instance);

        ProductInstance duplicate = someProductInstance().campaign(instance.campaign()).source(instance.source());

        service.add(listOf(duplicate));

    }


    @Test
    public void can_retrieve_instances_in_campaign() {

        ProductInstance instance1 = someProductInstance();
        ProductInstance instance2 = someProductInstance();

        stage(instance1, instance2);

        List<ProductInstance> instances = dao.allMatching(filter().campaign(instance1.campaign()));

        assertTrue(instances.contains(instance1));
        assertFalse(instances.contains(instance2));

    }

    @Test
    public void can_update_instances_without_side_effects_and_observe_it() {

        ProductInstance instance1 = someProductInstance(); // this we change
        ProductInstance instance2 = someProductInstance(); // this we dont touch

        stage(instance1, instance2);

        List<ProductInstance> instances = dao.allMatching(withNoConditions());

        instance1.properties().set(String.class,"changed").withDefault();

        assertFalse(instances.contains(instance1));

        service.update(instance1);

        instances = dao.allMatching(withNoConditions());

        // first has the changes, second is untouched.
        assertTrue(instances.containsAll(setOf(instance1, instance2)));

        assertEquals(instance1, observed.updated());
    }


    @Test
    public void can_remove_instances_without_side_effects_and_observe_it() {

        ProductInstance productinstance = someProductInstance();
        ProductInstance otherProductInstance = someProductInstance();

        stage(productinstance,otherProductInstance);
        
        service.remove(productinstance.id());

        List<ProductInstance> productinstances = dao.allMatching(withNoConditions());

        assertFalse(productinstances.contains(productinstance));
        assertTrue(productinstances.contains(otherProductInstance));

        assertEquals(List.of(productinstance), observed.removedEvent().instances());

    }


    @Test
    public void can_remove_multiple_instances_without_side_effects_and_observe_it() {

        ProductInstance productinstance = someProductInstance();
        ProductInstance otherProductInstance = someProductInstance();
        ProductInstance yetAnotherProductInstance = someProductInstance();

        stage(productinstance,otherProductInstance,yetAnotherProductInstance);
        
        service.remove(listOf(productinstance.id(),otherProductInstance.id()));

        List<ProductInstance> productinstances = dao.allMatching(withNoConditions());

        assertFalse(productinstances.containsAll(listOf(productinstance,otherProductInstance)));
        assertTrue(productinstances.contains(yetAnotherProductInstance));

        assertEquals(listOf(productinstance,otherProductInstance), observed.removedEvent().instances());

    }
}

