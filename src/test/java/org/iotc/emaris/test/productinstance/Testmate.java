package org.iotc.emaris.test.productinstance;

import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.test.campaign.Fixture.someCampaign;
import static org.iotc.emaris.test.product.Fixture.someProduct;
import static org.iotc.emaris.test.productinstance.Fixture.someProductInstance;

import java.util.List;
import java.util.stream.Stream;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.productinstance.ProductInstance;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceService;

import apprise.backend.data.Events.DatabaseReady;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    ProductInstanceService service;

    @Inject
    ProductInstanceDao dao;

    public static ProductInstance stageDepsOf(ProductInstance instance) {

        test.apprise.backend.iam.tag.Testmate.stageTagsOf(instance);
        org.iotc.emaris.test.campaign.Testmate.stage(someCampaign().id(instance.campaign()));
        org.iotc.emaris.test.product.Testmate.stage(someProduct().id(instance.source()));

 
        if (instance.lineage().isPresent()) {

            InstanceRef ref = instance.lineage().get();

            stage(someProductInstance()
                    .campaign(ref.campaign())
                    .source(ref.source())
                    .lineage(null)
            );
        }

        return instance;
    }

    public static List<ProductInstance> stage(ProductInstance ... instances) {

        List<ProductInstance> newinstances = Stream.of(instances)
                .filter(i -> _this.dao.lookup(i.id()).isEmpty())
                .peek(Testmate::stageDepsOf)
                .collect(toList());

        return _this.service.add(newinstances);

    }
}
