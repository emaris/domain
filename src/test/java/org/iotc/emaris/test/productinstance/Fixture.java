package org.iotc.emaris.test.productinstance;

import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;
import static org.iotc.emaris.product.Constants.productPrefix;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.productinstance.Constants.productinstancePrefix;
import static test.apprise.backend.Fixture.someTag;
import static test.apprise.backend.Fixture.someTagExpession;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.productinstance.ProductInstance;

import apprise.backend.model.Bag;
import apprise.backend.model.Id;

public class Fixture {

    public static ProductInstance someProductInstance() {

        return new ProductInstance()
                .id(Id.mint(productinstancePrefix))
                .source(Id.mint(productPrefix))
                .campaign(Id.mint(campaignPrefix))
                .lineage(new CampaignInstance.InstanceRef(Id.mint(campaignPrefix), Id.mint(productPrefix)))
                .audience(someTagExpession(tenantType))
                .userProfile(someTagExpession(userType))
                .tags(someTag(productType), someTag().type(productType))
                .properties(new Bag().set("prop","test").withDefault());

    }
    
}
