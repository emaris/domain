package org.iotc.emaris.test.productinstance;

import static org.iotc.emaris.productinstance.api.ApiConstants.byref;
import static org.iotc.emaris.productinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.productinstance.api.ApiConstants.productinstanceapi;
import static org.iotc.emaris.productinstance.api.ApiConstants.remove;
import static org.iotc.emaris.productinstance.api.ApiConstants.search;
import static org.iotc.emaris.productinstance.data.ProductInstanceFilter.withNoConditions;
import static org.iotc.emaris.test.productinstance.Fixture.someProductInstance;
import static org.iotc.emaris.test.productinstance.Testmate.stage;
import static org.iotc.emaris.test.productinstance.Testmate.stageDepsOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.Q;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.iotc.emaris.productinstance.ProductInstance;
import org.junit.Test;

import test.apprise.backend.Testbase;

public class ProductInstanceApiTest extends Testbase {

    @Test
    public void can_add_retrieve_update_and_remove_instances() {

        ProductInstance instance = someProductInstance();

        stageDepsOf(instance);

        Response response = at(productinstanceapi).post(jsonOf(listOf(instance)));

        assertSuccessful(response);

        ProductInstance fetched = at(productinstanceapi, instance.id()).get(ProductInstance.class);

        assertEquals(instance, fetched);

        List<ProductInstance> instances = at(productinstanceapi, search).post(jsonOf(withNoConditions()))
                .readEntity(collofinstances);

        assertTrue(instances.contains(instance));

        // some changes.
        instance.properties().set(String.class, "changed").withDefault();

        response = at(productinstanceapi, instance.id()).put(jsonOf(instance));

        assertSuccessful(response);

        fetched = at(productinstanceapi, instance.id()).get(ProductInstance.class);

        assertEquals(instance, fetched);

        response = at(productinstanceapi, instance.id()).delete();

        assertSuccessful(response);

        response = at(productinstanceapi, instance.id()).get();

        assertFailed(response);

    }

    @Test
    public void can_remove_instances_in_bulk() {

        ProductInstance instance1 = someProductInstance();
        ProductInstance instance2 = someProductInstance();
        ProductInstance instance3 = someProductInstance();

        stage(instance1, instance2, instance3);

        Response response = at(productinstanceapi, remove).post(jsonOf(listOf(instance1.id(), instance2.id())));

        assertSuccessful(response);

        List<ProductInstance> instances = at(productinstanceapi).get(collofinstances);

        assertFalse(instances.contains(instance1));
        assertFalse(instances.contains(instance2));
        assertTrue(instances.contains(instance3));

    }

    @Test
    public void can_retrieve_instances_byref() {

        ProductInstance instance = someProductInstance();

        stage(instance);

        ProductInstance retrieved = at(productinstanceapi, instance.ref(), Q(byref, true)).get(ProductInstance.class);

        assertEquals(instance, retrieved);
    }

    @Test
    public void can_retrieve_instance_samples() {

        ProductInstance instance = someProductInstance();

        stage(instance);

        List<ProductInstance> retrieved = at(productinstanceapi).get(collofinstances);

        assertTrue(retrieved.contains(instance));
    }

    @Test
    public void can_retrieve_instances_directly_by_campaign() {

        ProductInstance instance1 = someProductInstance();
        ProductInstance instance2 = someProductInstance();

        stage(instance1,instance2);

        List<ProductInstance> retrieved = at(productinstanceapi, campaign, instance1.campaign()).get(collofinstances);

        assertTrue(retrieved.contains(instance1));
        assertFalse(retrieved.contains(instance2));
    }

    /////// helpers

    static GenericType<List<ProductInstance>> collofinstances = new GenericType<List<ProductInstance>>() {
    };
}
