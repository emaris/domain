package org.iotc.emaris.test.productinstance;

import static apprise.backend.iam.Constants.tenantType;
import static apprise.backend.iam.Constants.userType;
import static org.iotc.emaris.test.productinstance.Fixture.someProductInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;
import static test.apprise.backend.Fixture.someTagExpession;
import static test.apprise.backend.Testmate.assertRoundTrips;
import static test.apprise.backend.Testmate.setOf;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.productinstance.ProductInstance;
import org.junit.Test;

import apprise.backend.validation.Exceptions.ValidationException;

public class ProductInstanceTest {

    @Test
    public void productinstances_roundtrip() {

        assertRoundTrips(someProductInstance());
    }

    @Test
    public void changes_can_be_copied() {

        ProductInstance productinstance = someProductInstance();

        ProductInstance updated = productinstance.copy();

        assertEquals(productinstance.campaign(), updated.campaign());
    }

    @Test
    public void changes_are_controlled() {

        ProductInstance instance = someProductInstance();
        ProductInstance original = instance.copy();
        ProductInstance updated = instance.copy();

        updated.id("newid");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.campaign("newcampaign");
        instance.updateWith(updated);
        assertEquals(original, instance);

        updated.source("newsource");
        instance.updateWith(updated);
        assertEquals(original, instance);

        // some changes are accepted

        updated.audience(someTagExpession(tenantType));
        instance.updateWith(updated);
        assertNotEquals(original, instance);

        updated.userProfile(someTagExpession(userType));
        instance.updateWith(updated);
        assertNotEquals(original, instance);

        updated.lineage(new CampaignInstance.InstanceRef("campaign", "source"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);

        updated.tags(setOf("anytag"));
        instance.updateWith(updated);
        assertNotEquals(original, instance);


    }


    @Test
    public void some_fields_are_mandatory() {

        try {
            someProductInstance().id(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}

        try {
            someProductInstance().campaign(null).validateNow();
            fail();
        }
        catch(ValidationException expected){}


    }

}
