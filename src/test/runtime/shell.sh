evalin() {
	local curr=$PWD
	cd $1 && shift
	eval $@
	cd $curr
}


tests() {

	local runtime=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	local red=`tput setaf 1`
	local green=`tput setaf 2`
	local reset=`tput sgr0`

	describe() {
		help="$help${green}\n$1${reset}\t$2"
	}

	local help="\nTESTSHELL\n========\n"

	describe "tests up" "brings up the test environment"
	describe "tests down" "tears down the test environment"
	

	# cli

	local cmd=${1:-"help"}; shift


	if [ $cmd = "help" ]
	then
		tabs -40 && echo -e "$help\n" && tabs -8
		return
	fi


	if [ $cmd = "up" ]; then
	   echo "${green}starting up test environment...${reset}"
	   evalin $runtime docker-compose up -d "$@" 
	   return;
	fi

	
	if [ $cmd = "down" ]; then
   		echo "${green}stopping test environment...${reset}"
   		evalin $runtime docker-compose down -v "$@"
		return;
	fi


	if [ $cmd = "ps" ]
	then
	   evalin $runtime docker-compose ps "$@"
	   return
	fi

	echo "${red}no, don't do '$cmd'.${reset}"
	
}


tests $@