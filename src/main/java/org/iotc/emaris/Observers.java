package org.iotc.emaris;

import static javax.interceptor.Interceptor.Priority.APPLICATION;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.event.data.EventSchema;
import org.iotc.emaris.eventinstance.data.EventInstanceSchema;
import org.iotc.emaris.partyinstance.data.PartyInstanceSchema;
import org.iotc.emaris.product.data.ProductSchema;
import org.iotc.emaris.productinstance.data.ProductInstanceSchema;
import org.iotc.emaris.requirement.data.RequirementSchema;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceSchema;
import org.iotc.emaris.submission.data.SubmissionSchema;

import apprise.backend.data.Events.DatabaseReady;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    void stageTables(@Observes @Priority(APPLICATION) DatabaseReady event, 
        
        RequirementSchema requirements,
        ProductSchema products,
        EventSchema events,
        CampaignSchema campaigns,
        PartyInstanceSchema partyinstances,
        RequirementInstanceSchema requirementinstances,
        ProductInstanceSchema productinstances,
        EventInstanceSchema eventinstances,
        SubmissionSchema submissions
        
    ) {

        log.trace("staging emaris tables (if required)");
        
        requirements.stageTables();
        products.stageTables();
        events.stageTables();
        campaigns.stageTables();
        partyinstances.stageTables();
        requirementinstances.stageTables();
        productinstances.stageTables();
        eventinstances.stageTables();
        submissions.stageTables();
        
    }

}


