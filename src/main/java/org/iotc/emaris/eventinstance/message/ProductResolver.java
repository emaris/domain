
package org.iotc.emaris.eventinstance.message;

import static apprise.backend.iam.Constants.tenantType;
import static java.lang.String.format;
import static org.iotc.emaris.product.Constants.productType;

import java.util.HashSet;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.message.model.Message;
import apprise.backend.message.model.Message.Topic;
import lombok.Data;

@ApplicationScoped
@Data
public class ProductResolver implements MessageResolver {

    final String type = productType;

    @Inject
    HelperFactory helpers;

    @Override
    public Message messageFor(EventInstance instance) {

        var helper = helpers.helperFor(instance);

        var source = helper.product();

        var message = helper.newMessage(Map.of(

            "asset", source.name(),
            "assetTitle", source.description()

        ));

        var topics = new HashSet<>(message.topics());

        topics.add(new Topic().name(format("%s:%s",message.scope().get(),source.id())).type(productType));
        topics.add(new Topic().name("*").type(tenantType));

        message.topics(topics);

        return message;

    }
}
