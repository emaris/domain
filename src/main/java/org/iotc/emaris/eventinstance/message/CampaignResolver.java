
package org.iotc.emaris.eventinstance.message;

import static org.iotc.emaris.campaign.Constants.campaignType;
import static org.iotc.emaris.campaign.stage.StagedData.campaignEnd;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.message.HelperFactory.Helper;

import apprise.backend.message.model.Message;
import lombok.Data;

@ApplicationScoped
@Data
public class CampaignResolver implements MessageResolver {

    final String type = campaignType;

    @Inject
    HelperFactory helpers;

    Map<String,String> templates = Map.of(
        campaignStart.id(),"Campaign has now started.",
        campaignEnd.id(), "Campaign has now ended."
    );

    @Override
    public Message messageFor(EventInstance instance) {

        Helper helper = helpers.helperFor(instance);

        return helper.newMessage();

    }
}
