
package org.iotc.emaris.eventinstance.message;

import static java.lang.String.format;
import static java.util.stream.Collectors.toMap;
import static org.iotc.emaris.event.Constants.eventType;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.eventinstance.message.HelperFactory.Helper;
import org.ocpsoft.prettytime.PrettyTime;

import apprise.backend.message.model.Message;
import apprise.backend.model.Multilingual.Language;
import lombok.Data;
import lombok.ToString;

@ApplicationScoped
@Data
public class EventResolver implements MessageResolver {

    final String type = eventType;

    @Inject
    HelperFactory helpers;

    @Inject
    EventInstanceDao instances;

    @Inject
    @ToString.Exclude
    Map<String, MessageResolver> resolvers;

    @Override
    public Message messageFor(EventInstance instance) {

        EventInstance targetInstance = instances.get(instance.target().get());

        String type = targetInstance.type().get();

        var targetMessage = resolvers.get(type).messageFor(targetInstance);

        var parameters = new HashMap<>(targetMessage.contentAs(Message.RawContent.class).parameters());

        Helper helper = helpers.helperFor(targetInstance);

        OffsetDateTime date = targetInstance.date()
                .map(d -> d.isAbsolute() ? d.absoluteValue() : instances.absoluteDateOf(targetInstance)).orElse(null);

                
        String modifier = null;

        if (date != null) {

            parameters.put("date", date);
            parameters.put("timeRelativeDate", Stream.of(Language.values())
                    .collect(toMap(lang -> lang, lang -> new PrettyTime(new Locale(lang.name())).format(date))));
            modifier = date.isAfter(OffsetDateTime.now()) ? "before" : "after";

        }
        else {

            modifier = "nodate";
        }

        var message = helper.newMessage().content(new Message.RawContent()
                .keys(List.of(
                        format("campaign.duedate.%s_%s_%s", instance.source(), targetInstance.source(), modifier),
                        format("campaign.duedate.%s_default_%s", instance.source(), modifier)))
                .parameters(parameters));

        message.topics(targetMessage.topics());

        return message;

    }
}
