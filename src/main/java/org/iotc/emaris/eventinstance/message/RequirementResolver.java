
package org.iotc.emaris.eventinstance.message;

import static apprise.backend.iam.Constants.tenantType;
import static java.lang.String.format;
import static org.iotc.emaris.requirement.Constants.requirementType;

import java.util.HashSet;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.message.model.Message;
import apprise.backend.message.model.Message.Topic;
import apprise.backend.model.Multilingual;
import lombok.Data;

@ApplicationScoped
@Data
public class RequirementResolver implements MessageResolver {

    final String type = requirementType;

    @Inject
    HelperFactory helpers;

    @Override
    public Message messageFor(EventInstance instance) {

        var helper = helpers.helperFor(instance);

        var source = helper.requirement();

        var message = helper.newMessage(Map.of(

            "asset", source.name(),
            "assetTitle", source.properties().get("title", Multilingual.class)

        ));

        var topics = new HashSet<>(message.topics());

        // matches any asset board.
        topics.add(new Topic().name(format("%s:%s",message.scope().get(),source.id())).type(requirementType));
        
        // matches any party board (message is sent to all CPCs anyways, so frontend needs before topic match, to avoid notifications).
        topics.add(new Topic().name("*").type(tenantType));

        message.topics(topics);

        return message;

    }
}
