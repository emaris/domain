package org.iotc.emaris.eventinstance.message;

import static java.util.stream.Collectors.toMap;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    static Map<String, MessageResolver> resolvers(Instance<MessageResolver> resolvers) {

        return resolvers.stream().collect(toMap(r -> r.type(), r -> r));
    }
}
