
package org.iotc.emaris.eventinstance.message;

import static java.lang.String.format;
import static org.iotc.emaris.campaign.Constants.campaignType;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.EventDao;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;

import apprise.backend.message.model.Message;
import apprise.backend.message.model.Message.Topic;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
public class HelperFactory {

    @Inject
    CampaignDao campaigns;

    @Inject
    RequirementDao requirements;

    @Inject
    ProductDao products;

    @Inject
    EventDao events;

    Helper helperFor(EventInstance instance) {

        return new Helper(instance);
    }

    @RequiredArgsConstructor
    public class Helper {

        final EventInstance instance;

        @Getter(lazy = true)
        final Campaign campaign = _campaign();

        Campaign _campaign() {
            return campaigns.get(instance.campaign());
        }

        @Getter(lazy = true)
        final Requirement requirement = _requirement();

        Requirement _requirement() {
            return requirements.get(instance.target().get());
        }

        @Getter(lazy = true)
        final Product product = _product();

        Product _product() {
            return products.get(instance.target().get());
        }

        @Getter(lazy = true)
        final Event event = _event();

        Event _event() {
            return events.get(instance.source());
        }
        

        // base due date message: includes instance campaign as topic and scope.
        Message newMessage(Map<String,Object> parameters) {

            var campaignTopic = new Topic().name(campaign().id()).type(campaignType);

            var augmentedParams = new HashMap<>(parameters);

            augmentedParams.put("event", event().name());
            augmentedParams.put("evenTitle", event().description());

            var message = new Message()
                                .scope(instance.campaign())
                                .topics(Set.of(campaignTopic))
                                .content(
                                    new Message.RawContent()
                                                .keys(List.of(format("campaign.duedate.%s", instance.source()),"campaign.duedate.default"))
                                                .parameters(augmentedParams)
                                );

            return message;
        }

        Message newMessage() {
            return newMessage(Collections.emptyMap());
        }


        String standardContent() {

            String event = events.lookup(instance.source()).map(e->e.name().inDefaultLanguage()).orElse("Event");
            return format("%s is now due.",event);
        }
    }

}
