
package org.iotc.emaris.eventinstance.message;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.message.model.Message;

public interface MessageResolver {

    String type();

    Message messageFor(EventInstance instance);

    static MessageResolver noResolver = new NoResolver();

    
    @ApplicationScoped 
    static class Instance {

       
        @Inject
        Map<String, MessageResolver> resolvers;

        public Message messageFor(EventInstance instance) {

          return resolvers.getOrDefault(instance.type().get(),noResolver).messageFor(instance);
        }
    }

}
