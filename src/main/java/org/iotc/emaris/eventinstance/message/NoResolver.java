
package org.iotc.emaris.eventinstance.message;

import javax.inject.Inject;

import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.message.model.Message;
import lombok.Data;

@Data
public class NoResolver implements MessageResolver {

    String type = "none";

    @Inject
    HelperFactory helpers;

    @Override
    public Message messageFor(EventInstance instance) {

        return helpers.helperFor(instance).newMessage();
    }
}
