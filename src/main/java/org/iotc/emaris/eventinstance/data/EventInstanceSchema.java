package org.iotc.emaris.eventinstance.data;

import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_campaign;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_date;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_details;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_id;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_notified_on;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_relative;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_source;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_table;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_target;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_type;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.TIMESTAMPWITHTIMEZONE;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.eventinstance.EventInstance;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class EventInstanceSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        CampaignSchema campaignSchema;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> eventinstances;

        @Inject
        @Getter
        Binder<EventInstance> binder;

        @Inject
        Event<SchemaReady<EventInstance>> events;

        @Getter
        RecordMapper<Record, EventInstance> mapper = record -> {

                EventInstance instance = binder.bind(record.get(eventinstance_details))
                                .source(record.get(eventinstance_source))
                                .campaign(record.get(eventinstance_campaign));

                if (instance.date().isPresent() && instance.date().get().isRelative())
                        instance.date().get().asRelative().target(record.get(eventinstance_relative));
                        
                return instance;

        };

        public void stageTables() {

                eventinstances = table(name(config.dbschema(), eventinstance_table));

                boolean firstTime = !schemas.exists(eventinstances);

                String stmt = jooq.createTableIfNotExists(eventinstances)
                                .column(eventinstance_id, VARCHAR.length(100))
                                .column(eventinstance_campaign, VARCHAR.nullable(false))
                                .column(eventinstance_source, VARCHAR.nullable(true))
                                .column(eventinstance_type, VARCHAR.nullable(false))
                                .column(eventinstance_target, VARCHAR.nullable(true))
                                .column(eventinstance_date, TIMESTAMPWITHTIMEZONE.nullable(true))
                                .column(eventinstance_notified_on, TIMESTAMPWITHTIMEZONE.nullable(true))
                                .column(eventinstance_relative, VARCHAR.nullable(true))
                                .column(eventinstance_details, VARCHAR.nullable(false))
                                .constraints(
                                                constraint(eventinstances.getName() + "_pkey")
                                                                .primaryKey(eventinstance_id),
                                                constraint(eventinstances.getName() + "campaign_fkey")
                                                                .foreignKey(eventinstance_campaign)
                                                                .references(campaignSchema.campaigns(), campaign_id)
                                                                .onDeleteCascade(),
                                                constraint(eventinstances.getName() + "relative_ikey")
                                                                .foreignKey(eventinstance_relative)
                                                                .references(eventinstances(), eventinstance_id)
                                                                .onDeleteSetNull())

                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(eventinstances.getName() + "_campaign_index")
                                .on(eventinstances, eventinstance_campaign).execute();
                jooq.createIndexIfNotExists(eventinstances.getName() + "_type_index") 
                                .on(eventinstances, eventinstance_type).execute();
                jooq.createIndexIfNotExists(eventinstances.getName() + "_target_index")
                                .on(eventinstances, eventinstance_target).execute();
                jooq.createIndexIfNotExists(eventinstances.getName() + "_source_index")
                                .on(eventinstances, eventinstance_source).execute();
                jooq.createIndexIfNotExists(eventinstances.getName() + "_date_index")
                                .on(eventinstances, eventinstance_date).execute();
                jooq.createIndexIfNotExists(eventinstances.getName() + "_notified_on_index")
                                .on(eventinstances, eventinstance_notified_on).execute();

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }

}
