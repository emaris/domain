package org.iotc.emaris.eventinstance.data;

import static org.iotc.emaris.campaign.Constants.muted;
import static org.iotc.emaris.event.Constants.eventType;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.eventinstance.EventDate.RelativeDate;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.api.Dtos.DetectionOutcome;
import org.iotc.emaris.eventinstance.data.Events.DueDateEvent;
import org.iotc.emaris.eventinstance.message.MessageResolver;

import apprise.backend.message.data.MessageService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class DueDateService {

    @Inject
    EventInstanceDao dao;

    @Inject
    CampaignDao campaigns;

    @Inject
    MessageService messages;

    @Inject
    MessageResolver.Instance resolver;

    @Inject
    Event<DueDateEvent> events;

    public DetectionOutcome detectAndNotify(EventInstanceFilter filter) {

        log.trace("detecting due dates ({})", filter);

        List<EventInstance> due = dao.allMatching(filter.due(true));

        Map<String, Boolean> offlineMap = new HashMap<>();

        // skip notifications if campaign is currently offline.
        Predicate<EventInstance> campaignIsMuted = (instance) -> {

            boolean suppress = offlineMap.computeIfAbsent(instance.campaign(),
                    id -> campaigns.get(id).properties().get(muted, false));

            if (suppress)
                log.trace("supressed notification on temporal {} because campaign is muted.", instance.source());

            return suppress;
        };

        // skip notifications for temporals dated before an already notified target.
        Predicate<EventInstance> temporalPredatesNotifiedTarget = (instance) -> {

            var suppress = false;

            if (instance.type().map(e -> e.equals(eventType)).orElse(false)) {

                var absoluteDate = instance.date().map(d -> d.isAbsolute()).orElse(false);
                var relativeBefore = instance.date().map(
                        d -> d.isRelative() && d.asRelative().period().direction() == RelativeDate.Direction.before)
                        .orElse(false);

                if (absoluteDate || relativeBefore) {

                    var target = dao.lookup(instance.target().get());

                    if (target.get().notifiedOn().isPresent()) {

                        if (relativeBefore)
                            suppress = true;

                        else if (absoluteDate) {
                            var targetDate = dao.absoluteDateOf(target.get());
                            if (targetDate != null
                                    && instance.date().map(d -> d.asAbsolute().absoluteValue().isBefore(targetDate))
                                            .orElse(false))
                                suppress = true;
                        }

                    }

                    if (suppress)
                        log.trace("suppressed notification for temporal {} because target {} is already notified...",
                                instance.source(), target.get().source());

                }

            }

            return suppress;

        };

        Predicate<EventInstance> temporalPredatesStartOfCampaign = (instance) -> {

            var suppress = false;

            var campaignStart = dao.startOf(instance.campaign());

            if (campaignStart.isEmpty())
                return false;

            var campaignStartDate = campaignStart.get();

            if (dao.absoluteDateOf(instance).isBefore(campaignStartDate)) {

                log.trace("suppressed notification for {} because predates start of campaign {}...",
                        instance.source(), campaignStartDate);
                suppress = true;

            }
            return suppress;

        };

        if (due.size() > 0) {

            // want to fail-fast here, no try/catch.
            due.stream().forEach(instance -> {

                instance.notifiedOn(OffsetDateTime.now());

                if (!campaignIsMuted.test(instance) && !temporalPredatesNotifiedTarget.test(instance)
                        && !temporalPredatesStartOfCampaign.test((instance))) {

                    // we can't fail with only one of the notification mechanisms now, or the message will be retried later and be duplicated with
                    // the mechanism that works. it's best effort at this point: we either send some notification or we mark quitely.

                    try {
                        messages.add(resolver.messageFor(instance));
                    } catch (Exception e) {
                        log.error("can't send due date message", e);
                    }

                    try {

                        events.fire(new DueDateEvent(instance));

                    } catch (Exception e) {
                        log.error("can't send due date mail", e);
                    }
                }

                dao.update(instance);

            });
        }

        var outcome = new DetectionOutcome().detected(due.size());

        return outcome;
    }
}
