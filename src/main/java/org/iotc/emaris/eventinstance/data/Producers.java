package org.iotc.emaris.eventinstance.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.eventinstance.EventInstance;


import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<EventInstance> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, EventInstance.class);
    } 
}
