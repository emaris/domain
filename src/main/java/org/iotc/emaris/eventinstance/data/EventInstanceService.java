package org.iotc.emaris.eventinstance.data;

import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.eventinstance.Constants.eventinstancePrefix;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class EventInstanceService {


    @Inject
    EventInstanceDao dao;

    @Inject
    Event<List<EventInstance>> bulkEvents;

    @Inject
    Event<EventInstance> events;

    @Inject
    Event<RemovedInstanceEvents.Event<EventInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;


    public List<EventInstance> add(@NonNull List<EventInstance> instances) {

        instances.stream().filter(i -> i.id() == null).forEach(i -> i.id(Id.mint(eventinstancePrefix)));

        Validated.validateNow(instances,Mode.create);

        log.info("adding {} event instances",instances.size());

        dao.add(instances);

        bulkEvents.select(Added.event).fire(instances);
        
        return instances;
    }

    public EventInstance update(@NonNull EventInstance updated) {

        EventInstance instance = dao.get(updated.id());

        instance.updateWith(updated);

        instance.validate(Mode.update);

        log.info("updating event instance {}",instance.ref());

        dao.updateWithClosure(instance);

        events.select(Updated.event).fire(instance);

        return instance;
    }

    public List<EventInstance> update(@NonNull Changes changes) {

       List<EventInstance> processed = new ArrayList<>();

        if (changes.added()!=null && changes.added().size()>0)
            processed.addAll(add(changes.added()));

        if (changes.updated()!=null && changes.updated().size()>0)
            processed.addAll(update(changes.updated()));

        if (changes.removed()!=null && changes.removed().size()>0)
            remove(changes.removed());

        return processed;
    }


    public List<EventInstance> update(@NonNull List<EventInstance> updated) {

         

     List<EventInstance> instances = updated.stream()
            .map( i -> dao.get(i.id()).updateWith(i))
            .peek(i -> i.validate(Mode.update))
            .collect(toList());

        log.info("updating {} event instances",instances.size());

        dao.updateMany(instances);

        bulkEvents.select(Updated.event).fire(instances);

        return instances;
    }


    public EventInstance remove(@NonNull String id) {

        EventInstance instance = dao.get(id);

        instance.validateNow(Mode.delete);
					
		log.info("removing event instance {}", instance.ref());
		
		dao.remove(instance);

		removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(List.of(instance)));
		
		return instance;
	

    }
    

    public void remove(@NonNull List<String> ids) {


        var instances = dao.allMatching((EventInstanceFilter.filter().ids(ids)));

        var removed = dao.remove(ids);

        log.info("removed {} event instances", removed);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(instances));

		
	}

}
