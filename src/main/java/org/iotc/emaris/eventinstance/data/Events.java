package org.iotc.emaris.eventinstance.data;

import org.iotc.emaris.eventinstance.EventInstance;

import lombok.AllArgsConstructor;
import lombok.Data;

public interface Events {
    
    @Data
    @AllArgsConstructor
    public static class DueDateEvent {

        EventInstance instance;
    }
}
