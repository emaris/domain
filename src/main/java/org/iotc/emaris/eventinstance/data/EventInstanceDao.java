package org.iotc.emaris.eventinstance.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.campaign.stage.StagedData.campaignEnd;
import static org.iotc.emaris.campaign.stage.StagedData.campaignStart;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_campaign;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_date;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_details;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_id;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_id_as;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_notified_on;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_relative;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_relative_as;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_source;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_table;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_target;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_target_as;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_type;
import static org.iotc.emaris.eventinstance.data.EventInstanceFilter.filter;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.selectDistinct;
import static org.jooq.impl.DSL.table;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.DataConstants;
import org.iotc.emaris.event.data.EventSchema;
import org.iotc.emaris.eventinstance.EventInstance;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.conf.ParamType;
import org.jooq.exception.DataAccessException;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.data.ErrorCode;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class EventInstanceDao {

    private static final int max_on_empty = 500;

    @Inject
    DSLContext jooq;

    @Inject
    EventInstanceSchema schema;

    @Inject
    TagMappers tagmappers;

    @Inject
    TagSchema tagschema;

    @Inject
    EventSchema eventSchema;

    @Inject
    TagInstanceDao tags;

    public List<EventInstance> allMatching(EventInstanceFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit()).collect(toList());

    }

    public List<Event> allSourcesMatching(EventInstanceFilter filter) {

        var query = jooq.select().from(eventSchema.events())
                .join(selectDistinct(eventinstance_source.as("source")).from(schema.eventinstances())
                        .where(filter.toCondition()))
                .on(eventinstance_source.as("source").eq(DataConstants.event_id));

        return query.fetch(eventSchema.mapper());
    }


    public List<String> usage(String source) {

        return jooq.selectDistinct()
                 .from(schema.eventinstances())
                 .where(eventinstance_source.eq(source))
                 .fetch(eventinstance_campaign);
        
    }
    public int sizeMatching(EventInstanceFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.eventinstances()).where(filter.toCondition()));
    }

    public OffsetDateTime absoluteDateOf(EventInstance instance) {

        return jooq.select().from(schema.eventinstances()).where(eventinstance_id.eq(instance.id()))
                .fetchOne(eventinstance_date);
    }

    public EventInstance get(String pid) {

        return lookup(pid).orElseThrow(() -> new NoSuchEntityException(format("unknown event instance %s", pid)));

    }

    public Optional<EventInstance> lookup(String pid) {

        return fetch(eventinstance_id.equalIgnoreCase(pid)).findFirst();

    }

    public void add(List<EventInstance> instances) {

        Collection<Query> stmts = new ArrayList<>();

        for (EventInstance instance : instances)
            stmts.add(jooq.insertInto(schema.eventinstances())
                    .set(eventinstance_id, instance.id())
                    .set(eventinstance_campaign, instance.campaign())
                    .set(eventinstance_source, instance.source())
                    .set(eventinstance_type, instance.type().orElse(null))
                    .set(eventinstance_target, instance.target().orElse(null))
                    .set(eventinstance_relative,
                            instance.date().filter(d -> d.isRelative()).map(d -> d.asRelative().target()).orElse(null))
                    .set(eventinstance_date,
                            instance.date().filter(d -> d.isAbsolute() && !d.asAbsolute().none())
                                    .map(d -> OffsetDateTime.parse(d.asAbsolute().value())).orElse(null))
                    .set(eventinstance_notified_on, instance.notifiedOn().orElse(null))
                    .set(eventinstance_details, schema.binder().bind(pruned(instance))));

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instances refer to resources that don't exist (campaign,source,lineage,relative).")))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(("duplicate instances.")))

            );
        }

        updateClosureOf(instances);

        tags.addTags(instances, i -> i.id());

    }

    public void updateMany(List<EventInstance> instances) {

        Collection<Query> stmts = new ArrayList<>();

        for (EventInstance instance : instances)
            stmts.add(updateStatement(instance));

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instances refer to resources that don't exist (campaign,source,lineage,relative).")))

            );
        }

        updateClosureOf(instances);

        tags.replaceTags(instances, i -> i.id());

    }

    public void updateWithClosure(EventInstance instance) {

        update(instance);

        updateClosureOf(instance);

    }

    void update(EventInstance instance) {

        try {

            updateStatement(instance).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instance refer to resources that don't exist (campaign,source,lineage,relative).")))

            );
        }

        tags.replaceTags(instance.id(), instance.tags());

    }

    public Optional<OffsetDateTime> dateOf(EventInstance instance) {

        return jooq.selectFrom(schema.eventinstances()).where(eventinstance_id.eq(instance.id())).fetchOptional(
                eventinstance_date,
                OffsetDateTime.class);

    }

    public Optional<OffsetDateTime> startOf(String campaign) {

        return allMatching(filter().campaign(campaign).source(campaignStart.id()).target(campaign)).stream().sorted()
                .findFirst().flatMap(this::dateOf);

    }

    public Optional<OffsetDateTime> endOf(String campaign) {

        return allMatching(filter().campaign(campaign).source(campaignEnd.id()).target(campaign)).stream().sorted()
                .findFirst().flatMap(this::dateOf);

    }

    private Query updateStatement(EventInstance instance) {

        return jooq.update(schema.eventinstances())
                .set(eventinstance_type, instance.type().orElse(null))
                .set(eventinstance_target, instance.target().orElse(null))
                .set(eventinstance_relative,
                        instance.date().filter(d -> d.isRelative()).map(d -> d.asRelative().target()).orElse(null))

                .set(eventinstance_notified_on, instance.notifiedOn().orElse(null))
                .set(eventinstance_details, schema.binder().bind(pruned(instance)))
                .where(eventinstance_id.equalIgnoreCase(instance.id()));
    }

    private List<String> updateClosureOf(EventInstance instance) {

        // fetches all instances whose dates are directly or indirectly related to this
        // instance's.
        List<EventInstance> closure = jooq.withRecursive("targets").as(

                jooq.selectFrom(schema.eventinstances()).where(eventinstance_id.eq(instance.id()))
                        .union(
                                jooq.select(field("i.*")).from(schema.eventinstances().as("i")
                                        .innerJoin(table(name("targets")).as("t"))
                                        // we need both directions because a new event may be added in tail to chain
                                        .on(eventinstance_id_as("i").eq(eventinstance_relative_as("t")))
                                        .or(eventinstance_id_as("t").eq(eventinstance_relative_as("i")))))

        ).selectFrom("targets")
                .fetch(schema.mapper())
                .stream()
                .sorted().collect(toList());

        // closure.forEach(i -> log.trace(i.date().toString()));

        // either absolute or relative without a target.
        EventInstance root = closure.get(0);

        log.trace("closure: {}",closure);

        // if we cannot resolve a date for the root we cannot resolve any of them.
        if (root.date().isEmpty() || root.date().get().isRelative() || root.date().get().asAbsolute().none())
            return emptyList();

        // resolves all relative dates into absolute dates using the relevant part of
        // the closure
        Collection<Query> stmts = closure.stream().map(i -> {

            OffsetDateTime absoluteDate = i.date().get().absoluteValue(closure);

            // log.trace("id:{},computed date:{}", absoluteDate);

            return jooq.update(schema.eventinstances())
                    .set(eventinstance_date, absoluteDate)
                    .where(eventinstance_id.equalIgnoreCase(i.id()));

        })
                .collect(toList());

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae);
        }

        return closure.stream().map(i -> i.id()).collect(toList());

    }

    private void updateClosureOf(List<EventInstance> instances) {

        List<EventInstance> sorted = instances.stream().sorted().collect(toList());

        if (sorted.size() > 0) {
            List<String> udpated = updateClosureOf(sorted.get(0));
            updateClosureOf(
                    sorted.subList(1, sorted.size()).stream().filter(i -> !udpated.contains(i.id())).collect(toList()));
        }

    }

    public int remove(EventInstance instance) {

        var removed = jooq.delete(schema.eventinstances()).where(eventinstance_id.equalIgnoreCase(instance.id()))
                .execute();

        tags.removeTags(instance.id());

        return removed;

    }

    public int remove(List<String> ids) {

        var removed = jooq.delete(schema.eventinstances()).where(eventinstance_id.in(ids)).execute();

        tags.removeTags(ids);

        return removed;

    }

    public List<EventInstance> removeMatching(EventInstanceFilter filter) {

        Select<Record> query = jooq.select().from(schema.eventinstances()).where(filter.toCondition());

        log.info("query:{}", query.getSQL(ParamType.INLINED));

        List<String> ids = query.fetch(eventinstance_id);

        var closure = jooq.withRecursive("targets").as(

                jooq.selectFrom(schema.eventinstances()).where(eventinstance_id.in(ids))
                        .union(
                                jooq.select(field("i.*")).from(schema.eventinstances().as("i")
                                        .innerJoin(table(name("targets")).as("t"))
                                        .on(eventinstance_id_as("t").eq(eventinstance_target_as("i"))

                                        )))

        ).selectFrom("targets")
                .fetch(schema.mapper())
                .stream()
                .sorted().collect(toList());

        //log.info("closure:{}",closure);

        var closureIds = closure.stream().map(i -> i.id()).collect(toList());

        jooq.delete(schema.eventinstances()).where(eventinstance_id.in(closureIds)).execute();

        tags.removeTags(closureIds);

        return closure;

    }

    // helpers

    private Stream<EventInstance> fetch(Condition conditions) {

        return fetch(conditions, 0);
    }

    private Stream<EventInstance> fetch(Condition conditions, int limit) {

        SelectConditionStep<Record> select = jooq.select().from(schema.eventinstances()).where(conditions);

        Table<Record> selectedInstances = (limit > 0 ? select.limit(limit) : select).asTable(eventinstance_table);

        Select<Record> query = jooq.select().from(selectedInstances).leftJoin(tagschema.tag_instances())
                .on(eventinstance_id.eq(tagged));

        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    EventInstance instance = e.getKey();

                    instance.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return instance;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private EventInstance pruned(EventInstance instance) {

        EventInstance pruned = instance.copy().tags(emptyList()).campaign(null).source(null);

        if (pruned.date().isPresent() && pruned.date().get().isRelative())
            pruned.date().get().asRelative().target(null);

        return pruned;

    }

}
