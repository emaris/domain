
package org.iotc.emaris.eventinstance.data;

import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_id;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_campaign;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_date;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_source;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_target;
import static org.iotc.emaris.eventinstance.data.DataConstants.eventinstance_type;

import java.time.OffsetDateTime;
import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(includeFieldNames = true)
public class EventInstanceFilter {

	List<String> ids;

	String campaign;
	List<String> campaigns;
	String source;
	List<String> sources;
	String target;
	List<String> targets;
	String type;
	OffsetDateTime before;
	OffsetDateTime after;
	boolean due;

	int limit;

	public static EventInstanceFilter filter() {
		return new EventInstanceFilter();
	}

	public static EventInstanceFilter withNoConditions() {
		return new EventInstanceFilter();
	}

	public boolean isEmpty() {

		return campaign == null && source == null && (sources == null || sources.size() == 0)
				&& target == null && (targets == null || targets.size() == 0) && before == null && after == null;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (campaign != null)
			condition = condition.and(eventinstance_campaign.eq(campaign));

		if (campaigns != null)
			condition = condition.and(eventinstance_campaign.in(campaigns));

		if (ids != null && ids.size() > 0)
			condition = condition.and(eventinstance_id.in(ids));

		if (source != null)
			condition = condition.and(eventinstance_source.eq(source));

		if (sources != null && sources.size() > 0)
			condition = condition.and(eventinstance_source.in(sources));

		if (type != null)
			condition = condition.and(eventinstance_type.eq(type));

		if (target != null)
			condition = condition.and(eventinstance_target.eq(target));

		if (targets != null && targets.size() > 0)
			condition = condition.and(eventinstance_target.in(targets));

		if (due){
			
			if (before==null)
				before(OffsetDateTime.now());	// default upper boundis now,  (lower bound has no default: anything still due)

			condition = condition.and(DataConstants.eventinstance_notified_on.isNull());
		}

		if (after != null)
			condition = condition.and(eventinstance_date.greaterOrEqual(after));

		if (before != null)
			condition = condition.and(eventinstance_date.lessOrEqual(before));
		
		return condition;
	} 
}