package org.iotc.emaris.eventinstance.data;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.event.Constants;
import org.iotc.emaris.event.Event;
import org.iotc.emaris.eventinstance.EventInstance;

import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    EventInstanceDao dao;

    @Inject
    javax.enterprise.event.Event<RemovedInstanceEvents.Event<EventInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    // on template removal, removes all its nstances campaign-by-campaign.
    void onEventTemplateRemoval(@Observes @Removed Event source) {

        // shouldn't happen but extra safety after being burnt: can't risk a broader filter.
        if (source.id() == null)
            return;

        var filter = EventInstanceFilter.filter().source(source.id());

        var removed = dao.removeMatching(filter);

        removed.stream().collect(groupingBy(t -> t.campaign())).values().forEach(removedByCampaign ->

        removeEvents.select(Removed.cascadeEvent).fire(removeEventFactory.eventFor(removedByCampaign))

        );

        log.info("removed {} event instance(s) following removal of event {}", removed.size(),
                source.name().inDefaultLanguage());

    }


    // triggers when multiple instances are removed, any type, removes related events.
    void onBulkInstanceRemoval(@Observes  @Removed RemovedInstanceEvents.Event<?> event) {

        var instances = event.instances();

        // shouldn't happen but extra safety after being burnt: can't risk a broader filter.
        if (instances.isEmpty())
            return;

        var type = instances.get(0).instanceType();
        var campaign = instances.get(0).campaign();

        var filter = type.equals(Constants.eventType) ?

                EventInstanceFilter.filter().campaign(campaign)
                        .targets(instances.stream().map(i -> i.id()).collect(toList()))
                : EventInstanceFilter.filter().campaign(campaign)
                        .targets(instances.stream().map(i -> i.source()).collect(toList()));

        var removed = dao.removeMatching(filter);

        log.info("removed {} event instance(s) in campaign {} following removal of {} '{}' instance(s)",
                removed.size(), campaign, instances.size(), type);

        // fires event to about the cascade removal.
        if (!removed.isEmpty())
            removeEvents.select(Removed.cascadeEvent).fire(removeEventFactory.eventFor(removed));

    }

    // triggers when multiple instances are removed in cascade, any type, to removes related events.
    void onBulkInstanceCascadeRemoval(@Observes @Removed(cascade = true) RemovedInstanceEvents.Event<?> event) {

        var instances = event.instances();

        if (instances.isEmpty())
            return;

        var type = instances.get(0).instanceType();

        // if we're cascade remocing events we're not trying to cascade again.
        if (!type.equals(Constants.eventType))
            onBulkInstanceRemoval(event);

    }

}
