package org.iotc.emaris.eventinstance.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import java.time.OffsetDateTime;


import org.jooq.Field;

public class DataConstants {

    public static final String eventinstance_table  = "eventinstance";

    public static final Field<String> eventinstance_id = field(name(eventinstance_table, "id"), String.class);
    public static final Field<String> eventinstance_campaign = field(name(eventinstance_table,"campaign"), String.class);
    public static final Field<String> eventinstance_source = field(name(eventinstance_table,"source"), String.class);
    public static final Field<String> eventinstance_type = field(name(eventinstance_table,"type"), String.class);
    public static final Field<String> eventinstance_target = field(name(eventinstance_table,"target"), String.class);
    public static final Field<OffsetDateTime> eventinstance_notified_on = field(name(eventinstance_table,"notified_on"), OffsetDateTime.class);
    public static final Field<OffsetDateTime> eventinstance_date = field(name(eventinstance_table,"date"), OffsetDateTime.class);
    public static final Field<String> eventinstance_relative = field(name(eventinstance_table,"relative_to"), String.class);
	public static final Field<String> eventinstance_details = field(name(eventinstance_table, "details"), String.class);
    
    public static final Field<String> eventinstance_id_as(String name) {
   
        return field(name(name, eventinstance_id.getName()), String.class);

    }

    public static final Field<String> eventinstance_relative_as (String name) {
   
        return field(name(name, eventinstance_relative.getName()), String.class);

    }

    public static final Field<String> eventinstance_target_as (String name) {
   
        return field(name(name, eventinstance_target.getName()), String.class);

    }

}