package org.iotc.emaris.eventinstance.data;

import static apprise.backend.mail.model.Constants.noTenantTarget;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.requirement.Constants.requirementType;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.common.TenantAudience;
import org.iotc.emaris.event.data.EventDao;
import org.iotc.emaris.eventinstance.Constants;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.data.Events.DueDateEvent;
import org.iotc.emaris.partyinstance.data.PartyInstanceDao;
import org.iotc.emaris.partyinstance.data.PartyInstanceFilter;
import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceFilter;
import org.iotc.emaris.requirement.data.RequirementDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter;
import org.iotc.emaris.submission.Submission.SubmissionState;
import org.iotc.emaris.submission.data.TrailDao;
import org.iotc.emaris.submission.data.TrailFilter;
import org.ocpsoft.prettytime.PrettyTime;

import apprise.backend.iam.model.TenantRef;
import apprise.backend.mail.client.Mailer;
import apprise.backend.mail.model.MailContent;
import apprise.backend.mail.model.MailEvent;
import apprise.backend.mail.model.MailTarget;
import apprise.backend.mail.model.MailTopic;
import apprise.backend.model.Multilingual;
import apprise.backend.model.Multilingual.Language;
import apprise.backend.tag.TagExpression;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class DueDateMailer {

        @Inject
        Mailer mailer;

        @Inject
        CampaignDao campaigns;

        @Inject
        RequirementDao requirements;

        @Inject
        RequirementInstanceDao requirementInstances;

        @Inject
        ProductDao products;

        @Inject
        ProductInstanceDao productInstances;

        @Inject
        EventDao events;

        @Inject
        EventInstanceDao eventInstances;

        @Inject
        TrailDao trails;

        @Data
        @AllArgsConstructor
        static class AssetDescriptor {
                String id;
                Multilingual name;
                Multilingual title;
                TagExpression audience;
                TenantAudience audienceList = new TenantAudience();

        }

        AssetDescriptor assetOf(EventInstance instance) {

                var assetId = instance.target().get();

                var type = instance.type().get();

                var campaign = instance.campaign();

                if (type.equals(productType)) {

                        var asset = products.get(assetId);

                        var assetinstance = productInstances
                                        .allMatching(ProductInstanceFilter.filter().campaign(campaign).source(assetId))
                                        .get(0);

                        return new AssetDescriptor(asset.id(), asset.name(), asset.description(),
                                        assetinstance.audience().orElse(asset.audience()),
                                        assetinstance.audienceList().orElse(asset.audienceList()));

                } else if (type.equals(requirementType)) {

                        var asset = requirements.get(instance.target().get());

                        var assetinstance = requirementInstances
                                        .allMatching(RequirementInstanceFilter.filter().campaign(campaign)
                                                        .source(assetId))
                                        .get(0);

                        return new AssetDescriptor(asset.id(), asset.name(),
                                        asset.properties().get("title", Multilingual.class),
                                        assetinstance.audience().orElse(asset.audience()),
                                        assetinstance.audienceList().orElse(asset.audienceList()));
                }

                return null;

        };

        void send(@Observes DueDateEvent event, PartyInstanceDao parties) {

                var instance = event.instance();

                var assetType = instance.type().get();

                var temporal = assetType.equals(eventType);

                var target = temporal ? eventInstances.get(instance.target().get()) : instance;

                var source = events.get(target.source());

                var campaign = campaigns.get(instance.campaign());

                var asset = assetOf(target);

                var assetTrails = trails
                                .allMatching(TrailFilter.filter().campaign(instance.campaign()).asset((asset.id())));

                var partiesWithTrails = assetTrails.stream()
                                .map(t -> t.key().party())
                                .distinct()
                                .collect(toList());

                var partiesYetToSubmit = assetTrails
                                .stream()
                                .filter(t -> t.submissions().stream()
                                                .noneMatch(s -> s.lifecycle().state() == SubmissionState.submitted))
                                .map(t -> t.key().party())
                                .distinct()
                                .collect(toList());

                Map<String, Object> parameters = new HashMap<>(Map.of( // a mutable map
                                "eventId", target.source(),
                                "event", source.name(),
                                "eventTitle", source.description(),
                                "campaign", campaign.name(),
                                "campaignId", campaign.id(),
                                "assetType", target.type()));

                var date = eventInstances.absoluteDateOf(target);

                if (date != null) {

                        var relativeDate = Stream.of(Language.values())
                                        .collect(toMap(lang -> lang,
                                                        lang -> new PrettyTime(new Locale(lang.name())).format(date)));

                        parameters.put("eventDate", String.valueOf(date.toInstant().toEpochMilli()));
                        parameters.put("timeRelativeDate", relativeDate);

                }

                if (temporal)
                        parameters.put("temporal", instance.source());

                if (asset != null) {
                        parameters.put("asset", asset.name());
                        parameters.put("assetId", asset.id());
                        parameters.put("assetTitle",
                                        asset.title().languages().isEmpty() ? asset.name() : asset.title());
                }

                var selectedPartiesAndSEC = Stream.concat(

                                Stream.of(noTenantTarget),
                                parties.allMatching((PartyInstanceFilter.filter().campaign(instance.campaign())))
                                                .stream()
                                                .filter(p -> !partiesWithTrails.contains(p.source())
                                                                || partiesYetToSubmit.contains(p.source()))
                                                .filter(p -> asset.audience().matches(p)
                                                                && asset.audienceList()
                                                                                .matches(new TenantRef(p.source())))
                                                .map(p -> p.source())

                ).collect(joining(MailTarget.targetSeparator));

                log.trace("addressing due data audience: {} given {} have saved, and of these {} have yet to submit",
                                selectedPartiesAndSEC,
                                partiesWithTrails,
                                partiesYetToSubmit);

                var mail = new MailEvent()
                                .target(new MailTarget(selectedPartiesAndSEC))
                                .topic(new MailTopic().value(Constants.duedateMailTopic))
                                .content(new MailContent.Raw().template(Constants.duedateTemplate)
                                                .parameters(parameters));

                mailer.send(mail);

        }
}
