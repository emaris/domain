package org.iotc.emaris.eventinstance.data;

import java.util.ArrayList;
import java.util.List;

import org.iotc.emaris.eventinstance.EventInstance;

import lombok.Data;

@Data
public class Changes {
    
    List<EventInstance> added = new ArrayList<>();
    List<EventInstance> updated = new ArrayList<>();
    List<String> removed = new ArrayList<>();
}
