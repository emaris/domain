package org.iotc.emaris.eventinstance;

import static java.util.Collections.emptyList;
import static org.iotc.emaris.event.Constants.eventType;

import java.time.OffsetDateTime;
import java.util.Optional;

import org.iotc.emaris.campaign.CampaignInstance.Base;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class EventInstance extends Base<EventInstance> implements Comparable<EventInstance> {

    String type;

    String target;

    EventDate date;

    OffsetDateTime notifiedOn;
    

    public EventInstance() {
        super(eventType);
    }

    public EventInstance(EventInstance other) {
      
        super(other);

        type(other.type).notifiedOn(other.notifiedOn);
    }


    @Override
    public EventInstance copy() {
        return new EventInstance(this);
    }


    @Override
    public int compareTo(EventInstance o) {
        
        if (date==null)
            return o.date==null ? 0: -1;
            
        if (o.date==null) 
            return 1;

        if (date.isAbsolute()){ 

            if (o.date.isAbsolute()){  // both absolute

                if (date.asAbsolute().none()) // this none.
                    return o.date.asAbsolute().none() ? 0 : -1;
                else 
                    // other none
                    return o.date.asAbsolute().none() ? 1 
                    
                    // both are valued.
                    : date.absoluteValue(emptyList()).compareTo(o.date.asAbsolute().absoluteValue(emptyList())) ;
            }
            // other not abslute.
            else return -1;
        }
       
        if (o.date.isAbsolute())
            return 1;

        return id().equals(o.date.asRelative().target()) ? -1 : o.id().equals(date.asRelative().target()) ? 1 :0;

    }


    @Override
    public EventInstance updateWith(EventInstance other) {

        return super.updateWith(other)
         .target(other.target().orElse(null))
         .notifiedOn(other.notifiedOn().orElse((null)))
         .date(other.date().map(d->d.copy()).orElse(null));
    }

    
    public Optional<String> type() {

        return Optional.ofNullable(type);
    }

    public Optional<String> target() {

        return Optional.ofNullable(target);
    }

    public Optional<EventDate> date() {

        return Optional.ofNullable(date);
    }

    public Optional<OffsetDateTime> notifiedOn() {

        return Optional.ofNullable(notifiedOn);
    }

    

}
