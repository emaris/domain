package org.iotc.emaris.eventinstance;

import static org.iotc.emaris.event.Constants.eventPrefix;

public class Constants {

    public static final String eventinstancePrefix = eventPrefix + "I";

    public static final String duedateTemplate = "duedate";
    public static final String duedateMailTopic = "event";
    public static final String duedateReminderMailTopic = "event.reminder";
}
