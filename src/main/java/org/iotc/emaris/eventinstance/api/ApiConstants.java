package org.iotc.emaris.eventinstance.api;


public class ApiConstants {
	
	public static final String eventinstanceapi = "eventinstance";
	public static final String search = "search";
	public static final String remove = "remove";
	public static final String campaign = "campaign";

	public static final String due = "due";

}
