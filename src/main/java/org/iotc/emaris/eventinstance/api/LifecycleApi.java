package org.iotc.emaris.eventinstance.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.eventinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.eventinstance.api.ApiConstants.eventinstanceapi;
import static org.iotc.emaris.eventinstance.api.ApiConstants.remove;
import static org.iotc.emaris.eventinstance.api.ApiConstants.search;
import static org.iotc.emaris.eventinstance.data.EventInstanceFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.eventinstance.EventInstance;
import org.iotc.emaris.eventinstance.data.Changes;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.eventinstance.data.EventInstanceFilter;
import org.iotc.emaris.eventinstance.data.EventInstanceService;

import lombok.NonNull;

public interface LifecycleApi {

    @Operation(summary = "Returns the first few event instances")

    List<EventInstance> fetchAll();

    @Operation(summary = "Returns all the event instances that match a filter")

    List<EventInstance> fetchAll(EventInstanceFilter filter);

    @Operation(summary = "Returns all the event instances in a given campaign")

    List<EventInstance> fetchByCampaign(String cid);

    @Operation(summary = "Returns a event instance with a given identifier")

    EventInstance fetchOne(String pid);

    @Operation(summary = "Adds one or more event instances", description = "Requires management rights")
    List<EventInstance> addmany(List<EventInstance> instances);

    @Operation(summary = "Updates a event instance", description = "Requires management rights")

    EventInstance updateOne(String pid, EventInstance instance);

    @Operation(summary = "Updates one ore more event instances", description = "Requires management rights")

    List<EventInstance> updateMany(Changes instances);

    @Operation(summary = "Deletes a event instances", description = "Requires management rights")
    void removeOne(String pid);

    @Operation(summary = "Deletes one or more event instances", description = "Requires management rights")

    void removeMany(List<String> pids);

    @Path(eventinstanceapi)
    public class Default implements LifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        EventInstanceDao dao;

        @Inject
        EventInstanceService service;

        @POST
        @Path(search)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<EventInstance> fetchAll(EventInstanceFilter filter) {

            return dao.allMatching(filter);

        }

        @GET
        @Produces(APPLICATION_JSON)
        @Override
        public List<EventInstance> fetchAll() {
            return dao.allMatching(filter().limit(fetchAllLimit));
        }

        @GET
        @Path(campaign + "/{cid}")
        @Override
        public List<EventInstance> fetchByCampaign(@NonNull @PathParam("cid") String cid) {
            return dao.allMatching(EventInstanceFilter.filter().campaign(cid));
        }

        @GET
        @Produces(APPLICATION_JSON)
        @Path("{pid}")
        @Override
        public EventInstance fetchOne(@NonNull @PathParam("pid") String pid) {

            return dao.get(pid);

        }

        @Override
        @POST
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        public List<EventInstance> addmany(@NonNull List<EventInstance> instances) {

            return service.add(instances);

        }


        @Override
        @PUT
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        public List<EventInstance> updateMany(@NonNull Changes changes) {

            return service.update(changes);

        }

        @PUT
        @Path("{pid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public EventInstance updateOne(@NonNull @PathParam("pid") String pid, @NonNull EventInstance instance) {

            if (!pid.equals(instance.id()))
                throw new IllegalArgumentException(
                        format("instance id '%s' does not match id in payload ('%s')", pid, instance.id()));

            return service.update(instance);
        }

        @DELETE
        @Path("{pid}")
        @Override
        public void removeOne(@NonNull @PathParam("pid") String pid) {

            service.remove(pid);
        }

        @POST
        @Path(remove)
        @Override
        public void removeMany(@NonNull List<String> pids) {
           
            service.remove(pids);
        }

    }
		
}


