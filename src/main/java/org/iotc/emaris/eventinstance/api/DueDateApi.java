package org.iotc.emaris.eventinstance.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.eventinstance.api.ApiConstants.due;
import static org.iotc.emaris.eventinstance.api.ApiConstants.eventinstanceapi;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.eventinstance.api.Dtos.DetectionOutcome;
import org.iotc.emaris.eventinstance.data.DueDateService;
import org.iotc.emaris.eventinstance.data.EventInstanceFilter;

public interface DueDateApi {
    


    @Operation(summary = "Detects events that are due in a given time window, and notifies users about them.")

    DetectionOutcome detectAntNotify(EventInstanceFilter request);


    @Path(eventinstanceapi)
    public static class Default implements DueDateApi {

        @Inject
        DueDateService service;
       
        @POST
        @Path(due)
        @Consumes(APPLICATION_JSON)
        @Produces(APPLICATION_JSON)
        @Override
        public DetectionOutcome detectAntNotify(EventInstanceFilter request) {

              return service.detectAndNotify(request);

        }


        
    }

}
