package org.iotc.emaris.eventinstance.api;

import lombok.Data;

public class Dtos {
	
	

	@Data
	public static class DetectionOutcome {
		
       int detected;
	}

}