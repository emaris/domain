package org.iotc.emaris.eventinstance;

import static java.lang.String.format;
import static java.time.format.DateTimeFormatter.ISO_OFFSET_DATE_TIME;
import static java.util.Collections.emptyList;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;

import org.iotc.emaris.campaign.CampaignInstance;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonTypeInfo(use = Id.NAME, property = "kind")
@JsonSubTypes({

        @Type(value = EventDate.AbsoluteDate.class),
        @Type(value = EventDate.RelativeDate.class)

})
public interface EventDate  {

    static AbsoluteDate absolute(OffsetDateTime date) {
        return new AbsoluteDate(date.format(ISO_OFFSET_DATE_TIME));
    }

    static RelativeDate relativeTo(CampaignInstance<?> instance) {
        return relativeTo(instance.id());
    }

    static RelativeDate relativeTo(String target) {
        return new RelativeDate().target(target);
    }

    default OffsetDateTime absoluteValue() {

        return absoluteValue(emptyList());
    }

    OffsetDateTime absoluteValue(List<EventInstance> closure);

    @JsonIgnore
    default boolean isRelative() {
        return this instanceof RelativeDate;
    }

    default RelativeDate asRelative() {
        if (isRelative())
            return RelativeDate.class.cast(this);

        throw new IllegalStateException("not a relative date");
    }

    @JsonIgnore
    default boolean isAbsolute() {
        return this instanceof AbsoluteDate;
    }

    default AbsoluteDate asAbsolute() {
        if (isAbsolute())
            return AbsoluteDate.class.cast(this);

        throw new IllegalStateException("not an absolute date");
    }

    EventDate copy();

    @Data
    @NoArgsConstructor
    @JsonTypeName("absolute")
    public static class AbsoluteDate implements EventDate {

        public static enum BranchType { pinned, recurring, single }

        String value;
        boolean none;
        BranchType branchType;

        public AbsoluteDate(String value) {
            this.value=value;
        }   

        @Override
        public AbsoluteDate copy() {
            return new AbsoluteDate(value).branchType(branchType).none(none);
        }

        @Override
        public OffsetDateTime absoluteValue(List<EventInstance> closure) {
            return OffsetDateTime.parse(value);
        }
    }



    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonTypeName("relative")
    public static class RelativeDate implements EventDate {

        public static enum Direction {
            before, after
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class RelativePeriod {

            public static enum Unit {
                
                months{

                    @Override
                    public TemporalUnit asTemporalUnit() {return ChronoUnit.MONTHS;}
                    
                }, 
                days{

                    @Override
                    public TemporalUnit asTemporalUnit() {return ChronoUnit.DAYS;}
                    
                }, 
                weeks{

                    @Override
                    public TemporalUnit asTemporalUnit() {return ChronoUnit.WEEKS;}
                    
                };

                public abstract TemporalUnit asTemporalUnit();
            }

            int number;
            Unit unit;
            Direction direction;

        }

        RelativePeriod period;
        String target;

        @Override
        public EventDate copy() {
            return new RelativeDate(new RelativePeriod(period.number, period.unit, period.direction), target);
        }

        @Override
        public OffsetDateTime absoluteValue(List<EventInstance> closure) {
            
            OffsetDateTime targetDate = closure.stream()
                    .filter(i->i.id().equals(target()))
                    .findAny()
                    .flatMap(t-> t.date())
                    .map(d -> d.absoluteValue(closure))
                    .orElseThrow(() -> new IllegalArgumentException(format("can't resolve target %s in closure",target())));
            
            
           TemporalUnit unit = period.unit().asTemporalUnit();
  
           targetDate = period.direction() == Direction.after ? targetDate.plus(period.number(),unit): targetDate.minus(period.number(),unit);

           return targetDate;

        }

    }

}
