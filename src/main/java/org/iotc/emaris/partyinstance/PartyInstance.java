package org.iotc.emaris.partyinstance;

import static apprise.backend.iam.Constants.tenantType;

import org.iotc.emaris.campaign.CampaignInstance.Base;

public class PartyInstance extends Base<PartyInstance> {

    public PartyInstance() {
        super(tenantType);
    }

    public PartyInstance(PartyInstance other) {
        super(other);
    }

    @Override
    public PartyInstance copy() {
        return new PartyInstance(this);
    }
}
