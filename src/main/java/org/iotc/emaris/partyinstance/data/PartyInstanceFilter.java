package org.iotc.emaris.partyinstance.data;

import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_campaign;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_id;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_source;

import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class PartyInstanceFilter {

	String campaign;
	String source;

	int limit;

	List<String> ids;
	
	public static PartyInstanceFilter filter() {
		return new PartyInstanceFilter();
	}

	public static PartyInstanceFilter withNoConditions() {
		return new PartyInstanceFilter();
	}

	public boolean isEmpty() {

		return campaign == null && source == null;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (campaign != null)
			condition = condition.and(partyinstance_campaign.eq(campaign));

		if (source != null)
			condition = condition.and(partyinstance_source.eq(source));
		
		if (ids != null && ids.size() > 0)
			condition = condition.and(partyinstance_id.in(ids));

		return condition;
	} 
}