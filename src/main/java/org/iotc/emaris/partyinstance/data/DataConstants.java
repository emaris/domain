package org.iotc.emaris.partyinstance.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {

    public static final String partyinstance_table  = "partyinstance";

    public static final Field<String> partyinstance_id = field(name(partyinstance_table, "id"), String.class);
    public static final Field<String> partyinstance_campaign = field(name(partyinstance_table,"campaign"), String.class);
    public static final Field<String> partyinstance_source = field(name(partyinstance_table,"source"), String.class);
	public static final Field<String> partyinstance_details = field(name(partyinstance_table, "details"), String.class);
    
}