package org.iotc.emaris.partyinstance.data;

import static org.iotc.emaris.partyinstance.Constants.partyinstancePrefix;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.partyinstance.PartyInstance;

import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class PartyInstanceService {

    @Inject
    PartyInstanceDao dao;

    @Inject
    Event<List<PartyInstance>> bulkEvents;

    @Inject
    Event<PartyInstance> events;

    @Inject
    Event<RemovedInstanceEvents.Event<PartyInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    public List<PartyInstance> add(@NonNull List<PartyInstance> instances) {

        instances.stream().filter(i -> i.id() == null).forEach(i -> i.id(Id.mint(partyinstancePrefix)));

        Validated.validateNow(instances, Mode.create);

        log.info("adding {} party instances", instances.size());

        dao.add(instances);

        bulkEvents.select(Added.event).fire(instances);

        return instances;
    }

    public PartyInstance update(@NonNull PartyInstance updated) {

        PartyInstance instance = dao.get(updated.id());

        instance.updateWith(updated);

        instance.validate(Mode.update);

        log.info("updating party instance {}", instance.ref());

        dao.update(instance);

        events.select(Updated.event).fire(instance);

        return instance;
    }

    public PartyInstance remove(@NonNull String id) {

        PartyInstance instance = dao.get(id);

        instance.validateNow(Mode.delete);

        log.info("removing party instance {}", instance.ref());

        dao.remove(instance);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(List.of(instance)));

        return instance;

    }

    public void remove(@NonNull List<String> ids) {

        var instances = dao.allMatching((PartyInstanceFilter.filter().ids(ids)));

        var removed = dao.remove(ids);

        log.info("removed {} party instances", removed);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(instances));
    }

}
