package org.iotc.emaris.partyinstance.data;

import static java.util.stream.Collectors.groupingBy;
import static org.iotc.emaris.partyinstance.data.PartyInstanceFilter.filter;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.partyinstance.PartyInstance;

import apprise.backend.iam.model.Tenant;
import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    PartyInstanceDao dao;

    @Inject
    Event<RemovedInstanceEvents.Event<PartyInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    void onTenantRemoval(@Observes @Removed Tenant source) {

        // shouldn't happen but extra safety after being burnt: can't risk a broader filter.
        if (source.id() == null)
            return;

        var filter = filter().source(source.id());

        var removed = dao.removeMatching(filter);

        log.info("removed {} party instance(s) following removal of tenant {}", removed.size(),
                source.name().inDefaultLanguage());

        // notify removals by campaign.
        removed.stream().collect(groupingBy(t -> t.campaign())).values().forEach(removedByCampaign ->

         removeEvents.select(Removed.cascadeEvent).fire(removeEventFactory.eventFor(removedByCampaign))


        );

    }

}
