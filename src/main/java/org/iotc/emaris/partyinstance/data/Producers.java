package org.iotc.emaris.partyinstance.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.partyinstance.PartyInstance;


import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<PartyInstance> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, PartyInstance.class);
    } 
}
