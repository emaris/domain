package org.iotc.emaris.partyinstance.data;

import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_campaign;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_details;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_id;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_source;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_table;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class PartyInstanceSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        CampaignSchema campaignSchema;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> partyinstances;

        @Inject
        @Getter
        Binder<PartyInstance> binder;

        @Inject
        Event<SchemaReady<PartyInstance>> events;

        @Getter
        RecordMapper<Record, PartyInstance> mapper = record ->

        binder.bind(record.get(partyinstance_details))
                        .source(record.get(partyinstance_source))
                        .campaign(record.get(partyinstance_campaign));

        public void stageTables() {

                partyinstances = table(name(config.dbschema(), partyinstance_table));

                boolean firstTime = !schemas.exists(partyinstances);

                String stmt = jooq.createTableIfNotExists(partyinstances)
                                .column(partyinstance_id, VARCHAR.length(100))
                                .column(partyinstance_campaign, VARCHAR.nullable(false))
                                .column(partyinstance_source, VARCHAR.nullable(true))
                                .column(partyinstance_details, VARCHAR.nullable(false))
                                .constraints(
                                                constraint(partyinstances.getName() + "_pkey")
                                                                .primaryKey(partyinstance_id),
                                                constraint(partyinstances.getName() + "_ref")
                                                                .unique(partyinstance_campaign, partyinstance_source),
                                                constraint(partyinstances.getName() + "campaign_fkey")
                                                                .foreignKey(partyinstance_campaign)
                                                                .references(campaignSchema.campaigns(), campaign_id)
                                                                .onDeleteCascade())

                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(partyinstances.getName()+ "_campaign_index").on(partyinstances, partyinstance_campaign).execute();
                jooq.createIndexIfNotExists(partyinstances.getName()+ "_source_index").on(partyinstances, partyinstance_source).execute();

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }

}
