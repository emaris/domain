package org.iotc.emaris.partyinstance.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.iam.admin.data.DataConstants.tenant_id;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_campaign;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_details;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_id;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_source;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.exception.DataAccessException;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.data.ErrorCode;
import apprise.backend.iam.admin.data.IamSchema;
import apprise.backend.iam.admin.data.Mappers;
import apprise.backend.iam.model.Tenant;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;

@ApplicationScoped
public class PartyInstanceDao {

    private static final int max_on_empty = 500;

    @Inject
    DSLContext jooq;

    @Inject
    PartyInstanceSchema schema;

    @Inject
    IamSchema tenantSchema;

    @Inject
	Mappers mappers;

    @Inject
    TagMappers tagmappers;

    @Inject
    TagSchema tagschema;

    @Inject
    TagInstanceDao tags;

    public List<PartyInstance> allMatching(PartyInstanceFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit()).collect(toList());

    }

    public List<Tenant> allSourcesMatching(PartyInstanceFilter filter) {

        var query = jooq.select().from(tenantSchema.tenants())
                            .join(schema.partyinstances().where(filter.toCondition()))
                            .on(partyinstance_source.eq(tenant_id));
                           
        return query.fetch(mappers.tenant());
    }

    public int sizeMatching(PartyInstanceFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.partyinstances()).where(filter.toCondition()));
    }

    public PartyInstance get(String pid) {

        return lookup(pid).orElseThrow(() -> new NoSuchEntityException(format("unknown party instance %s", pid)));

    }

    public Optional<PartyInstance> lookup(String pid) {

        return fetch(partyinstance_id.equalIgnoreCase(pid)).findFirst();

    }

    public PartyInstance get(InstanceRef ref) {

        return lookup(ref).orElseThrow(() -> new NoSuchEntityException(format("unknown party instance %s", ref)));

    }

    public Optional<PartyInstance> lookup(InstanceRef ref) {

        return fetch(

                partyinstance_campaign.equalIgnoreCase(ref.campaign())
                        .and(partyinstance_source.equalIgnoreCase(ref.source())))
                                .findFirst();

    }

    public void add(List<PartyInstance> instances) {

        Collection<Query> stmts = new ArrayList<>();

        for (PartyInstance instance : instances)
            stmts.add(jooq.insertInto(schema.partyinstances())
                    .columns(partyinstance_id,
                            partyinstance_campaign,
                            partyinstance_source,
                            partyinstance_details)
                    .values(instance.id(),
                            instance.campaign(),
                            instance.source(),
                            schema.binder().bind(pruned(instance))));

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instances refer to resources that don't exist (campaign,source,lineage).")))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(("duplicate instances.")))

            );
        }

        tags.addTags(instances, i -> i.id());

    }

    public void update(PartyInstance instance) {

        jooq.update(schema.partyinstances())
                .set(partyinstance_details, schema.binder().bind(pruned(instance)))
                .where(partyinstance_id.equalIgnoreCase(instance.id()))
                .execute();

        tags.replaceTags(instance.id(), instance.tags());

    }

    public void remove(PartyInstance instance) {

        jooq.delete(schema.partyinstances()).where(partyinstance_id.equalIgnoreCase(instance.id())).execute();

        tags.removeTags(instance.id());

    }

    public int remove(List<String> ids) {

        var removed = jooq.delete(schema.partyinstances()).where(partyinstance_id.in(ids)).execute();

        tags.removeTags(ids);

        return removed;

    }

    public List<PartyInstance> removeMatching(PartyInstanceFilter filter) {

        List<PartyInstance> removed = jooq.select().from(schema.partyinstances()).where(filter.toCondition()).fetch(schema.mapper());

        List<String> removedIds = removed.stream().map( i -> i.id()).collect((toList()));

        jooq.delete(schema.partyinstances()).where(filter.toCondition()).execute();

        tags.removeTags(removedIds);

        return removed;

    }

    // helpers

    private Stream<PartyInstance> fetch(Condition conditions) {

        return fetch(conditions, 0);
    }

    private Stream<PartyInstance> fetch(Condition conditions, int limit) {

        SelectConditionStep<Record> select = jooq.select().from(schema.partyinstances()).where(conditions);

        Table<Record> selectedInstances =  (limit > 0 ? select.limit(limit) : select).asTable(partyinstance_table);

        Select<Record> query = jooq.select().from(selectedInstances).leftJoin(tagschema.tag_instances()).on(partyinstance_id.eq(tagged));

       
        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    PartyInstance requirement = e.getKey();

                    requirement.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return requirement;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private PartyInstance pruned(PartyInstance instance) {

        return instance.copy().tags(emptyList()).campaign(null).source(null);

    }

}
