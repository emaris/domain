package org.iotc.emaris.partyinstance.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.partyinstance.api.ApiConstants.byref;
import static org.iotc.emaris.partyinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.partyinstance.api.ApiConstants.partyinstanceapi;
import static org.iotc.emaris.partyinstance.api.ApiConstants.remove;
import static org.iotc.emaris.partyinstance.api.ApiConstants.search;
import static org.iotc.emaris.partyinstance.data.PartyInstanceFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.partyinstance.PartyInstance;
import org.iotc.emaris.partyinstance.data.PartyInstanceDao;
import org.iotc.emaris.partyinstance.data.PartyInstanceFilter;
import org.iotc.emaris.partyinstance.data.PartyInstanceService;

import lombok.NonNull;

public interface LifecycleApi {

    @Operation(summary = "Returns the first few party instances")

    List<PartyInstance> fetchAll();

    @Operation(summary = "Returns all the party instances that match a filter")

    List<PartyInstance> fetchAll(PartyInstanceFilter filter);

    @Operation(summary = "Returns all the party instances in a given campaign")

    List<PartyInstance> fetchByCampaign(String cid);

    @Operation(summary = "Returns a party instance with a given identifier or reference")

    PartyInstance fetchOne(String pid, boolean ref);

    @Operation(summary = "Adds one or more party instances", description = "Requires management rights")
    List<PartyInstance> addmany(List<PartyInstance> instances);

    @Operation(summary = "Updates a party instance", description = "Requires management rights")

    PartyInstance updateOne(String pid, PartyInstance instance);

    @Operation(summary = "Deletes a party instances", description = "Requires management rights")
    void removeOne(String pid);

    @Operation(summary = "Deletes one or more party instances", description = "Requires management rights")

    void removeMany(List<String> pids);

    @Path(partyinstanceapi)
    public class Default implements LifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        PartyInstanceDao dao;

        @Inject
        PartyInstanceService service;

        @POST
        @Path(search)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<PartyInstance> fetchAll(PartyInstanceFilter filter) {

            return dao.allMatching(filter);

        }

        @GET
        @Produces(APPLICATION_JSON)
        @Override
        public List<PartyInstance> fetchAll() {
            return dao.allMatching(filter().limit(fetchAllLimit));
        }

        @GET
        @Path(campaign + "/{cid}")
        @Override
        public List<PartyInstance> fetchByCampaign(@NonNull @PathParam("cid") String cid) {
            return dao.allMatching(PartyInstanceFilter.filter().campaign(cid));
        }

        @GET
        @Produces(APPLICATION_JSON)
        @Path("{pid}")
        @Override
        public PartyInstance fetchOne(@NonNull @PathParam("pid") String pid, @QueryParam(byref) boolean ref) {

            return ref ? dao.get(new CampaignInstance.InstanceRef(pid)) : dao.get(pid);

        }

        @Override
        @POST
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        public List<PartyInstance> addmany(@NonNull List<PartyInstance> instances) {

            return service.add(instances);

        }

        @PUT
        @Path("{pid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public PartyInstance updateOne(@NonNull @PathParam("pid") String pid, @NonNull PartyInstance instance) {

            if (!pid.equals(instance.id()))
                throw new IllegalArgumentException(
                        format("instance id '%s' does not match id in payload ('%s')", pid, instance.id()));

            return service.update(instance);
        }

        @DELETE
        @Path("{pid}")
        @Override
        public void removeOne(@NonNull @PathParam("pid") String pid) {

            service.remove(pid);
        }

        @POST
        @Path(remove)
        @Override
        public void removeMany(@NonNull List<String> pids) {
           
            service.remove(pids);
        }

    }
		
}


