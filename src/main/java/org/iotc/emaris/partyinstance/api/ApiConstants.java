package org.iotc.emaris.partyinstance.api;


public class ApiConstants {
	
	public static final String partyinstanceapi = "partyinstance";
	public static final String search = "search";
	public static final String remove = "remove";
	public static final String campaign = "campaign";
	public static final String byref = "byref";

}
