package org.iotc.emaris.productinstance.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.productinstance.api.ApiConstants.byref;
import static org.iotc.emaris.productinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.productinstance.api.ApiConstants.productinstanceapi;
import static org.iotc.emaris.productinstance.api.ApiConstants.remove;
import static org.iotc.emaris.productinstance.api.ApiConstants.search;
import static org.iotc.emaris.productinstance.data.ProductInstanceFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.productinstance.ProductInstance;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceFilter;
import org.iotc.emaris.productinstance.data.ProductInstanceService;

import lombok.NonNull;

public interface LifecycleApi {

    @Operation(summary = "Returns the first few product instances")

    List<ProductInstance> fetchAll();

    @Operation(summary = "Returns all the product instances that match a filter")

    List<ProductInstance> fetchAll(ProductInstanceFilter filter);

    @Operation(summary = "Returns all the product instances in a given campaign")

    List<ProductInstance> fetchByCampaign(String cid);

    @Operation(summary = "Returns a product instance with a given identifier or reference")

    ProductInstance fetchOne(String pid, boolean ref);

    @Operation(summary = "Adds one or more product instances", description = "Requires management rights")
    List<ProductInstance> addmany(List<ProductInstance> instances);

    @Operation(summary = "Updates a product instance", description = "Requires management rights")

    ProductInstance updateOne(String pid, ProductInstance instance);

    @Operation(summary = "Deletes a product instances", description = "Requires management rights")
    void removeOne(String pid);

    @Operation(summary = "Deletes one or more product instances", description = "Requires management rights")

    void removeMany(List<String> pids);

    @Path(productinstanceapi)
    public class Default implements LifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        ProductInstanceDao dao;

        @Inject
        ProductInstanceService service;

        @POST
        @Path(search)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<ProductInstance> fetchAll(ProductInstanceFilter filter) {

            return dao.allMatching(filter);

        }

        @GET
        @Produces(APPLICATION_JSON)
        @Override
        public List<ProductInstance> fetchAll() {
            return dao.allMatching(filter().limit(fetchAllLimit));
        }

        @GET
        @Path(campaign + "/{cid}")
        @Override
        public List<ProductInstance> fetchByCampaign(@NonNull @PathParam("cid") String cid) {
            return dao.allMatching(ProductInstanceFilter.filter().campaign(cid));
        }

        @GET
        @Produces(APPLICATION_JSON)
        @Path("{pid}")
        @Override
        public ProductInstance fetchOne(@NonNull @PathParam("pid") String pid, @QueryParam(byref) boolean ref) {

            return ref ? dao.get(new CampaignInstance.InstanceRef(pid)) : dao.get(pid);

        }

        @Override
        @POST
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        public List<ProductInstance> addmany(@NonNull List<ProductInstance> instances) {

            return service.add(instances);

        }

        @PUT
        @Path("{pid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public ProductInstance updateOne(@NonNull @PathParam("pid") String pid, @NonNull ProductInstance instance) {

            if (!pid.equals(instance.id()))
                throw new IllegalArgumentException(
                        format("instance id '%s' does not match id in payload ('%s')", pid, instance.id()));

            return service.update(instance);
        }

        @DELETE
        @Path("{pid}")
        @Override
        public void removeOne(@NonNull @PathParam("pid") String pid) {

            service.remove(pid);
        }

        @POST
        @Path(remove)
        @Override
        public void removeMany(@NonNull List<String> pids) {
           
            service.remove(pids);
        }

    }
		
}


