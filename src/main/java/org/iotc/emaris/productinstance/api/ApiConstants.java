package org.iotc.emaris.productinstance.api;


public class ApiConstants {
	
	public static final String productinstanceapi = "productinstance";
	public static final String search = "search";
	public static final String remove = "remove";
	public static final String campaign = "campaign";
	public static final String byref = "byref";

}
