package org.iotc.emaris.productinstance;

import static org.iotc.emaris.product.Constants.productPrefix;

public class Constants {

    public static final String productinstancePrefix = productPrefix + "I";
}
