package org.iotc.emaris.productinstance;

import static org.iotc.emaris.product.Constants.productType;

import java.util.Optional;

import org.iotc.emaris.campaign.CampaignInstance.Base;
import org.iotc.emaris.common.TenantAudience;

import apprise.backend.tag.TagExpression;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductInstance extends Base<ProductInstance> {

    TagExpression audience;
    TenantAudience audienceList;
    TagExpression userProfile;

    public ProductInstance() {
        super(productType);
    }

    public ProductInstance(ProductInstance other) {
        super(other);
    }

    @Override
    public ProductInstance copy() {
        return new ProductInstance(this);
    }

    @Override
    public ProductInstance updateWith(ProductInstance other) {

        super.updateWith(other);

        if (other.audience().isPresent())
            audience(other.audience().get());

        if (other.audienceList().isPresent())
            audienceList(new TenantAudience(other.audienceList().get()));

        if (other.userProfile().isPresent())
            userProfile(other.userProfile().get());

        return this;
    }

    public Optional<TagExpression> audience() {

        return Optional.ofNullable(audience);
    }

    public ProductInstance audience(TagExpression audience) {
        this.audience = audience;
        return this;
    }

    public Optional<TenantAudience> audienceList() {

        return Optional.ofNullable(audienceList);
    }

    public ProductInstance audienceList(TenantAudience audienceList) {
        this.audienceList = audienceList;
        return this;
    }

    public Optional<TagExpression> userProfile() {

        return Optional.ofNullable(userProfile);
    }

    public ProductInstance userProfile(TagExpression profile) {
        this.userProfile = profile;
        return this;
    }

}
