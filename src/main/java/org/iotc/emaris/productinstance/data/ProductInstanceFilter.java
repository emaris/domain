package org.iotc.emaris.productinstance.data;

import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_campaign;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_id;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_source;

import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class ProductInstanceFilter {

	String campaign;
	String source;

	int limit;

	List<String> ids;

	public static ProductInstanceFilter filter() {
		return new ProductInstanceFilter();
	}

	public static ProductInstanceFilter withNoConditions() {
		return new ProductInstanceFilter();
	}

	public boolean isEmpty() {

		return campaign == null && source == null;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (campaign != null)
			condition = condition.and(productinstance_campaign.eq(campaign));

		if (source != null)
			condition = condition.and(productinstance_source.eq(source));

		if (ids != null && ids.size() > 0)
			condition = condition.and(productinstance_id.in(ids));
		
		return condition;
	} 
}