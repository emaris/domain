package org.iotc.emaris.productinstance.data;

import static org.iotc.emaris.productinstance.Constants.productinstancePrefix;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.productinstance.ProductInstance;

import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class ProductInstanceService {

    @Inject
    ProductInstanceDao dao;

    @Inject
    Event<List<ProductInstance>> bulkEvents;

    @Inject
    Event<ProductInstance> events;

    @Inject
    Event<RemovedInstanceEvents.Event<ProductInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    public List<ProductInstance> add(@NonNull List<ProductInstance> instances) {

        instances.stream().filter(i -> i.id() == null).forEach(i -> i.id(Id.mint(productinstancePrefix)));

        Validated.validateNow(instances, Mode.create);

        log.info("adding {} product instances", instances.size());

        dao.add(instances);

        bulkEvents.select(Added.event).fire(instances);

        return instances;
    }

    public ProductInstance update(@NonNull ProductInstance updated) {

        ProductInstance instance = dao.get(updated.id());

        instance.updateWith(updated);

        instance.validate(Mode.update);

        log.info("updating product instance {}", instance.ref());

        dao.update(instance);

        events.select(Updated.event).fire(instance);

        return instance;
    }

    public ProductInstance remove(@NonNull String id) {

        ProductInstance instance = dao.get(id);

        instance.validateNow(Mode.delete);

        log.info("removing product instance {}", instance.ref());

        dao.remove(instance);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(List.of(instance)));

        return instance;

    }

    public void remove(@NonNull List<String> ids) {

        var instances = dao.allMatching((ProductInstanceFilter.filter().ids(ids)));

        var removed = dao.remove(ids);

        log.info("removed {} product instances", removed);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(instances));

    }

}
