package org.iotc.emaris.productinstance.data;

import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_campaign;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_details;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_id;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_source;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_table;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.productinstance.ProductInstance;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class ProductInstanceSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        CampaignSchema campaignSchema;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> productinstances;

        @Inject
        @Getter
        Binder<ProductInstance> binder;

        @Inject
        Event<SchemaReady<ProductInstance>> events;

        @Getter
        RecordMapper<Record, ProductInstance> mapper = record ->

        binder.bind(record.get(productinstance_details))
                        .source(record.get(productinstance_source))
                        .campaign(record.get(productinstance_campaign));

        public void stageTables() {

                productinstances = table(name(config.dbschema(), productinstance_table));

                boolean firstTime = !schemas.exists(productinstances);

                String stmt = jooq.createTableIfNotExists(productinstances)
                                .column(productinstance_id, VARCHAR.length(100))
                                .column(productinstance_campaign, VARCHAR.nullable(false))
                                .column(productinstance_source, VARCHAR.nullable(false))
                                .column(productinstance_details, VARCHAR.nullable(false))
                                .constraints(
                                                constraint(productinstances.getName() + "_pkey")
                                                                .primaryKey(productinstance_id),
                                                constraint(productinstances.getName() + "_ref")
                                                                .unique(productinstance_campaign, productinstance_source),
                                                constraint(productinstances.getName() + "campaign_fkey")
                                                                .foreignKey(productinstance_campaign)
                                                                .references(campaignSchema.campaigns(), campaign_id)
                                                                .onDeleteCascade())

                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(productinstances.getName()+ "_campaign_index").on(productinstances, productinstance_campaign).execute();
                jooq.createIndexIfNotExists(productinstances.getName()+ "_source_index").on(productinstances, productinstance_source).execute();

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }

}
