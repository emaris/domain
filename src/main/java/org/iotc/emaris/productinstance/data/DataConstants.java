package org.iotc.emaris.productinstance.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {

    public static final String productinstance_table  = "productinstance";

    public static final Field<String> productinstance_id = field(name(productinstance_table, "id"), String.class);
    public static final Field<String> productinstance_campaign = field(name(productinstance_table,"campaign"), String.class);
    public static final Field<String> productinstance_source = field(name(productinstance_table,"source"), String.class);
	public static final Field<String> productinstance_details = field(name(productinstance_table, "details"), String.class);
    
}