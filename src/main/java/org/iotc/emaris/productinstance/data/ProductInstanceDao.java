package org.iotc.emaris.productinstance.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.product.data.DataConstants.product_id;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_campaign;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_details;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_id;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_source;
import static org.iotc.emaris.productinstance.data.DataConstants.productinstance_table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.product.data.ProductSchema;
import org.iotc.emaris.productinstance.ProductInstance;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.exception.DataAccessException;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.data.ErrorCode;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;

@ApplicationScoped
public class ProductInstanceDao {

    private static final int max_on_empty = 500;

    @Inject
    DSLContext jooq;

    @Inject
    ProductInstanceSchema schema;

    @Inject
    ProductSchema productSchema;

    @Inject
    TagMappers tagmappers;

    @Inject
    TagSchema tagschema;

    @Inject
    TagInstanceDao tags;

    public List<ProductInstance> allMatching(ProductInstanceFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit()).collect(toList());

    }

    public List<Product> allSourcesMatching(ProductInstanceFilter filter) {

        return jooq.select().from(productSchema.products())
                            .join(schema.productinstances().where(filter.toCondition()))
                            .on(productinstance_source.eq(product_id))
                            .fetch(productSchema.mapper());


    }


    public List<String> usage(String source) {

        return jooq.selectDistinct()
                 .from(schema.productinstances())
                 .where(DataConstants.productinstance_source.eq(source))
                 .fetch(DataConstants.productinstance_campaign);
        
    }

    public int sizeMatching(ProductInstanceFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.productinstances()).where(filter.toCondition()));
    }

    public ProductInstance get(String pid) {

        return lookup(pid).orElseThrow(() -> new NoSuchEntityException(format("unknown product instance %s", pid)));

    }

    public Optional<ProductInstance> lookup(String pid) {

        return fetch(productinstance_id.equalIgnoreCase(pid)).findFirst();

    }

    public ProductInstance get(InstanceRef ref) {

        return lookup(ref).orElseThrow(() -> new NoSuchEntityException(format("unknown product instance %s", ref)));

    }

    public Optional<ProductInstance> lookup(InstanceRef ref) {

        return fetch(

                productinstance_campaign.equalIgnoreCase(ref.campaign())
                        .and(productinstance_source.equalIgnoreCase(ref.source())))
                                .findFirst();

    }

    public void add(List<ProductInstance> instances) {

        Collection<Query> stmts = new ArrayList<>();

        for (ProductInstance instance : instances)
            stmts.add(jooq.insertInto(schema.productinstances())
                    .columns(productinstance_id,
                            productinstance_campaign,
                            productinstance_source,
                            productinstance_details)
                    .values(instance.id(),
                            instance.campaign(),
                            instance.source(),
                            schema.binder().bind(pruned(instance))));

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instances refer to resources that don't exist (campaign,source,lineage).")))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(("duplicate instances.")))

            );
        }

        tags.addTags(instances, i -> i.id());

    }

    public void update(ProductInstance instance) {

        jooq.update(schema.productinstances())
                .set(productinstance_details, schema.binder().bind(pruned(instance)))
                .where(productinstance_id.equalIgnoreCase(instance.id()))
                .execute();

        tags.replaceTags(instance.id(), instance.tags());

    }

    public void remove(ProductInstance instance) {

        jooq.delete(schema.productinstances()).where(productinstance_id.equalIgnoreCase(instance.id())).execute();

        tags.removeTags(instance.id());

    }

    public int remove(List<String> ids) {

        var removed = jooq.delete(schema.productinstances()).where(productinstance_id.in(ids)).execute();

        tags.removeTags(ids);

        return removed;

    }

    public List<ProductInstance> removeMatching(ProductInstanceFilter filter) {

        List<ProductInstance> removed = jooq.select().from(schema.productinstances()).where(filter.toCondition()).fetch(schema.mapper());

        List<String> removedIds = removed.stream().map( i -> i.id()).collect((toList()));

        jooq.delete(schema.productinstances()).where(filter.toCondition()).execute();

        tags.removeTags(removedIds);

        return removed;

    }

    // helpers

    private Stream<ProductInstance> fetch(Condition conditions) {

        return fetch(conditions, 0);
    }

    private Stream<ProductInstance> fetch(Condition conditions, int limit) {

        SelectConditionStep<Record> select = jooq.select().from(schema.productinstances()).where(conditions);

        Table<Record> selectedInstances =  (limit > 0 ? select.limit(limit) : select).asTable(productinstance_table);

        Select<Record> query = jooq.select().from(selectedInstances).leftJoin(tagschema.tag_instances()).on(productinstance_id.eq(tagged));

       
        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    ProductInstance product = e.getKey();

                    product.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return product;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private ProductInstance pruned(ProductInstance instance) {

        return instance.copy().tags(emptyList()).campaign(null).source(null);

    }

}
