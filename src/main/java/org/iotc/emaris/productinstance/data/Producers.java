package org.iotc.emaris.productinstance.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.productinstance.ProductInstance;


import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<ProductInstance> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, ProductInstance.class);
    } 
}
