package org.iotc.emaris.productinstance.data;

import static java.util.stream.Collectors.groupingBy;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.productinstance.ProductInstance;

import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    ProductInstanceDao dao;

    @Inject
    Event<RemovedInstanceEvents.Event<ProductInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    void onProductRemoval(@Observes @Removed Product source) {

        // shouldn't happen but extra safety after being burnt: can't risk a broader filter.
        if (source.id() == null)
            return;

        var filter = ProductInstanceFilter.filter().source(source.id());

        var removed = dao.removeMatching(filter);

        log.info("removed {} product instance(s) following removal of product {}", removed.size(), source.name().inDefaultLanguage());

        // notify removals by campaign.
        removed.stream().collect(groupingBy(t -> t.campaign())).values().forEach(removedByCampaign ->

            removeEvents.select(Removed.cascadeEvent).fire(removeEventFactory.eventFor(removedByCampaign))

        );

    }

}
