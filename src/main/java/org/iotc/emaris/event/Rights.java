package org.iotc.emaris.event;

import static apprise.backend.iam.model.Action.action;
import static org.iotc.emaris.event.Constants.eventType;

import apprise.backend.iam.model.Action;

public class Rights {

    public static final Action manage_events = action("manage").type(eventType).make();
    public static final Action edit_events = action("manage","edit").type(eventType).make();

}
