package org.iotc.emaris.event.data;

import static org.iotc.emaris.event.data.DataConstants.event_details;
import static org.iotc.emaris.event.data.DataConstants.event_table;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.SchemaDao;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class EventSchema {

    @Inject
    Config config;

    @Inject
    SchemaDao schemas;

    @Inject
    DSLContext jooq;

    @Getter
    Table<Record> events;

    @Inject
    @Getter
    Binder<Event> binder;

    @Inject
    javax.enterprise.event.Event<SchemaReady<Event>> fwkevents;

    @Getter
    RecordMapper<Record, Event> mapper = record -> binder.bind(record.get(event_details));

    public boolean isStaged() {

        return schemas.exists(events);

    }

    public void stageTables() {

        events = table(name(config.dbschema(), event_table));

        boolean firstTime = !schemas.exists(events);

        String stmt = jooq.createTableIfNotExists(events)
                .column(DataConstants.event_id, VARCHAR.length(100))
                .column(DataConstants.event_state, VARCHAR.nullable(false))
                .column(DataConstants.event_details, VARCHAR.nullable(false))
                .constraints(
                        constraint(events.getName() + "_pkey").primaryKey(DataConstants.event_id))
                .getSQL();

        jooq.execute(stmt);

        fwkevents.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

    }

}
