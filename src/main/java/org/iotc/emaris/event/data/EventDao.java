package org.iotc.emaris.event.data;

import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.event.data.DataConstants.event_details;
import static org.iotc.emaris.event.data.DataConstants.event_id;
import static org.iotc.emaris.event.data.DataConstants.event_state;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.Record;
import org.jooq.Select;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;

@ApplicationScoped
public class EventDao {

    @Inject
    DSLContext jooq;

    @Inject
    TagInstanceDao tags;

    @Inject
    TagSchema tagschema;

    @Inject
    TagMappers tagmappers;

    @Inject
    EventSchema schema;

    public List<Event> all(EventFilter filter) {

        return fetch(filter.toCondition()).collect(toList());

    }

    public int size(EventFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.events()).where(filter.toCondition()));
    }

    public Event get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown event %s", id)));

    }

    public Optional<Event> lookup(String id) {

        return fetch(event_id.equalIgnoreCase(id)).findFirst();

    }

    public void add(Event event) {

        try {

            add(event, false);

        } catch (RuntimeException dae) {

            map(dae,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("event %s already exists."), event.id())))

            );
        }

    }

    public void add(Event event, boolean ignoreOnDuplicate) {

        InsertOnDuplicateStep<Record> insert = jooq.insertInto(schema.events())
                .set(event_id, event.id())
                .set(event_state, event.state().name())
                .set(event_details, schema.binder().bind(pruned(event)));

        if (ignoreOnDuplicate)
            insert.onDuplicateKeyIgnore();

        int inserted = insert.execute();

        if (inserted > 0)
            tags.replaceTags(event.id(), event.tags());

    }

    public void update(Event event) {

        jooq.update(schema.events())
                .set(event_state, event.state().name())
                .set(event_details, schema.binder().bind(pruned(event)))
				.where(event_id.equalIgnoreCase(event.id()))
				.execute();

		tags.replaceTags(event.id(), event.tags());

    }
    
	public void remove(Event event) {

		jooq.delete(schema.events()).where(event_id.equalIgnoreCase(event.id())).execute();

		tags.removeTags(event.id());

    }

    
    // helpers

    private Stream<Event> fetch(Condition conditions) {

        Select<Record> query = jooq.select()
                .from(schema.events())
                .leftJoin(tagschema.tag_instances()).on(event_id.eq(tagged))
                .where(conditions);

        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    Event event = e.getKey();

                    event.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return event;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private Event pruned(Event event) {

        return event.copy().tags(emptyList());

    }
}
