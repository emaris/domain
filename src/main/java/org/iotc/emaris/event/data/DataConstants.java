package org.iotc.emaris.event.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {

    public static final String event_table = "event";

    public static final Field<String> event_id = field(name(event_table, "id"), String.class);
    public static final Field<String> event_state = field(name(event_table, "state"), String.class);
    public static final Field<String> event_details = field(name(event_table, "details"), String.class);

}
