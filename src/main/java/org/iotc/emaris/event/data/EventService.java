package org.iotc.emaris.event.data;

import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.event.Event.State.inactive;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;

import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class EventService {

    @Inject
    EventDao dao;

    @Inject
    User logged;

    @Inject
    javax.enterprise.event.Event<Event> events;

    public Event add(Event event) {

        if (event.id() == null)
            event.id(Id.mint(eventPrefix));

        if (event.state() == null)
            event.state(inactive);

        event.validateNow(Mode.create);

        event.touchedBy(logged.username());

        log.info("adding event {}", event.ref());

        dao.add(event);

        events.select(Added.event).fire(event);

        return event;
    }

    public Event update(@NonNull Event updated) {

        Event event = dao.get(updated.id());

        event.updateWith(updated);

        event.validateNow(Mode.update);

        event.touchedBy(logged.username());

        log.info("updating event {}", event.ref());

        dao.update(event);

        events.select(Updated.event).fire(event);

        return event;

    }

    public Event remove(@NonNull String id) {

        Event event = dao.get(id);

        event.validateNow(Mode.delete);

        // observers might still want to see who's removing this.
        event.touchedBy(logged.username());

        log.info("removing event {}", event.ref());

        dao.remove(event);

        events.select(Removed.event).fire(event);

        return event;

    }
}
