package org.iotc.emaris.event.data;

import static org.iotc.emaris.event.data.DataConstants.event_state;
import org.iotc.emaris.event.Event;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class EventFilter {

	Event.State state = null;

	public static EventFilter filter() {
		return new EventFilter();
	}

	public static EventFilter withNoConditions() {
		return new EventFilter();
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (state != null)
			condition = condition.and(event_state.eq(state.name()));

		return condition;
	}
}