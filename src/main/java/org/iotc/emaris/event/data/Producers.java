package org.iotc.emaris.event.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.event.Event;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    

    @Produces @ApplicationScoped
    Binder<Event> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, Event.class);
    } 
}
