package org.iotc.emaris.event.stage;

import static apprise.backend.model.Multilingual.text;
import static apprise.backend.model.Multilingual.Language.fr;
import static apprise.backend.tag.stage.StagedData.idformat;
import static java.lang.String.format;
import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.event.Event.Cardinality.any;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.iotc.emaris.event.Event;

import apprise.backend.data.Staged;

@ApplicationScoped
public class StagedData {

        @Produces
        @Staged
        public static Event reminder = new Event()
                        .id(format(idformat, eventPrefix, eventType, "reminder"))
                        .type(eventType)
                        .cardinality(any)
                        .predefined(true)
                        .managed(true)
                        .name(text().inDefaultLanguage("Reminder").in(fr, "Rappel"))
                        .description(text().inDefaultLanguage("A reminder about the occurrence of an event.").in(fr,
                                        "Un rappel sur la survenance d'un événement."));
}
