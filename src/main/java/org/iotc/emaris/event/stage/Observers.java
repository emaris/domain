package org.iotc.emaris.event.stage;

import static apprise.backend.iam.stage.StagedData.bootadmin;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.EventDao;

import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.Staged;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    EventDao dao;




    void stageAllOnFirstStart(@Observes @Added SchemaReady<Event> event, @Staged Instance<Event> singletons, @Staged Instance<List<Event>> groups) {

        stageEvents(singletons, groups,true);
    }

    void stagePredefineOnStart(@Observes @Updated SchemaReady<Event> event, @Staged Instance<Event> singletons, @Staged Instance<List<Event>> groups) {

        stageEvents(singletons, groups,false);
    }


    void stageEvents(@Staged Instance<Event> singletons, Instance<List<Event>> groups, boolean firstStart ) {

        Predicate<Event> selected = r -> firstStart ? true : r.predefined();

        List<Event> events = singletons.stream().filter(selected).collect(toList());

        events.addAll(groups.stream()
            .flatMap(t -> t.stream())
            .filter(selected)
            .collect(toList()));

        if (events.size() > 0) {
           
            log.info("staging events ({})", events.stream().map(c -> c.id()).collect(joining(" , ")));
           
            events.stream()
                    .map(r -> r.state(Event.State.active).touchedBy(bootadmin.username()))
                    .forEach(r -> dao.add(r, true));
        }
    }

}
