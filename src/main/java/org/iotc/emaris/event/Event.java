package org.iotc.emaris.event;

import static java.lang.String.format;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.event.Event.State.active;
import static org.iotc.emaris.event.Event.State.inactive;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import apprise.backend.model.Bag;
import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.model.Predefined;
import apprise.backend.tag.TagExpression;
import apprise.backend.tag.Tagged;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Models an event independently from its occurrences.
 * <p/>
 * May optionally be about objects of a <em>target type</em>, possibly other
 * events in turn (<em>meta-events</em>).
 * <p/>
 * May be <em>managed</em>, ie. to be generated and removed by the system in
 * correspondence with target objects, possibly only when the objects match a
 * {@linkplain} {@link TagExpression} (<em>managed expression</em>). Meta-events
 * may also be managed conditionally to the target type of their own targets
 * (<em>managed types</em>).
 */

@Data
@NoArgsConstructor
public class Event implements Lifecycled<Event.State, Event>, Predefined<Event.Predefined,Event>, Tagged<Event>, Validated, Comparable<Event> {

    public static enum State {
        active, inactive
    }

    public static enum Predefined {

        guarded, cardinality
    }

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Cardinality {

		public static final Cardinality any = new Cardinality(1,null);
		public static final Cardinality one = new Cardinality(1, 1);
		public static final Cardinality initiallyOne = new Cardinality(1, null);

		Integer initial;
		Integer max;

		public Cardinality(Cardinality other) {
			initial(other.initial()).max(other.max());
		}
	}

    String id;

    boolean guarded;
   
    boolean predefined;
    Set<Predefined> predefinedProperties = new HashSet<>();

    Lifecycle<State> lifecycle = new Lifecycle<>(inactive);

    Multilingual name = new Multilingual();
    Multilingual description = new Multilingual();

    String type;

    Cardinality cardinality = Cardinality.one;
    
    boolean managed;

    // managed if target matches the expression
    TagExpression managedExpression = new TagExpression();

    // for meta-events only: managed if target has this type (only for metaevents,
    // when type=event)
    List<String> managedTypes = new ArrayList<>();

    Set<String> tags = new HashSet<>();

    Bag properties = new Bag();

    public Event tags(Collection<String> tags) {
        this.tags = new HashSet<>(tags);
        return this;
    }

    public String ref() {
        return format("%s (%s)", name.inDefaultLanguage(), id);
    }

    public Event(Event other) {

        updateWith(other)
                .id(other.id())
                .cardinality(predefined(Predefined.cardinality) ? this.cardinality() : new Cardinality(other.cardinality()))
                .predefined(other.predefined())
                .predefinedProperties(new HashSet<>(other.predefinedProperties()))
                .guarded(other.guarded())
                .tags(other.tags())
                .type(other.type)
                .managed(other.managed())
                .managedTypes(new ArrayList<>(other.managedTypes()))
                .managedExpression(new TagExpression(other.managedExpression()))
                .lifecycle(new Lifecycle<>(other.lifecycle()));

    }

    public Event copy() {

        return new Event(this);

    }

    public Event updateWith(Event other) {

        // these can change at all times
        name(new Multilingual(other.name()))
                .description(new Multilingual(other.description()))
                .properties(new Bag(other.properties()));

        guarded(predefined(Predefined.guarded) ? this.guarded() : other.guarded());

        if (!this.guarded())
            tags(other.tags())
                    .cardinality(predefined(Predefined.cardinality) ? this.cardinality() : new Cardinality(other.cardinality()))
                    .managed(other.managed())
                    .managedTypes(new ArrayList<>(other.managedTypes()))
                    .managedExpression(new TagExpression(other.managedExpression()))
                    .lifecycle().updateWith(other.lifecycle());

        return this;

    }

    @Override
    public List<String> validate(Mode mode) {

        return CheckDsl.given(this)
                .check($ -> id != null).or("missing identifier")
                .provided(name != null)
                .check($ -> name.inDefaultLanguage() != null).or("no name in default language")
                .provided(mode==Mode.create)
                .check($->!predefined()).or("invalid state: predefined")
                .provided(mode == Mode.delete)
                .check($ -> !predefined() && !guarded() && state() != active)
                .or("cannot remove event %s because it is active, predefined, or protected", ref())
                .provided(managedTypes() != null & !managedTypes().isEmpty())
                .check($ -> type.equals(eventType))
                .or("managed types aren't support for target type", type())
                .andCollect();
    }

    @Override
    public int compareTo(Event o) {
        return name().compareTo(o.name());
    }
}
