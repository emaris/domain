package org.iotc.emaris.event.api;

import static java.lang.Integer.MAX_VALUE;
import static org.iotc.emaris.event.Rights.manage_events;
import static org.iotc.emaris.event.Rights.edit_events;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;

import apprise.backend.iam.model.User;

@Decorator
@Priority(MAX_VALUE)
public abstract class LifecycleSecureApi implements LifecycleApi {

    @Inject
    @Delegate
    LifecycleApi unsecure;

    @Inject
    User logged;

    @Override
    public Event addOne(Event event) {

        logged.assertCan(manage_events);

        return unsecure.addOne(event);
    }

    @Override
    public Event updateOne(String id, Event event) {

        logged.assertCan(edit_events.on(id));

        return unsecure.updateOne(id, event);
    }


    @Override
	public void removeOne(String id) {
	
		logged.assertCan(manage_events.on(id));
		
		unsecure.removeOne(id);
	}

}
