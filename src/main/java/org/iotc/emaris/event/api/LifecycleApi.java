package org.iotc.emaris.event.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.event.api.ApiConstants.eventapi;
import static org.iotc.emaris.event.data.EventFilter.withNoConditions;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.data.EventDao;
import org.iotc.emaris.event.data.EventService;

import lombok.NonNull;

public interface LifecycleApi {

	@Operation(summary = "Returns all events")

	List<Event> fetchAll();


	@Operation(summary="Returns one event")

	Event fetchOne(String rid);


	@Operation(summary="Creates a new event", description="Requires management rights")
	Event addOne(Event event);


	@Operation(summary="Updates a event",description="Requires management rights")
	
	Event updateOne(String rid, Event event);

	
	@Operation(summary="Deletes a event",description="Requires management rights")
	void removeOne(String rid);





	@Path(eventapi)
	public class Default implements LifecycleApi {

		@Inject
		EventDao dao;

		@Inject
		EventService service;

		@GET
		@Produces(APPLICATION_JSON)
		@Override
		public List<Event> fetchAll() {
			
			return dao.all(withNoConditions());

		}

		@GET
		@Produces(APPLICATION_JSON)
		@Path("{eid}")
		@Override
		public Event fetchOne(@NonNull @PathParam("eid") String id) {
			
			return dao.get(id);
		
		}


		@POST
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Event addOne(@NonNull Event event) {
			
			return service.add(event);
		}


		@PUT
		@Path("{rid}")
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Event updateOne(@NonNull @PathParam("rid") String tid, Event event) {
			
			if (!event.id().equals(tid))
				throw new IllegalArgumentException(format("event id '%s' does not match id in payload ('%s')",tid, event.id()));
			
			return service.update(event);
		}


		@DELETE 
		@Path("{eid}") 
		@Override 
		public void removeOne(@NonNull @PathParam("eid") String id) {
			
			service.remove(id);
			
		}
	}
}


