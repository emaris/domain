package org.iotc.emaris.event.push;

import org.iotc.emaris.event.Event;

import apprise.backend.event.StandardEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class EventChange extends StandardEvent<EventChange> {

    public static enum Type { add, remove, change }

    Event event;
    Type type;

}

