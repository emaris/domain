package org.iotc.emaris.event.push;

import static org.iotc.emaris.event.Constants.eventType;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;

import org.iotc.emaris.event.Event;

import apprise.backend.event.EventBus;
import apprise.backend.iam.model.User;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;

@ApplicationScoped
public class Observers {

    public static final String topic = eventType;

    @Inject
    User logged;

    @Inject
    EventBus bus;

    void onAdd(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Added Event event) {

        onEvent(new EventChange().type(EventChange.Type.add).event(event));
    }

    void onUpdate(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated Event event) {

        onEvent(new EventChange().type(EventChange.Type.change).event(event));
    }

    void onRemove(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed Event event) {

        onEvent(new EventChange().type(EventChange.Type.remove).event(event));
    }   

    void onEvent( EventChange change) {

        bus.instanceFor(topic).publish(change.origin(logged.username()));
    }
    
}
