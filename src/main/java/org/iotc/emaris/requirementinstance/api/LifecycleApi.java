package org.iotc.emaris.requirementinstance.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.byref;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.campaign;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.requirementinstanceapi;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.remove;
import static org.iotc.emaris.requirementinstance.api.ApiConstants.search;
import static org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceService;

import lombok.NonNull;

public interface LifecycleApi {

    @Operation(summary = "Returns the first few requirement instances")

    List<RequirementInstance> fetchAll();

    @Operation(summary = "Returns all the requirement instances that match a filter")

    List<RequirementInstance> fetchAll(RequirementInstanceFilter filter);

    @Operation(summary = "Returns all the requirement instances in a given campaign")

    List<RequirementInstance> fetchByCampaign(String cid);

    @Operation(summary = "Returns a requirement instance with a given identifier or reference")

    RequirementInstance fetchOne(String pid, boolean ref);

    @Operation(summary = "Adds one or more requirement instances", description = "Requires management rights")
    List<RequirementInstance> addmany(List<RequirementInstance> instances);

    @Operation(summary = "Updates a requirement instance", description = "Requires management rights")

    RequirementInstance updateOne(String pid, RequirementInstance instance);

    @Operation(summary = "Deletes a requirement instances", description = "Requires management rights")
    void removeOne(String pid);

    @Operation(summary = "Deletes one or more requirement instances", description = "Requires management rights")

    void removeMany(List<String> pids);

    @Path(requirementinstanceapi)
    public class Default implements LifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        RequirementInstanceDao dao;

        @Inject
        RequirementInstanceService service;

        @POST
        @Path(search)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<RequirementInstance> fetchAll(RequirementInstanceFilter filter) {

            return dao.allMatching(filter);

        }

        @GET
        @Produces(APPLICATION_JSON)
        @Override
        public List<RequirementInstance> fetchAll() {
            return dao.allMatching(filter().limit(fetchAllLimit));
        }

        @GET
        @Path(campaign + "/{cid}")
        @Override
        public List<RequirementInstance> fetchByCampaign(@NonNull @PathParam("cid") String cid) {
            return dao.allMatching(RequirementInstanceFilter.filter().campaign(cid));
        }

        @GET
        @Produces(APPLICATION_JSON)
        @Path("{pid}")
        @Override
        public RequirementInstance fetchOne(@NonNull @PathParam("pid") String pid, @QueryParam(byref) boolean ref) {

            return ref ? dao.get(new CampaignInstance.InstanceRef(pid)) : dao.get(pid);

        }

        @Override
        @POST
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        public List<RequirementInstance> addmany(@NonNull List<RequirementInstance> instances) {

            return service.add(instances);

        }

        @PUT
        @Path("{pid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public RequirementInstance updateOne(@NonNull @PathParam("pid") String pid, @NonNull RequirementInstance instance) {

            if (!pid.equals(instance.id()))
                throw new IllegalArgumentException(
                        format("instance id '%s' does not match id in payload ('%s')", pid, instance.id()));

            return service.update(instance);
        }

        @DELETE
        @Path("{pid}")
        @Override
        public void removeOne(@NonNull @PathParam("pid") String pid) {

            service.remove(pid);
        }

        @POST
        @Path(remove)
        @Override
        public void removeMany(@NonNull List<String> pids) {
           
            service.remove(pids);
        }

    }
		
}


