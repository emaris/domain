package org.iotc.emaris.requirementinstance;

import static org.iotc.emaris.requirement.Constants.requirementPrefix;

public class Constants {

    public static final String requirementinstancePrefix = requirementPrefix + "I";
}
