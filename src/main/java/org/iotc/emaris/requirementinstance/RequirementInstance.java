package org.iotc.emaris.requirementinstance;

import static org.iotc.emaris.requirement.Constants.requirementType;

import java.util.Optional;

import org.iotc.emaris.campaign.CampaignInstance.Base;
import org.iotc.emaris.common.TenantAudience;

import apprise.backend.tag.TagExpression;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RequirementInstance extends Base<RequirementInstance> {

    TagExpression audience;
    TenantAudience audienceList;
    TagExpression userProfile;

    
    public RequirementInstance() {
        super(requirementType);
    }

    public RequirementInstance(RequirementInstance other) {
        super(other);
    }

    @Override
    public RequirementInstance copy() {
        return new RequirementInstance(this);
    }

    @Override
    public RequirementInstance updateWith(RequirementInstance other) {

        super.updateWith(other);

        if (other.audience().isPresent())
            audience(other.audience().get());

        if (other.audienceList().isPresent())
            audienceList(new TenantAudience(other.audienceList().get()));

        if (other.userProfile().isPresent())
            userProfile(other.userProfile().get());

        return this;
    }

    public Optional<TagExpression> audience() {

        return Optional.ofNullable(audience);
    }

    public RequirementInstance audience(TagExpression audience) {
        this.audience = audience;
        return this;
    }

    public Optional<TenantAudience> audienceList() {

        return Optional.ofNullable(audienceList);
    }

    public RequirementInstance audienceList(TenantAudience audienceList) {
        this.audienceList = audienceList;
        return this;
    }


    public Optional<TagExpression> userProfile() {

        return Optional.ofNullable(userProfile);
    }

    public RequirementInstance userProfile(TagExpression profile) {
        this.userProfile = profile;
        return this;
    }
}
