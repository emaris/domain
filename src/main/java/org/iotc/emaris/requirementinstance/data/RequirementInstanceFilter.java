package org.iotc.emaris.requirementinstance.data;

import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_campaign;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_id;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_source;

import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class RequirementInstanceFilter {

	String campaign;
	String source;

	int limit;

	List<String> ids;
	
	public static RequirementInstanceFilter filter() {
		return new RequirementInstanceFilter();
	}

	public static RequirementInstanceFilter withNoConditions() {
		return new RequirementInstanceFilter();
	}

	public boolean isEmpty() {

		return campaign == null && source == null;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (campaign != null)
			condition = condition.and(requirementinstance_campaign.eq(campaign));

		if (source != null)
			condition = condition.and(requirementinstance_source.eq(source));
		
		if (ids != null && ids.size() > 0)
			condition = condition.and(requirementinstance_id.in(ids));

		return condition;
	} 
}