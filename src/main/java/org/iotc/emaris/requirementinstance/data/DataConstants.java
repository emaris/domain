package org.iotc.emaris.requirementinstance.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {

    public static final String requirementinstance_table  = "requirementinstance";

    public static final Field<String> requirementinstance_id = field(name(requirementinstance_table, "id"), String.class);
    public static final Field<String> requirementinstance_campaign = field(name(requirementinstance_table,"campaign"), String.class);
    public static final Field<String> requirementinstance_source = field(name(requirementinstance_table,"source"), String.class);
	public static final Field<String> requirementinstance_details = field(name(requirementinstance_table, "details"), String.class);
    
}