package org.iotc.emaris.requirementinstance.data;

import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_campaign;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_details;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_id;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_source;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_table;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class RequirementInstanceSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        CampaignSchema campaignSchema;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> requirementinstances;

        @Inject
        @Getter
        Binder<RequirementInstance> binder;

        @Inject
        Event<SchemaReady<RequirementInstance>> events;

        @Getter
        RecordMapper<Record, RequirementInstance> mapper = record ->

        binder.bind(record.get(requirementinstance_details))
                        .source(record.get(requirementinstance_source))
                        .campaign(record.get(requirementinstance_campaign));

        public void stageTables() {

                requirementinstances = table(name(config.dbschema(), requirementinstance_table));

                boolean firstTime = !schemas.exists(requirementinstances);

                String stmt = jooq.createTableIfNotExists(requirementinstances)
                                .column(requirementinstance_id, VARCHAR.length(100))
                                .column(requirementinstance_campaign, VARCHAR.nullable(false))
                                .column(requirementinstance_source, VARCHAR.nullable(false))
                                .column(requirementinstance_details, VARCHAR.nullable(false))
                                .constraints(
                                                constraint(requirementinstances.getName() + "_pkey")
                                                                .primaryKey(requirementinstance_id),
                                                constraint(requirementinstances.getName() + "_ref")
                                                                .unique(requirementinstance_campaign, requirementinstance_source),
                                                constraint(requirementinstances.getName() + "campaign_fkey")
                                                                .foreignKey(requirementinstance_campaign)
                                                                .references(campaignSchema.campaigns(), campaign_id)
                                                                .onDeleteCascade())

                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(requirementinstances.getName()+ "_campaign_index").on(requirementinstances, requirementinstance_campaign).execute();
                jooq.createIndexIfNotExists(requirementinstances.getName()+ "_source_index").on(requirementinstances, requirementinstance_source).execute();

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }

}
