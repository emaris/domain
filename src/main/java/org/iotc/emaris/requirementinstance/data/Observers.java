package org.iotc.emaris.requirementinstance.data;

import static java.util.stream.Collectors.groupingBy;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirementinstance.RequirementInstance;

import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    RequirementInstanceDao dao;

    @Inject
    Event<RemovedInstanceEvents.Event<RequirementInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;


    void onRequirementRemoval(@Observes @Removed Requirement source) {

        // shouldn't happen but extra safety after being burnt: can't risk a broader filter.
        if (source.id() == null)
            return;

        var filter = RequirementInstanceFilter.filter().source(source.id());

        var removed = dao.removeMatching(filter);

        log.info("removed {} requirement instance(s) following removal of requirement {}", removed.size(), source.name().inDefaultLanguage());

        // notify removals by campaign.
        removed.stream().collect(groupingBy(t -> t.campaign())).values().forEach(removedByCampaign ->

            removeEvents.select(Removed.cascadeEvent).fire(removeEventFactory.eventFor(removedByCampaign))

        );

     
    }



}