package org.iotc.emaris.requirementinstance.data;

import static org.iotc.emaris.requirementinstance.Constants.requirementinstancePrefix;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.requirementinstance.RequirementInstance;

import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class RequirementInstanceService {

    @Inject
    RequirementInstanceDao dao;

    @Inject
    Event<List<RequirementInstance>> bulkEvents;

    @Inject
    Event<RequirementInstance> events;

    @Inject
    Event<RemovedInstanceEvents.Event<RequirementInstance>> removeEvents;

    @Inject
    RemovedInstanceEvents removeEventFactory;

    public List<RequirementInstance> add(@NonNull List<RequirementInstance> instances) {

        instances.stream().filter(i -> i.id() == null).forEach(i -> i.id(Id.mint(requirementinstancePrefix)));

        Validated.validateNow(instances, Mode.create);

        log.info("adding {} requirement instances", instances.size());

        dao.add(instances);

        bulkEvents.select(Added.event).fire(instances);

        return instances;
    }

    public RequirementInstance update(@NonNull RequirementInstance updated) {

        RequirementInstance instance = dao.get(updated.id());

        instance.updateWith(updated);

        instance.validate(Mode.update);

        log.info("updating requirement instance {}", instance.ref());

        dao.update(instance);

        events.select(Updated.event).fire(instance);

        return instance;
    }

    public RequirementInstance remove(@NonNull String id) {

        RequirementInstance instance = dao.get(id);

        instance.validateNow(Mode.delete);

        log.info("removing requirement instance {}", instance.ref());

        dao.remove(instance);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(List.of(instance)));

        return instance;

    }

    public void remove(@NonNull List<String> ids) {

        var instances = dao.allMatching((RequirementInstanceFilter.filter().ids(ids)));

        var removed = dao.remove(ids);

        log.info("removed {} requirement instances", removed);

        removeEvents.select(Removed.event).fire(removeEventFactory.eventFor(instances));

    }

}
