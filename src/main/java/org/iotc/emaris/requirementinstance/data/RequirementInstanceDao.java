package org.iotc.emaris.requirementinstance.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_id;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_campaign;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_details;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_id;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_source;
import static org.iotc.emaris.requirementinstance.data.DataConstants.requirementinstance_table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance.InstanceRef;
import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementSchema;
import org.iotc.emaris.requirementinstance.RequirementInstance;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;
import org.jooq.exception.DataAccessException;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.data.ErrorCode;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;

@ApplicationScoped
public class RequirementInstanceDao {

    private static final int max_on_empty = 500;

    @Inject
    DSLContext jooq;

    @Inject
    RequirementInstanceSchema schema;

    @Inject
    RequirementSchema requirementSchema;

    @Inject
    TagMappers tagmappers;

    @Inject
    TagSchema tagschema;

    @Inject
    TagInstanceDao tags;

    public List<RequirementInstance> allMatching(RequirementInstanceFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit()).collect(toList());

    }

    public List<String> usage(String source) {

        return jooq.selectDistinct()
                 .from(schema.requirementinstances())
                 .where(DataConstants.requirementinstance_source.eq(source))
                 .fetch(DataConstants.requirementinstance_campaign);
        
    }

    public List<Requirement> allSourcesMatching(RequirementInstanceFilter filter) {

        var query = jooq.select().from(requirementSchema.requirements())
                            .join(schema.requirementinstances().where(filter.toCondition()))
                            .on(requirementinstance_source.eq(requirement_id));
                           
        return query.fetch(requirementSchema.mapper());
    }

    public int sizeMatching(RequirementInstanceFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.requirementinstances()).where(filter.toCondition()));
    }

    public RequirementInstance get(String pid) {

        return lookup(pid).orElseThrow(() -> new NoSuchEntityException(format("unknown requirement instance %s", pid)));

    }

    public Optional<RequirementInstance> lookup(String pid) {

        return fetch(requirementinstance_id.equalIgnoreCase(pid)).findFirst();

    }

    public RequirementInstance get(InstanceRef ref) {

        return lookup(ref).orElseThrow(() -> new NoSuchEntityException(format("unknown requirement instance %s", ref)));

    }

    public Optional<RequirementInstance> lookup(InstanceRef ref) {

        return fetch(

                requirementinstance_campaign.equalIgnoreCase(ref.campaign())
                        .and(requirementinstance_source.equalIgnoreCase(ref.source())))
                                .findFirst();

    }

    public void add(List<RequirementInstance> instances) {

        Collection<Query> stmts = new ArrayList<>();

        for (RequirementInstance instance : instances)
            stmts.add(jooq.insertInto(schema.requirementinstances())
                    .columns(requirementinstance_id,
                            requirementinstance_campaign,
                            requirementinstance_source,
                            requirementinstance_details)
                    .values(instance.id(),
                            instance.campaign(),
                            instance.source(),
                            schema.binder().bind(pruned(instance))));

        try {

            jooq.batch(stmts).execute();

        } catch (DataAccessException dae) {

            ErrorCode.map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(
                                    ("instances refer to resources that don't exist (campaign,source,lineage).")))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(("duplicate instances.")))

            );
        }

        tags.addTags(instances, i -> i.id());

    }

    public void update(RequirementInstance instance) {

        jooq.update(schema.requirementinstances())
                .set(requirementinstance_details, schema.binder().bind(pruned(instance)))
                .where(requirementinstance_id.equalIgnoreCase(instance.id()))
                .execute();

        tags.replaceTags(instance.id(), instance.tags());

    }

    public void remove(RequirementInstance instance) {

        jooq.delete(schema.requirementinstances()).where(requirementinstance_id.equalIgnoreCase(instance.id())).execute();

        tags.removeTags(instance.id());

    }

    public int remove(List<String> ids) {

        var removed = jooq.delete(schema.requirementinstances()).where(requirementinstance_id.in(ids)).execute();

        tags.removeTags(ids);

        return removed;

    }

    public List<RequirementInstance> removeMatching(RequirementInstanceFilter filter) {

        List<RequirementInstance> removed = jooq.select().from(schema.requirementinstances()).where(filter.toCondition()).fetch(schema.mapper());

        List<String> removedIds = removed.stream().map( i -> i.id()).collect((toList()));

        jooq.delete(schema.requirementinstances()).where(filter.toCondition()).execute();

        tags.removeTags(removedIds);

        return removed;

    }

    // helpers

    private Stream<RequirementInstance> fetch(Condition conditions) {

        return fetch(conditions, 0);
    }

    private Stream<RequirementInstance> fetch(Condition conditions, int limit) {

        SelectConditionStep<Record> select = jooq.select().from(schema.requirementinstances()).where(conditions);

        Table<Record> selectedInstances =  (limit > 0 ? select.limit(limit) : select).asTable(requirementinstance_table);

        Select<Record> query = jooq.select().from(selectedInstances).leftJoin(tagschema.tag_instances()).on(requirementinstance_id.eq(tagged));

       
        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    RequirementInstance requirement = e.getKey();

                    requirement.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return requirement;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private RequirementInstance pruned(RequirementInstance instance) {

        return instance.copy().tags(emptyList()).campaign(null).source(null);

    }

}
