package org.iotc.emaris.requirementinstance.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.requirementinstance.RequirementInstance;


import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<RequirementInstance> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, RequirementInstance.class);
    } 
}
