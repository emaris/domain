package org.iotc.emaris.submission;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import apprise.backend.model.Bag;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class Trail implements Validated {

    @Data
    @NoArgsConstructor
    public static class Key implements Validated {

        String campaign, party, asset, assetType;

        public String toString() {
            return format("%s:%s:(%s) %s", campaign, party, assetType, asset);
        }

        public List<String> validate() {

            return CheckDsl.given(this)
                    .check($ -> campaign != null).or("missing campaign")
                    .check($ -> party != null).or("missing party")
                    .check($ -> asset != null).or("missing asset")
                    .check($ -> assetType != null).or("missing type")
                    .andCollect();

        }
    }

    String id;

    Key key;

    List<Submission> submissions = new ArrayList<>();

    Bag properties = new Bag();

    public String ref() {

        return format("%s (%s)", key, id);
    }

    public Trail updateWith(Trail other) {

        properties(new Bag(other.properties()));

        return this;

    }

    @Override
    public List<String> validate() {

        return CheckDsl.given(this)
                .check($ -> id != null).or("missing identifier")
                .check($ -> key != null).or("missing key")
                .provided(key != null)
                .check(key)
                .andCollect();
    }

}
