package org.iotc.emaris.submission.api;


public class ApiConstants {
	
	public static final String trailapi = "trail";
	public static final String submissionapi = "submission";
	public static final String search = "search";
	public static final String published = "published";

}
