package org.iotc.emaris.submission.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.submission.api.ApiConstants.search;
import static org.iotc.emaris.submission.api.ApiConstants.submissionapi;
import static org.iotc.emaris.submission.data.AccessMode.stateful;
import static org.iotc.emaris.submission.data.SubmissionFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.data.SubmissionDao;
import org.iotc.emaris.submission.data.SubmissionFilter;
import org.iotc.emaris.submission.data.SubmissionService;

import lombok.NonNull;

public interface SubmissionLifecycleApi {

    @Operation(summary = "Returns the first few submission")

    List<Submission> fetchAll();

    @Operation(summary = "Returns the first submissions that match a given filter")

    List<Submission> allMatching(SubmissionFilter filter);

    @Operation(summary = "Returns the submission with a given identifier")

    Submission fetchOne(String sid);

    @Operation(summary = "Adds a submission")

    Submission addOne(Submission submission);

    @Operation(summary = "Updates a submission")

    Submission updateOne(String sid, Submission submission);

    List<Submission> updateShallow(List<Submission> submission);

    @Operation(summary = "Removes the trail with a given identifier")

    void removeOne(String sid);

    @Path(submissionapi)
    public class Default implements SubmissionLifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        SubmissionService service;

        @Inject
        SubmissionDao dao;

        @GET
        @Override
        public List<Submission> fetchAll() {
            return dao.allMatching(filter().limit(fetchAllLimit));
        }

        @POST
        @Path(search)
        @Consumes(APPLICATION_JSON)
        @Produces(APPLICATION_JSON)
        @Override
        public List<Submission> allMatching(SubmissionFilter filter) {
            return dao.allMatching(filter);
        }

        @GET
        @Path("{sid}")
        @Produces(APPLICATION_JSON)
        @Override
        public Submission fetchOne(@NonNull @PathParam("sid") String sid) {
            return dao.get(sid, stateful);
        }

        @POST
        @Consumes(APPLICATION_JSON)
        @Override
        public Submission addOne(Submission submission) {
            return service.add(submission);
        }

        @PUT
        @Path("{sid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public Submission updateOne(@NonNull @PathParam("sid") String sid, Submission submission) {

            if (!sid.equals(submission.id()))
                throw new IllegalArgumentException(
                        format("submission id '%s' does not match id in payload ('%s')", sid, submission.id()));

            return service.update(submission);
        }

        @PUT
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<Submission> updateShallow(@NonNull List<Submission> submissions) {

            return service.updateShallow(submissions);
        }

        @DELETE
        @Path("{sid}")
        @Override
        public void removeOne(@NonNull @PathParam("sid") String sid) {

            service.remove(sid);

        }

    }

}
