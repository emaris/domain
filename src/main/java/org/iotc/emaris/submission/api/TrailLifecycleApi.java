package org.iotc.emaris.submission.api;

import static java.lang.String.format;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.submission.api.ApiConstants.published;
import static org.iotc.emaris.submission.api.ApiConstants.search;
import static org.iotc.emaris.submission.api.ApiConstants.trailapi;
import static org.iotc.emaris.submission.data.TrailFilter.filter;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.TrailDao;
import org.iotc.emaris.submission.data.TrailFilter;
import org.iotc.emaris.submission.data.TrailService;
import org.iotc.emaris.submission.data.TrailService.KeyAndSubmission;
import org.iotc.emaris.submission.data.TrailService.TrailAndSubmission;

import apprise.backend.iam.admin.data.TenantDao;
import apprise.backend.iam.admin.data.TenantSummary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

public interface TrailLifecycleApi {

    @Operation(summary = "Returns the first few trails")

    List<Trail> fetchAll();

    @Operation(summary = "Returns all trails that match a filter")

    List<Trail> fetchAll(TrailFilter filter);

    @Data
    @NoArgsConstructor
    @AllArgsConstructor

    static class TrailClosure {

        List<Trail> trails;
        List<TenantSummary> tenants;
    }

    @Operation(summary = "Returns all trails that match a filter, but incldues only pubnlished submissions.")

    TrailClosure fetchPublished(TrailFilter filter);

    @Operation(summary = "Returns the trail with a given identifier")

    Trail fetchOne(String tid);

    @Operation(summary = "Adds a trail with a first submission")

    TrailAndSubmission addOne(@NonNull KeyAndSubmission keyAndSubmission);

    @Operation(summary = "Updates a trail with a given identifier, or creates it if it doesnn't exist")

    Trail updateOne(String tid, Trail trail);

    @Path(trailapi)
    public class Default implements TrailLifecycleApi {

        private static final int fetchAllLimit = 20;

        @Inject
        TrailDao dao;

        @Inject
        TenantDao tenants;

        @Inject
        TrailService service;

        @GET
        @Produces(APPLICATION_JSON)
        @Override
        public List<Trail> fetchAll() {

            var trails = dao.allMatching(filter().limit(fetchAllLimit));

            // prunes content.
            trails.stream().flatMap(t->t.submissions().stream()).forEach(sub -> sub.content(null));


            return trails;
        }

        @POST
        @Path(search)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public List<Trail> fetchAll(@NonNull TrailFilter filter) {

            var trails = dao.allMatching(filter);

            // prunes content.
            trails.stream().flatMap(t->t.submissions().stream()).forEach(sub -> sub.content(null));

            return trails;

        }

        @POST
        @Path(published)
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public TrailClosure fetchPublished(@NonNull TrailFilter filter) {
            return new TrailClosure(dao.allPublished(filter), tenants.allSummaries());
        }

        @GET
        @Path("{tid}")
        @Produces(APPLICATION_JSON)
        @Override
        public Trail fetchOne(@NonNull @PathParam("tid") String tid) {
            return dao.get(tid);
        }

        @POST
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public TrailAndSubmission addOne(@NonNull KeyAndSubmission keyAndSubmission) {

            return service.addWith(keyAndSubmission);
        }

        @PUT
        @Path("{tid}")
        @Produces(APPLICATION_JSON)
        @Consumes(APPLICATION_JSON)
        @Override
        public Trail updateOne(@NonNull @PathParam("tid") String tid, Trail trail) {

            if (!tid.equals(trail.id()))
                throw new IllegalArgumentException(
                        format("trail id '%s' does not match id in payload ('%s')", tid, trail.id()));

            return service.update(trail);
        }

    }

}
