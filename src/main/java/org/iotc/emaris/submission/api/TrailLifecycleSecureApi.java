package org.iotc.emaris.submission.api;

import static apprise.backend.iam.Rights.manage_tenants;
import static java.lang.Integer.MAX_VALUE;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.iotc.emaris.submission.Trail;

import apprise.backend.iam.model.User;

@Decorator
@Priority(MAX_VALUE)
public abstract class TrailLifecycleSecureApi implements TrailLifecycleApi {

    @Inject
    @Delegate
    TrailLifecycleApi unsecure;

    @Inject
    User logged;

    public Trail updateOne(String tid, Trail trail) {

        // tenant managers and, for now, all tenant users can change trail
        if (trail.key().party() != null && !trail.key().party().equals(logged.tenant().value()))
            logged.assertCan(manage_tenants.on(trail.key().party()));

        return unsecure.updateOne(tid, trail);
    }


}
