package org.iotc.emaris.submission;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.submission.Submission.SubmissionState.draft;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import apprise.backend.bytestream.model.Bytestreamed;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.tag.Tagged;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
public class Submission implements Lifecycled<Submission.SubmissionState, Submission>, Bytestreamed, Validated {

    public static enum SubmissionState {

        draft, pending, submitted, managed, published, missing;
    }

    @Data
    @EqualsAndHashCode(of = { "state", "message" })
    @NoArgsConstructor
    public static class ComplianceState {

        String state;
        String message;
        String timeliness;
        Multilingual officialObservation;
        OffsetDateTime lastAssessed;
        String lastAssessedBy;

        public ComplianceState(ComplianceState other) {

            updateWith(other);
        }

        // full copy
        ComplianceState copy(ComplianceState other) {

            return updateWith(other).lastAssessed(other.lastAssessed()).lastAssessedBy(other.lastAssessedBy());
        }

        public ComplianceState updateWith(ComplianceState other) {

            return state(other.state()).message(other.message()).officialObservation(other.officialObservation())
                    .timeliness(other.timeliness());

        }

    }

    @Data
    @NoArgsConstructor
    @EqualsAndHashCode(of = "compliance", callSuper = true)
    @ToString(callSuper = true)
    public static class Lifecycle extends apprise.backend.model.Lifecycle<SubmissionState> implements Tagged<Lifecycle> {

        OffsetDateTime firstEdited;

        OffsetDateTime lastEdited;
        String lastEditedBy;

        OffsetDateTime lastSubmitted;
        String lastApprovedBy;

        OffsetDateTime lastPublished;
        String lastPublishedBy;

        String changeRequest;
        Multilingual reference;
        Multilingual publication;

        ComplianceState compliance = new ComplianceState();

        ComplianceState revokedCompliance = new ComplianceState();

        Set<String> tags = new HashSet<>();

        @Override
        public Lifecycle tags(Collection<String> tags) {
                this.tags = new HashSet<>(tags);
               return this;
        }


        public Lifecycle(SubmissionState initial) {
            super(initial);
        }

        public Lifecycle(Lifecycle other) {
            updateWithSubmission(other)
                    .firstEdited(other.firstEdited == null ? null : other.firstEdited())
                    .lastEdited(other.lastEdited())
                    .lastEditedBy(other.lastEditedBy())
                    .lastApprovedBy(other.lastApprovedBy())
                    .tags(other.tags())
                    .lastSubmitted(other.lastSubmitted());
        }

        private Lifecycle updateWithSubmission(Lifecycle other) {
            updateWith(other)
                    .firstEdited(other.firstEdited == null ? null : other.firstEdited())
                    // client handles publication so we copy it
                    .lastPublished(other.lastPublished())
                    .lastPublishedBy(other.lastPublishedBy())
                    .changeRequest(other.changeRequest())
                    .reference(other.reference())
                    .tags(other.tags())
                    .publication(other.publication());
            
              compliance().updateWith(other.compliance());    
              revokedCompliance(new ComplianceState().copy(other.revokedCompliance()));        

            return this;

        }
    }

    String id;

    String trail;

    Lifecycle lifecycle = new Lifecycle(draft);

    SubmissionContent content = new SubmissionContent();

    public Submission(Submission other) {

        updateWith(other).id(other.id()).trail(other.trail()).lifecycle(new Lifecycle(other.lifecycle));
    }

    public Submission copy() {

        return new Submission(this);

    }

    public Submission updateWith(Submission other) {

        content(new SubmissionContent(other.content())).lifecycle().updateWithSubmission(other.lifecycle());

        return this;

    }

    @Override
    public List<String> streams() {

        if (content == null)
            return emptyList();

        var publication = lifecycle().publication();

        if (publication == null)

            return content.streams();

        return Stream.concat(publication.all().values().stream(), content.streams().stream()).collect((toList()));

    }

    public String ref() {

        return format("%s (%s,%s)", id(), trail(), state());
    }

    @Override
    public List<String> validate() {

        return CheckDsl.given(this)
                .check($ -> trail != null).or("missing trail")
                .check($ -> content != null).or("missing data")
                .andCollect();
    }

}
