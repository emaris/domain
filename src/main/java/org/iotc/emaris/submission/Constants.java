package org.iotc.emaris.submission;

public class Constants {
    
    public static final String trailPrefix = "TR";
    
    public static final String submissionPrefix = "SUB";
	public static final String submissionType = "submission";
}
