package org.iotc.emaris.submission.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.SubmissionContent;
import org.iotc.emaris.submission.Trail;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<Trail> trailBinder(ObjectMapper mapper) {

        return new Binder<>(mapper, Trail.class);
    } 


    @Produces @ApplicationScoped
    Binder<Submission> submissionBinder(ObjectMapper mapper) {

        return new Binder<>(mapper, Submission.class);
    } 


    @Produces @ApplicationScoped
    Binder<SubmissionContent> submissionContentBinder(ObjectMapper mapper) {

        return new Binder<>(mapper, SubmissionContent.class);
    } 

}


