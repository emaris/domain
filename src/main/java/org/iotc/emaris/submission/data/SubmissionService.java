package org.iotc.emaris.submission.data;

import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.campaign.Constants.suspenOnEndProperty;
import static org.iotc.emaris.campaign.Constants.suspendSubmissionsProperty;
import static org.iotc.emaris.submission.Constants.submissionPrefix;
import static org.iotc.emaris.submission.Submission.SubmissionState.draft;
import static org.iotc.emaris.submission.Submission.SubmissionState.missing;
import static org.iotc.emaris.submission.Submission.SubmissionState.pending;
import static org.iotc.emaris.submission.Submission.SubmissionState.submitted;
import static org.iotc.emaris.submission.data.AccessMode.stateless;

import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.campaign.data.CampaignInstanceService;
import org.iotc.emaris.submission.Submission;

import apprise.backend.iam.model.TenantRef;
import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class SubmissionService {

    @Inject
    User logged;

    @Inject
    SubmissionDao dao;

    @Inject
    Event<Submission> events;

    @Inject
    CampaignDao campaigns;

    @Inject
    CampaignInstanceService instances;

    @Inject
    TrailDao trails;

    public Submission add(@NonNull Submission submission) {

        OffsetDateTime now = OffsetDateTime.now(ZoneId.of(ZoneOffset.UTC.getId()));


        if (submission.id() == null)
            submission.id(Id.mint(submissionPrefix));

        submission.validateNow();

        if (submission.state() == missing)
            submission.lifecycle().lastSubmitted(now);

        submission.touchedBy(logged.username());

        submission.lifecycle().lastEdited(now);
        submission.lifecycle().lastEditedBy(logged.username());

        log.info("adding submission {}", submission.ref());

        dao.add(submission);

        events.select(Added.event).fire(submission);

        return submission;
    }

    // like update, but no content and and out-of-flow: client sets all lifecycle properties.
    public List<Submission> updateShallow(@NonNull List<Submission> updated) {

        return updated.stream().map(submission -> {

            if (submission.content() != null) {

                Submission fetched = dao.get(submission.id(), AccessMode.stateful);

                submission.content(fetched.content());
            }

            submission.updateWith(submission);

            submission.validateNow();

            log.info("updating submission {} (bulk)", submission.ref());

            dao.update(submission);

            events.select(Updated.event).fire(submission);

            return submission;

        }).collect(toList());
    }

    public Submission update(@NonNull Submission updated) {

        Submission submission = dao.get(updated.id(), stateless);

        OffsetDateTime now = OffsetDateTime.now(ZoneId.of(ZoneOffset.UTC.getId()));

        var campaign = campaigns.get(trails.get(submission.trail()).key().campaign());

        // if it's directly submitted or missing, mark it.
        if ((updated.state() == submitted || updated.state() == missing)) {
           
            if (!logged.tenant().equals(TenantRef.noRef)
                    && campaign.properties().get(suspendSubmissionsProperty, false))
                throw new IllegalStateException("No submissions allowed at this time.");

            if (submission.state() == pending){
                submission.lifecycle().lastApprovedBy(logged.username());
            }

            // fix 07/02/2025: an existing draft may be assessed as missing.
            if (updated.state() == missing && submission.lifecycle().lastSubmitted() == null)
               submission.lifecycle().lastSubmitted(now);
    
        }

        if ((updated.lifecycle().lastSubmitted()!=null && !updated.lifecycle().lastSubmitted().equals(submission.lifecycle().lastSubmitted())))
            setSubmissionDate(submission, updated);


        if (!submission.lifecycle().compliance().equals(updated.lifecycle().compliance())){
            submission.lifecycle().compliance().lastAssessed(now).lastAssessedBy(logged.username());
        }

        if (updated.lifecycle().state()==draft) {
            submission.lifecycle().lastEdited(now);
            submission.lifecycle().lastEditedBy(logged.username());
        }


        submission.touchedBy(logged.username());
        
        submission.updateWith(updated);

        submission.validateNow();

        log.info("updating submission {}", submission.ref());

        dao.update(submission);

        events.select(Updated.event).fire(submission);

        return submission;

    }

    private void setSubmissionDate(Submission submission, Submission updated) {

       OffsetDateTime now = OffsetDateTime.now(ZoneId.of(ZoneOffset.UTC.getId()));

        var date = Optional.ofNullable(updated.lifecycle().lastSubmitted()).orElse(now);

        var campaign = campaigns.get(trails.get(submission.trail()).key().campaign());

        // if (campaign.properties().get(suspendSubmissionsProperty, false))
        //     throw new IllegalStateException("No submissions allowed at this time.");

        var summary = instances.summaryOf(campaign);

        if (summary.startDate() != null && date.isBefore(summary.startDate()))
            throw new IllegalStateException("No submissions allowed before the start of campaign.");

        if (summary.endDate() != null && date.isAfter(summary.endDate())
                && campaign.properties().get(suspenOnEndProperty, true))
            throw new IllegalStateException("No submissions allowed at this time.");

        submission.lifecycle().lastSubmitted(date);
    }

    public Submission remove(@NonNull String id) {

        Submission requirement = dao.get(id, AccessMode.stateless);

        // observers might still want to see who's removing this.
        requirement.touchedBy(logged.username());

        log.info("removing submission {}", requirement.ref());

        dao.remove(requirement);

        events.select(Removed.event).fire(requirement);

        return requirement;

    }
}
