package org.iotc.emaris.submission.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.iotc.emaris.common.RemovedInstanceEvents;
import org.iotc.emaris.submission.Trail;

import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    Event<List<Trail>> events;

    @Inject
    TrailDao dao;


    // triggers on direct bulk instance removal.
    void onAnyBulkInstanceRemoval(@Observes @Removed RemovedInstanceEvents.Event<?> event) {

        onAnyBulkInstanceCascadeRemoval(event);
    }

    // triggers on cascading bulk instance removal.
    void onAnyBulkInstanceCascadeRemoval(@Observes @Removed(cascade=true) RemovedInstanceEvents.Event<?> event) {

        if (event.trails().isEmpty())
            return;

        var removed = dao.removeMany(event.trails());

        var type = event.instances().get(0).instanceType();

        log.info("removed {} trails following removal of {} '{}' instance(s)", removed, event.instances().size(), type);
          
        events.select(Removed.cascadeEvent).fire(event.trails());

    }

}
