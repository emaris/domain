package org.iotc.emaris.submission.data;

import static org.iotc.emaris.submission.data.DataConstants.submission_id;
import static org.iotc.emaris.submission.data.DataConstants.submission_trail;
import static org.iotc.emaris.submission.data.DataConstants.trail_asset;
import static org.iotc.emaris.submission.data.DataConstants.trail_campaign;
import static org.iotc.emaris.submission.data.DataConstants.trail_party;
import static org.iotc.emaris.submission.data.DataConstants.trail_type;

import java.util.ArrayList;
import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class SubmissionFilter {

	String trail;

	AccessMode mode = AccessMode.stateless;

	int limit;

	String campaign;

	String type;

	String asset;

	String party;

	List<String> submissions = new ArrayList<>();

	public static SubmissionFilter filter() {
		return new SubmissionFilter();
	}

	public static SubmissionFilter noConditions() {
		return new SubmissionFilter();
	}

	public boolean isEmpty() {

		return trail == null && limit==0;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (trail != null)
			condition = condition.and(submission_trail.eq(trail));


		if (asset != null)
			condition = condition.and(trail_asset.eq(asset));

		
		if (campaign != null)
			condition = condition.and(trail_campaign.eq(campaign));

		if (type != null)
			condition = condition.and(trail_type.eq(type));

		if (party != null)
			condition = condition.and(trail_party.eq(type));

		if (submissions!=null && submissions.size()>0)
			condition = condition.and(submission_id.in(submissions));

		return condition;
	} 
}