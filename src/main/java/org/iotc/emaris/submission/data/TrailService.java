package org.iotc.emaris.submission.data;

import static org.iotc.emaris.submission.Constants.trailPrefix;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Trail;

import apprise.backend.model.Id;
import apprise.backend.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class TrailService {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class KeyAndSubmission {

        Trail.Key key;
        Submission submission;

    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TrailAndSubmission {

        Trail trail;
        Submission submission;

    }

    

    @Inject
    TrailDao dao;

    @Inject
    SubmissionService submissions;

    public Trail add(@NonNull Trail trail) {

        if (trail.id() == null)
            trail.id(Id.mint(trailPrefix));

        trail.validateNow();

        log.info("adding trail {}", trail.ref());

        dao.add(trail);

        return trail;
    }

    
    public TrailAndSubmission addWith(@NonNull KeyAndSubmission keyAndSubmission) {

        var key = keyAndSubmission.key();
        var submission = keyAndSubmission.submission();     

        Trail added = dao.lookup(key).orElseGet( () -> add( new Trail().key(key)) );

        submission.trail(added.id());

        Submission addedSubmission = submissions.add(submission);

        added.submissions().add(addedSubmission);

        return new TrailAndSubmission(added, addedSubmission);

    }


    public Trail update(@NonNull Trail trail) {

        Optional<Trail> optional = dao.lookup(trail.id());

        if (optional.isEmpty())
            return add(trail);

        Trail current = optional.get();

        current.updateWith(trail);

        current.validate();

        log.info("updating trail {}", current.ref());

        dao.update(current);

        return current;

    }
    
}
