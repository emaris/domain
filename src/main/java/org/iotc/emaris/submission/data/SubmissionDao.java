package org.iotc.emaris.submission.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.submission.data.DataConstants.submission_details;
import static org.iotc.emaris.submission.data.DataConstants.submission_id;
import static org.iotc.emaris.submission.data.DataConstants.submission_state;
import static org.iotc.emaris.submission.data.DataConstants.submission_table;
import static org.iotc.emaris.submission.data.DataConstants.submission_trail;
import static org.iotc.emaris.submission.data.DataConstants.submissiondata_data;
import static org.iotc.emaris.submission.data.DataConstants.submissiondata_submission;
import static org.iotc.emaris.submission.data.DataConstants.trail_id;
import static org.iotc.emaris.submission.data.DataConstants.trail_party;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.submission.Submission;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import org.jooq.Table;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.iam.admin.data.TenantAwareDao;

@ApplicationScoped
public class SubmissionDao extends TenantAwareDao {

    private static final int max_on_empty = 200;

    @Inject
    DSLContext jooq;

    @Inject
    SubmissionSchema schema;

    public List<Submission> allMatching(SubmissionFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit(), filter.mode()).collect(toList());

    }

    public int sizeMatching(SubmissionFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.submissions()).where(filter.toCondition()));
    }

    public Submission get(String id, AccessMode mode) {

        return lookup(id, mode).orElseThrow(() -> new NoSuchEntityException(format("unknown submission %s", id)));

    }

    public Optional<Submission> lookup(String id, AccessMode mode) {

        return fetch(submission_id.equalIgnoreCase(id), mode).findFirst();

    }

    public void add(Submission submission) {

        try {

            jooq.insertInto(schema.submissions())
                    .set(submission_id, submission.id())
                    .set(submission_trail, submission.trail())
                    .set(submission_state, submission.lifecycle().state().name())
                    .set(submission_details, schema.submissionBinder().bind(submission))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(format(
                                    ("submission refers to resources that don't exist (trail)."), submission.id())))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("submission %s already exists."), submission.id())))

            );
        }

        try {

            jooq.insertInto(schema.submissionData())
                    .set(submissiondata_submission, submission.id())
                    .set(submissiondata_data, schema.submissionContentBinder().bind(submission.content()))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae);
        }

    }

    public void update(Submission submission) {

        try {

            jooq.update(schema.submissions())
                    .set(submission_state, submission.lifecycle().state().name())
                    .set(submission_details, schema.submissionBinder().bind(submission))
                    .where(submission_id.eq(submission.id()))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae);
        }

        try {

            jooq.update(schema.submissionData())
                    .set(submissiondata_data, schema.submissionContentBinder().bind(submission.content()))
                    .where(submissiondata_submission.eq(submission.id()))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae);
        }

    }

    public void remove(Submission submission) {

        jooq.delete(schema.submissions()).where(submission_id.eq(submission.id())).execute();

    }

    // helpers

    private Stream<Submission> fetch(Condition conditions, AccessMode mode) {

        return fetch(conditions, 0, mode);
    }

    private Stream<Submission> fetch(Condition conditions, int limit, AccessMode mode) {

        SelectConditionStep<Record> select = jooq.select()
                .from(schema.submissions())
                .join(schema.trails()).on(submission_trail.eq(trail_id))
                .where(conditions)
                .and(filterForTenancyOver(trail_party));

        Table<Record> selectedSubmissions = (limit > 0 ? select.limit(limit) : select).asTable(submission_table);

        switch (mode) {

        case stateless:
            return jooq.select().from(selectedSubmissions).fetch(schema.submissionStatelessMapper()).stream();

        case stateful:
        default:

            return jooq.select().from(selectedSubmissions)
                    .leftJoin(schema.submissionData())
                    .on(submission_id.eq(submissiondata_submission))
                    .fetch(schema.submissionStatefullMapper()).stream();

        }

    }

    public int removeMatching(TrailFilter filter) {

        return jooq.deleteFrom(schema.trails()).where(filter.toCondition()).execute();
    }
}
