package org.iotc.emaris.submission.data;

import static apprise.backend.iam.admin.data.DataConstants.tenant_id;
import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.submission.data.DataConstants.submission_data;
import static org.iotc.emaris.submission.data.DataConstants.submission_details;
import static org.iotc.emaris.submission.data.DataConstants.submission_id;
import static org.iotc.emaris.submission.data.DataConstants.submission_state;
import static org.iotc.emaris.submission.data.DataConstants.submission_table;
import static org.iotc.emaris.submission.data.DataConstants.submission_trail;
import static org.iotc.emaris.submission.data.DataConstants.submissiondata_data;
import static org.iotc.emaris.submission.data.DataConstants.submissiondata_submission;
import static org.iotc.emaris.submission.data.DataConstants.trail_asset;
import static org.iotc.emaris.submission.data.DataConstants.trail_campaign;
import static org.iotc.emaris.submission.data.DataConstants.trail_details;
import static org.iotc.emaris.submission.data.DataConstants.trail_id;
import static org.iotc.emaris.submission.data.DataConstants.trail_party;
import static org.iotc.emaris.submission.data.DataConstants.trail_table;
import static org.iotc.emaris.submission.data.DataConstants.trail_type;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.SubmissionContent;
import org.iotc.emaris.submission.Trail;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.iam.admin.data.IamSchema;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class SubmissionSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        IamSchema iamschema;

        @Inject
        CampaignSchema campaignSchema;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> trails;

        @Getter
        Table<Record> submissions;

        @Getter
        Table<Record> submissionData;

        @Inject
        @Getter
        Binder<Trail> trailBinder;

        @Inject
        @Getter
        Binder<Submission> submissionBinder;

        @Inject
        @Getter
        Binder<SubmissionContent> submissionContentBinder;

        @Inject
        Event<SchemaReady<Submission>> events;

        @Getter
        RecordMapper<Record, Trail> trailMapper = record -> trailBinder.bind(record.get(trail_details));

        @Getter
        RecordMapper<Record, Submission> submissionStatelessMapper = record -> record.get(submission_details) == null
                        ? null
                        : submissionBinder.bind(record.get(submission_details));

        @Getter
        RecordMapper<Record, Submission> submissionStatefullMapper = record -> submissionBinder
                        .bind(record.get(submission_details))
                        .content(submissionContentBinder.bind(record.get(submissiondata_data)));

        public void stageTables() {

                boolean firstTime = stageTrailTable();

                stageSubmissionTable();
                stageSubmissionDataTable();

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }

        public boolean stageTrailTable() {

                trails = table(name(config.dbschema(), trail_table));

                boolean firstTime = !schemas.exists(trails);

                String stmt = jooq.createTableIfNotExists(trails)
                                .column(trail_id, VARCHAR.length(100))
                                .column(trail_campaign, VARCHAR.nullable(false))
                                .column(trail_party, VARCHAR.nullable(false))
                                .column(trail_asset, VARCHAR.nullable(false))
                                .column(trail_type, VARCHAR.nullable(false))
                                .column(trail_details, VARCHAR.nullable(false))
                                .constraints(

                                                constraint(trails.getName() + "_pkey")
                                                                .primaryKey(trail_id),

                                                constraint(trails.getName() + "_ref")
                                                                .unique(trail_campaign,trail_party,trail_asset),

                                                constraint(trails.getName() + "party_fkey")
                                                                .foreignKey(trail_party)
                                                                .references(iamschema.tenants(), tenant_id)
                                                                .onDeleteCascade(),

                                                // can't express asset constraints as it may be towards different
                                                // tables.
                                                // app is reponsible for data integrity here

                                                constraint(trails.getName() + "campaign_fkey")
                                                                .foreignKey(trail_campaign)
                                                                .references(campaignSchema.campaigns(), campaign_id)
                                                                .onDeleteCascade())
                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(trails.getName() + "_campaign_index").on(trails, trail_campaign).execute();
                jooq.createIndexIfNotExists(trails.getName() + "_party_index").on(trails, trail_party).execute();
                jooq.createIndexIfNotExists(trails.getName() + "_asset_index").on(trails, trail_asset).execute();
                jooq.createIndexIfNotExists(trails.getName() + "_type_index").on(trails, trail_type).execute();

                return firstTime;

        }

        public void stageSubmissionTable() {

                submissions = table(name(config.dbschema(), submission_table));

                String stmt = jooq.createTableIfNotExists(submissions)
                                .column(submission_id, VARCHAR.length(100))
                                .column(submission_trail, VARCHAR.nullable(false))
                                .column(submission_state, VARCHAR.nullable(false))
                                .column(submission_details, VARCHAR.nullable(false))
                                .constraints(

                                                constraint(submissions.getName() + "_pkey")
                                                                .primaryKey(submission_id),

                                                constraint(submissions.getName() + "trail_fkey")
                                                                .foreignKey(submission_trail)
                                                                .references(trails, trail_id)
                                                                .onDeleteCascade())

                                .getSQL();

                jooq.execute(stmt);

                jooq.createIndexIfNotExists(submissions.getName() + "_trail_index").on(submissions, submission_trail)
                                .execute();

                jooq.createIndexIfNotExists(submissions.getName() + "_state_index").on(submissions, submission_state)
                                .execute();

        }

        public void stageSubmissionDataTable() {

                submissionData = table(name(config.dbschema(), submission_data));

                String stmt = jooq.createTableIfNotExists(submissionData)
                                .column(submissiondata_submission, VARCHAR.length(100))
                                .column(submissiondata_data, VARCHAR.nullable(false))
                                .constraints(

                                                constraint(submissionData.getName() + "_pkey")
                                                                .primaryKey(submissiondata_submission),

                                                constraint(submissionData.getName() + "submission_fkey")
                                                                .foreignKey(submissiondata_submission)
                                                                .references(submissions, submission_id)
                                                                .onDeleteCascade())

                                .getSQL();

                jooq.execute(stmt);

        }

}
