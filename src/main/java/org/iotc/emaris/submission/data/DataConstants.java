package org.iotc.emaris.submission.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {
    
    public static final String trail_table  = "trail";
    
    public static final Field<String> trail_id = field(name(trail_table, "trail_id"), String.class);
    public static final Field<String> trail_campaign = field(name(trail_table,"campaign"), String.class);
    public static final Field<String> trail_party = field(name(trail_table,"party"), String.class);
    public static final Field<String> trail_asset = field(name(trail_table,"asset"), String.class);
    public static final Field<String> trail_type = field(name(trail_table,"type"), String.class);
	public static final Field<String> trail_details = field(name(trail_table, "details"), String.class);

    public static final String submission_table  = "submission";

    public static final Field<String> submission_id = field(name(submission_table, "submission_id"), String.class);
    public static final Field<String> submission_trail = field(name(submission_table,"trail"), String.class);
    public static final Field<String> submission_state = field(name(submission_table, "state"), String.class);
    public static final Field<String> submission_details = field(name(submission_table, "details"), String.class);


    public static final String submission_data  = "submission_data";

    public static final Field<String> submissiondata_submission = field(name(submission_data, "submission"), String.class);
    public static final Field<String> submissiondata_data = field(name(submission_data,"data"), String.class);
    
}
