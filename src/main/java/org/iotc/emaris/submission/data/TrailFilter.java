package org.iotc.emaris.submission.data;

import static org.iotc.emaris.submission.data.DataConstants.trail_asset;
import static org.iotc.emaris.submission.data.DataConstants.trail_campaign;
import static org.iotc.emaris.submission.data.DataConstants.trail_party;
import static org.iotc.emaris.submission.data.DataConstants.trail_type;

import java.util.List;

import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class TrailFilter {

	String campaign;
	String party;
	String asset;
	String type;

	int limit;

	List<String> assets;
	List<String> parties;

	public static TrailFilter filter() {
		return new TrailFilter();
	}

	public static TrailFilter noConditions() {
		return new TrailFilter();
	}

	public boolean isEmpty() {

		return campaign == null && party==null && asset==null && type==null && limit==0;
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (campaign != null)
			condition = condition.and(trail_campaign.eq(campaign));

		if (party != null)
			condition = condition.and(trail_party.eq(party));

		if (asset != null)
			condition = condition.and(trail_asset.eq(asset));

		if (type != null)
			condition = condition.and(trail_type.eq(type));
		
		if (assets != null && assets.size() > 0)
			condition = condition.and(trail_asset.in(assets));

		if (parties != null && parties.size() > 0)
			condition = condition.and(trail_party.in(parties));

		return condition;
	} 
}