package org.iotc.emaris.submission.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.submission.data.DataConstants.submission_trail;
import static org.iotc.emaris.submission.data.DataConstants.trail_asset;
import static org.iotc.emaris.submission.data.DataConstants.trail_campaign;
import static org.iotc.emaris.submission.data.DataConstants.trail_details;
import static org.iotc.emaris.submission.data.DataConstants.trail_id;
import static org.iotc.emaris.submission.data.DataConstants.trail_party;
import static org.iotc.emaris.submission.data.DataConstants.trail_table;
import static org.iotc.emaris.submission.data.DataConstants.trail_type;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.submission.Submission.SubmissionState;
import org.iotc.emaris.submission.Trail;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.Table;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.iam.admin.data.TenantAwareDao;

@ApplicationScoped
public class TrailDao extends TenantAwareDao {

    private static final int max_on_empty = 100;

    @Inject
    DSLContext jooq;

    @Inject
    SubmissionSchema schema;

    public List<Trail> allMatching(TrailFilter filter) {

        if (filter.isEmpty() && sizeMatching(filter) > max_on_empty)
            throw new IllegalStateException("empty filter not allowed on current population");

        return fetch(filter.toCondition(), filter.limit()).collect(toList());

    }

    public List<Trail> allPublished(TrailFilter filter) {

        // only published submissions here.
        Select<Record> trails = jooq.select().from(schema.trails()).join(schema.submissions())
                .on(trail_id.eq(submission_trail))
                .where(filter.toCondition())
                .and(DataConstants.submission_state.eq(SubmissionState.published.name()));

        return fetchWith(trails).collect(toList());

    }

    public int sizeMatching(TrailFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.trails()).where(filter.toCondition()));
    }

    public Trail get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown trail %s", id)));

    }

    public Optional<Trail> lookup(String id) {

        return fetch(trail_id.equalIgnoreCase(id)).findFirst();

    }

    public Optional<Trail> lookup(Trail.Key key) {

        return fetch(trail_campaign.eq(key.campaign())
                .and(trail_party.eq(key.party()))
                .and(trail_asset.eq(key.asset()))
                .and(trail_type.eq(key.assetType())))
                .findFirst();

    }

    public void add(Trail trail) {

        try {

            jooq.insertInto(schema.trails())
                    .set(trail_id, trail.id())
                    .set(trail_campaign, trail.key().campaign())
                    .set(trail_party, trail.key().party())
                    .set(trail_asset, trail.key().asset())
                    .set(trail_type, trail.key().assetType())
                    .set(trail_details, schema.trailBinder().bind(trail))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(format(
                                    ("trail refers to resources that don't exist (campaign,party)."), trail.ref())))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("trail %s already exists."), trail.ref())))

            );
        }

    }

    public void update(Trail trail) {

        try {

            jooq.update(schema.trails())
                    .set(trail_details, schema.trailBinder().bind(trail))
                    .where(trail_id.eq(trail.id()))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae);
        }

    }

    // helpers

    private Stream<Trail> fetch(Condition conditions) {

        return fetch(conditions, 0);
    }

    private Stream<Trail> fetch(Condition conditions, int limit) {

        SelectConditionStep<Record> trails = jooq.select()
                .from(schema.trails())
                .where(conditions)
                .and(filterForTenancyOver(trail_party));

        Table<Record> selectedTrails = (limit > 0 ? trails.limit(limit) : trails).asTable(trail_table);

        Select<Record> query = jooq.select().from(selectedTrails).leftJoin(schema.submissions()).on(trail_id.eq(submission_trail));

        return fetchWith(query);

        

    }

    private Stream<Trail> fetchWith(Select<Record> query) {

        return query.fetchGroups(schema.trailMapper())
                .entrySet()
                .stream()
                .map(e -> {

                    Trail trail = e.getKey();

                    trail.submissions(
                            sanitized(e.getValue().stream().map(schema.submissionStatelessMapper()::map)
                                    .collect(toList())));

                    return trail;
                });

    }

    public List<Trail> removeMatching(TrailFilter filter) {

        List<Trail> removed = jooq.select().from(schema.trails()).where(filter.toCondition()).fetch(schema.trailMapper());

        removeMany(removed);

        return removed;
    }

    public int removeMany(List<Trail> trails) {

        List<String> removedIds = trails.stream().map( i -> i.id()).collect((toList()));

        return jooq.deleteFrom(schema.trails()).where(trail_id.in(removedIds)).execute();
    }
}
