package org.iotc.emaris.submission.data;

public enum AccessMode {

    stateless, stateful
    
}
