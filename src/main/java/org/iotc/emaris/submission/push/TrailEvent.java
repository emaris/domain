package org.iotc.emaris.submission.push;

import org.iotc.emaris.submission.Trail;

import apprise.backend.event.StandardEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TrailEvent extends StandardEvent<TrailEvent> {

    Trail trail;
}

