package org.iotc.emaris.submission.push;

import static java.lang.String.format;
import static org.iotc.emaris.submission.Constants.submissionType;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.iotc.emaris.submission.Submission;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.TrailDao;

import apprise.backend.event.EventBus;
import apprise.backend.iam.model.User;

@ApplicationScoped
public class Observers {

    public static final String topicFormat = "%s.%s.%s";

    @Inject
    User logged;

    @Inject
    TrailDao trails;

    @Inject
    EventBus bus;

    void onChange(@Observes @Any Submission sub) {

        Trail trail = trails.get(sub.trail());

        String topic = format(topicFormat, trail.key().party(), submissionType, trail.key().asset());
    
        bus.instanceFor(topic).publish(new TrailEvent().origin(logged.username()).trail(trail));
    }
    
}
