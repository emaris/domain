package org.iotc.emaris.submission;

import static java.util.stream.Collectors.toList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import apprise.backend.bytestream.model.Bytestream;
import apprise.backend.model.Bag;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubmissionContent {

    Bag data = new Bag();
    Bag changelog;
    Map<String, List<Bytestream>> resources = new HashMap<>();

    public SubmissionContent(SubmissionContent other) {

        data = new Bag(other.data());
        changelog = other.changelog == null ? null :  new Bag(other.changelog());
        resources.putAll(other.resources());
    }


   public List<String> streams() {
       return resources.values().stream().flatMap(r->r.stream()).map(Bytestream::id).collect(toList());
   }

}
