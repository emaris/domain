package org.iotc.emaris.common;

import static apprise.backend.iam.Constants.tenantType;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.submission.data.TrailFilter.filter;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.event.Constants;
import org.iotc.emaris.submission.Trail;
import org.iotc.emaris.submission.data.TrailDao;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
public class RemovedInstanceEvents {

    @Inject
    TrailDao trails;


    public <T extends CampaignInstance<T>> Event<T> eventFor(@NonNull List<T> instances) {

        var instance = new Event<T>(instances);
        
        if (instances.isEmpty())
            return instance;

        var type = instances.get(0).instanceType();
        var campaign = instances.get(0).campaign();

        // collecta and push related trails before they're removed by lower-priority cascade observers.
        if (!type.equals(Constants.eventType)) {

            var sources = instances.stream().map(i -> i.source()).collect(toList());

            var filter = type.equals(tenantType) ? filter().parties(sources) : filter().assets(sources);

            var related = trails.allMatching(filter.campaign(campaign));

            if (!related.isEmpty())
                instance.trails(related);

        }

        return instance;


    }

    @Data
    @NoArgsConstructor(force = true)
    @RequiredArgsConstructor
    public static class Event<T extends CampaignInstance<T>> {

        final List<T> instances;
        List<Trail> trails = new ArrayList<>();


    }
    
    

}
