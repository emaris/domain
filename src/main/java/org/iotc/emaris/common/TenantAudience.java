package org.iotc.emaris.common;

import java.util.HashSet;
import java.util.Set;

import apprise.backend.iam.model.TenantRef;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TenantAudience {

    Set<TenantRef> includes = new HashSet<>();
    Set<TenantRef> excludes = new HashSet<>();

    public TenantAudience(TenantAudience other) {

        includes(new HashSet<>(other.includes()))
                .excludes(new HashSet<>(other.excludes()));
    }

    public boolean matches(TenantRef tenantRef) {

        var audienceListCheckExclude = excludes.isEmpty() || !excludes.contains(tenantRef);
        var audienceListCheckInclude = includes.isEmpty() || includes.contains(tenantRef);

        return audienceListCheckExclude && audienceListCheckInclude;

    }
}
