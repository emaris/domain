package org.iotc.emaris.requirement.api;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.requirement.Constants.layoutProperty;
import static org.iotc.emaris.requirement.api.ApiConstants.requirementapi;
import static org.iotc.emaris.requirement.data.RequirementFilter.withNoConditions;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;
import org.iotc.emaris.requirement.data.RequirementFilter;
import org.iotc.emaris.requirement.data.RequirementService;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

public interface LifecycleApi {

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class Raw {

		@JsonRawValue
		@JsonValue // returns as string and without any object wrapping.
		List<String> data;
	}

	@Operation(summary = "Returns all requirements")

	List<Requirement> fetchAll();

	Response fetchRawSummaries();

	@Operation(summary = "Returns one requirement")

	Requirement fetchOne(String rid);

	@Operation(summary = "Service requirements that match a filter")

	Response search(RequirementFilter filter);

	@Operation(summary = "Creates a new requirement", description = "Requires management rights")
	Requirement addOne(Requirement requirement);

	@Operation(summary = "Updates a requirement", description = "Requires management rights")

	Requirement updateOne(String rid, Requirement requirement);

	@Operation(summary = "Deletes a requirement", description = "Requires management rights")
	void removeOne(String rid);

	@Path(requirementapi)
	public class Default implements LifecycleApi {

		@Inject
		RequirementDao dao;

		@Inject
		RequirementService service;

		@GET
		@Produces(APPLICATION_JSON)
		@Override
		public List<Requirement> fetchAll() {

			return dao.all(withNoConditions()).stream().map(r -> {

				r.properties().clear(layoutProperty);
				r.documents(Requirement.emptyDocuments);

				return r;

			}).collect(toList());

		}

		@GET
		@Path("raw")
		@Produces(APPLICATION_JSON)
		@Override
		public Response fetchRawSummaries() {

			return Response.ok().entity(dao.rawSummaries()).build();

		}

		@GET
		@Produces(APPLICATION_JSON)
		@Path("{rid}")
		@Override
		public Requirement fetchOne(@NonNull @PathParam("rid") String rid) {

			return dao.get(rid);

		}

		@POST
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Path(ApiConstants.search)
		@Override
		public Response search(RequirementFilter filter) {

			return Response.ok().entity(dao.allRaw(filter)).build();

		}

		@POST
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Requirement addOne(@NonNull Requirement requirement) {

			return service.add(requirement);
		}

		@PUT
		@Path("{rid}")
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Requirement updateOne(@NonNull @PathParam("rid") String tid, Requirement requirement) {

			if (!requirement.id().equals(tid))
				throw new IllegalArgumentException(
						format("requirement id '%s' does not match id in payload ('%s')", tid, requirement.id()));

			return service.update(requirement);
		}

		@DELETE
		@Path("{rid}")
		@Override
		public void removeOne(@NonNull @PathParam("rid") String id) {

			service.remove(id);

		}
	}
}
