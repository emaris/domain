package org.iotc.emaris.requirement.api;

import static java.lang.Integer.MAX_VALUE;
import static org.iotc.emaris.requirement.Rights.manage_requirements;
import static org.iotc.emaris.requirement.Rights.edit_requirements;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;

import apprise.backend.iam.model.User;

@Decorator
@Priority(MAX_VALUE)
public abstract class LifecycleSecureApi implements LifecycleApi {

    @Inject
    @Delegate
    LifecycleApi unsecure;

    @Inject
    User logged;

    @Override
    public Requirement addOne(Requirement requirement) {

        logged.assertCan(manage_requirements);

        return unsecure.addOne(requirement);
    }

    @Override
    public Requirement updateOne(String id, Requirement requirement) {

        logged.assertCan(edit_requirements.on(id));

        return unsecure.updateOne(id, requirement);
    }


    @Override
	public void removeOne(String id) {
	
		logged.assertCan(manage_requirements.on(id));
		
		unsecure.removeOne(id);
	}

}
