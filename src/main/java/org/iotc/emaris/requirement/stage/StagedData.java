package org.iotc.emaris.requirement.stage;

import static apprise.backend.Exceptions.storyOf;
import static apprise.backend.model.Multilingual.text;
import static apprise.backend.model.Multilingual.Language.fr;
import static apprise.backend.tag.Constants.categoryPrefix;
import static apprise.backend.tag.Constants.tagPrefix;
import static apprise.backend.tag.TagCategory.Cardinality.one;
import static apprise.backend.tag.stage.StagedData.idformat;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.requirement.Constants.requirementType;
import static org.iotc.emaris.stage.StagedData.iotcToggle;

import java.io.InputStream;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.Event.Predefined;
import org.iotc.emaris.requirement.Requirement;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.config.CoreConfig;
import apprise.backend.data.Staged;
import apprise.backend.model.Bag;
import apprise.backend.tag.Tag;
import apprise.backend.tag.TagCategory;
import apprise.backend.tag.stage.StagedData.Field;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class StagedData {

        private static final String requirementResource = "/META-INF/iotc-requirements.stage";

        @Produces
        @Staged
        List<Requirement> requirements(CoreConfig config, ObjectMapper mapper) {

                if (!config.toggles().isActive(iotcToggle))
                        return emptyList();

                InputStream instances = getClass().getResourceAsStream(requirementResource);

                if (instances == null) {
                        log.warn("cannot stage requirements, no {} ", requirementResource);
                        return emptyList();
                }

                try {
                        return asList(mapper.readValue(instances, Requirement[].class));
                } catch (Throwable t) {
                        log.warn("cannot stage requirements: {} ", storyOf(t));
                        return emptyList();
                }

        }

        @Produces
        @Staged
        public static TagCategory importanceScale = new TagCategory()
                        .id(format(idformat, categoryPrefix, requirementType, "importancescale"))
                        .type(requirementType)
                        .cardinality(one)
                        .guarded(true)
                        .predefined(true)

                        .name(text()
                                        .inDefaultLanguage("Importance")
                                        .in(fr, "Importance"))

                        .description(text()
                                        .inDefaultLanguage("Importance scale for requirement.")
                                        .in(fr, "Échelle d'importance pour l'exigence."))

                        .properties(new Bag()
                                        .set("color", "#2A8800").withDefault()
                                        .set(Field.name, Field.class).withDefault());

        @Produces
        @Staged
        public static Tag very_important = new Tag()
                        .id(format(idformat, tagPrefix, requirementType, "veryimportant"))
                        .type(requirementType)
                        .category(importanceScale.id())
                        .guarded(true)
                        .predefined(true)
                        .name(text().inDefaultLanguage("Very Important").in(fr, "Très important"));

        @Produces
        @Staged
        public static Tag important = new Tag()
                        .id(format(idformat, tagPrefix, requirementType, "important"))
                        .type(requirementType)
                        .category(importanceScale.id())
                        .guarded(true)
                        .predefined(true)
                        .name(text().inDefaultLanguage("Important").in(fr, "Important"));

        @Produces
        @Staged
        public static Tag normal = new Tag()
                        .id(format(idformat, tagPrefix, requirementType, "normal"))
                        .type(requirementType)
                        .category(importanceScale.id())
                        .guarded(true)
                        .predefined(true)
                        .name(text().inDefaultLanguage("Normal").in(fr, "Ordinaire"));

        @Produces
        @Staged
        public static Tag not_very_important = new Tag()
                        .id(format(idformat, tagPrefix, requirementType, "notveryimportant"))
                        .type(requirementType)
                        .category(importanceScale.id())
                        .guarded(true)
                        .predefined(true)
                        .name(text().inDefaultLanguage("Not Very Important").in(fr, "Pas très important"));

        @Produces
        @Staged
        public static Event requirement_deadline = new Event()
                        .id(format(idformat, eventPrefix, requirementType, "deadline"))
                        .type(requirementType)
                        .predefined(true)
                        .predefine(Predefined.cardinality)
                        .managed(true)
                        .name(text().inDefaultLanguage("Requirement Deadline").in(fr, "Échéance d'exigence"))
                        .description(text().inDefaultLanguage("The deadline of a requirement.").in(fr, "La echéance d'une exigence."));

}
