package org.iotc.emaris.requirement.stage;

import static apprise.backend.iam.stage.StagedData.bootadmin;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;

import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.Staged;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    RequirementDao dao;

    void stageAllOnFirstStart(@Observes @Added SchemaReady<Requirement> event, @Staged Instance<Requirement> singletons,
            @Staged Instance<List<Requirement>> groups) {

        stageRequirements(singletons, groups, true);
    }

    void stagePredefineOnStart(@Observes @Updated SchemaReady<Requirement> event,
            @Staged Instance<Requirement> singletons, @Staged Instance<List<Requirement>> groups) {

        stageRequirements(singletons, groups, false);
    }

    void stageRequirements(@Staged Instance<Requirement> singletons, Instance<List<Requirement>> groups,
            boolean firstStart) {

        Predicate<Requirement> selected = r -> firstStart ? true : r.predefined();

        List<Requirement> requirements = singletons.stream().filter(selected).collect(toList());

        requirements.addAll(groups.stream()
                .flatMap(t -> t.stream())
                .filter(selected)
                .collect(toList()));

        if (requirements.size() > 0) {

            log.info("staging requirements ({})", requirements.stream().map(c -> c.id()).collect(joining(" , ")));

            requirements.stream()
                    .map(r -> r.state(Requirement.State.active).touchedBy(bootadmin.username()))
                    .forEach(r -> dao.add(r, true));
        }
    }

}
