package org.iotc.emaris.requirement;

public class Constants {

	public static final String requirementPrefix = "RQ";
	public static final String requirementType = "requirement";
	public static final String layoutProperty = "layout";
}
