package org.iotc.emaris.requirement;

import static apprise.backend.iam.model.Action.action;
import static org.iotc.emaris.requirement.Constants.requirementType;

import apprise.backend.iam.model.Action;

public class Rights {

    public static final Action manage_requirements = action("manage").type(requirementType).make();
    public static final Action edit_requirements = action("manage","edit").type(requirementType).make();

}
