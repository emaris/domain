package org.iotc.emaris.requirement;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.requirement.Requirement.State.active;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.iotc.emaris.common.TenantAudience;

import apprise.backend.bytestream.model.Bytestreamed;
import apprise.backend.bytestream.model.Document;
import apprise.backend.model.Bag;
import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.model.Predefined;
import apprise.backend.tag.TagExpression;
import apprise.backend.tag.Tagged;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Requirement
		implements Lifecycled<Requirement.State, Requirement>, Predefined<Requirement.Predefined, Requirement>,
		Tagged<Requirement>, Validated, Comparable<Requirement>, Bytestreamed {

		
	public static final Map<String, List<Document>> emptyDocuments = new HashMap<>();

	public static enum State {
		active, inactive
	}

	public static enum Predefined {
		guarded
	}

	String id;

	boolean guarded;

	boolean predefined;
	Set<Predefined> predefinedProperties = new HashSet<>();

	Lifecycle<State> lifecycle = new Lifecycle<>(State.inactive);

	Multilingual name = new Multilingual();
	Multilingual description = new Multilingual();

	List<String> lineage = new ArrayList<>();

	TagExpression audience = new TagExpression();
	TenantAudience audienceList = new TenantAudience();

	TagExpression userProfile = new TagExpression();

	Set<String> tags = new HashSet<>();

	Map<String, List<Document>> documents = new HashMap<>(emptyDocuments);

	Bag properties = new Bag();

	public Requirement(Requirement other) {

		updateWith(other)
				.id(other.id())
				.predefined(other.predefined())
				.predefinedProperties(new HashSet<>(other.predefinedProperties()))
				.guarded(other.guarded())
				.tags(other.tags())
				.documents(other.documents() == null ? this.documents : new HashMap<>(other.documents()))
				.audience(new TagExpression(other.audience()))
				.audienceList(new TenantAudience(other.audienceList()))
				.userProfile(new TagExpression(other.userProfile()))
				.lineage(new ArrayList<>(other.lineage()))
				.lifecycle(new Lifecycle<>(other.lifecycle()));

	}

	public Requirement copy() {

		return new Requirement(this);

	}

	public Requirement updateWith(Requirement other) {

		// these can change at all times
		name(new Multilingual(other.name()))
				.description(new Multilingual(other.description()))
				.properties(new Bag(other.properties()));

		guarded(predefined(Predefined.guarded) ? this.guarded() : other.guarded());

		if (!this.guarded())
			tags(other.tags())
					.audience(new TagExpression(other.audience()))
					.audienceList(new TenantAudience(other.audienceList()))
					.userProfile(new TagExpression(other.userProfile()))
					.lineage(new ArrayList<>(other.lineage()))
					.documents(other.documents() == null ? this.documents : new HashMap<>(other.documents()))
					.lifecycle().updateWith(other.lifecycle());

		return this;

	}

	public Requirement tags(Collection<String> tags) {
		this.tags = new HashSet<>(tags);
		return this;
	}

	public String ref() {
		return format("%s (%s)", name.inDefaultLanguage(), id);
	}

	@Override
	public List<String> streams() {
		 return documents.entrySet().stream()
						.flatMap(e->e.getValue().stream().flatMap(d->d.streams().values().stream()))
						.map(s->s.id())
						.collect(toList());
			
	}

	@Override
	public List<String> validate(Mode mode) {

		return CheckDsl.given(this)
				.check($ -> id != null).or("missing identifier")
				.provided(name != null)
				.check($ -> name.inDefaultLanguage() != null).or("no name in default language")
				.provided(mode == Mode.create)
				.check($ -> !predefined())
				.or("invalid state: predefined")
				.provided(mode == Mode.delete)
				.check($ -> !predefined && !guarded && state() != active)
				.or("cannot remove requirement %s because it is active, predefined, or protected", ref())
				.andCollect();
	}

	@Override
	public int compareTo(Requirement o) {
		return name().compareTo(o.name());
	}
}
