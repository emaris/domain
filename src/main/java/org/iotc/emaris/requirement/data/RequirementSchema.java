package org.iotc.emaris.requirement.data;

import static org.iotc.emaris.requirement.data.DataConstants.requirement_details;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.JSONB;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class RequirementSchema {

    @Inject
    Config config;

    @Inject
    SchemaDao schemas;

    @Inject
    DSLContext jooq;

    @Getter
    Table<Record> requirements;

    @Inject
    @Getter
    Binder<Requirement> binder;

    @Inject
    Event<SchemaReady<Requirement>> events;

    @Getter
    RecordMapper<Record, Requirement> mapper = record -> binder.bind(record.get(requirement_details).data());




    public void stageTables() {

        requirements = table(name(config.dbschema(), DataConstants.requirement_table));

        boolean firstTime = !schemas.exists(requirements);

        String stmt = jooq.createTableIfNotExists(requirements)
                .column(DataConstants.requirement_id, VARCHAR.length(100))
                .column(DataConstants.requirement_state, VARCHAR.nullable(false))
                .column(DataConstants.requirement_lineage, VARCHAR.nullable(true))
                .column(DataConstants.requirement_details, JSONB.nullable(false))
                .constraints(
                        constraint(requirements.getName() + "_pkey").primaryKey(DataConstants.requirement_id))
                .getSQL();

        jooq.execute(stmt);

        events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());
        

    }
}
