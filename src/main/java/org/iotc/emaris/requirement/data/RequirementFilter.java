package org.iotc.emaris.requirement.data;

import static org.iotc.emaris.requirement.data.DataConstants.requirement_id;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_state;

import java.util.List;

import org.iotc.emaris.requirement.Requirement;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class RequirementFilter {

	Requirement.State state = null;

	List<String> ids = null;

	public static RequirementFilter filter() {
		return new RequirementFilter();
	}

	public static RequirementFilter withNoConditions() {
		return new RequirementFilter();
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (state != null)
			condition = condition.and(requirement_state.eq(state.name()));
		
		if (ids!=null && !ids.isEmpty())
			condition = condition.and(requirement_id.in(ids()));

		return condition;
	} 
}