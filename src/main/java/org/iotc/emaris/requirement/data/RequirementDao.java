package org.iotc.emaris.requirement.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_details;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_id;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_lineage;
import static org.iotc.emaris.requirement.data.DataConstants.requirement_state;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.JSONB;
import org.jooq.Record;

import apprise.backend.Exceptions.NoSuchEntityException;

@ApplicationScoped
public class RequirementDao {

    @Inject
    DSLContext jooq;

    @Inject
    RequirementSchema schema;

    public List<Requirement> all(RequirementFilter filter) {

        return jooq.select()
                .from(schema.requirements())
                .where(filter.toCondition())
                .fetch(schema.mapper());

    }

    public String allRaw(RequirementFilter filter) {

        return jooq.fetchOne(format(
                        "select jsonb_agg(%s) from %s where %s", 
                        requirement_details,
                        schema.requirements(), 
                        filter.toCondition().toString()
                ))
                .get(0,JSONB.class).data();

    }

    public String rawSummaries() {

        return jooq.fetchOne(format(
                "select jsonb_agg((%s #- '{properties,layout}')  - 'documents') from %s", requirement_details,schema.requirements()
        ))
        .get(0,JSONB.class).data();

    }

    public int size(RequirementFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.requirements()).where(filter.toCondition()));
    }

    public Requirement get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown requirement %s", id)));

    }

    public Optional<Requirement> lookup(String id) {

        return Optional.ofNullable(jooq.select()
                .from(schema.requirements())
                .where(requirement_id.equalIgnoreCase(id))
                .fetchOne(schema.mapper()));

    }

    public void add(Requirement requirement) {

        try {

            add(requirement, false);

        } catch (RuntimeException dae) {

            map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(format(
                                    ("requirement refers to resources that don't exist (lineage)."), requirement.id())))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("requirement %s already exists."), requirement.id())))

            );
        }

    }

    public void add(Requirement requirement, boolean ignoreOnDuplicate) {

        InsertOnDuplicateStep<Record> insert = jooq.insertInto(schema.requirements())
                .set(requirement_id, requirement.id())
                .set(requirement_state, requirement.state().name())
                .set(requirement_lineage,
                        requirement.lineage() == null ? null : requirement.lineage().stream().collect(joining(":")))
                .set(requirement_details, JSONB.valueOf(schema.binder().bind(requirement)));

        if (ignoreOnDuplicate)
            insert.onDuplicateKeyIgnore();

        insert.execute();


    }

    public void update(Requirement requirement) {

        jooq.update(schema.requirements())
                .set(requirement_state, requirement.state().name())
                .set(requirement_lineage,
                        requirement.lineage() == null ? null : requirement.lineage().stream().collect(joining(":")))
                .set(requirement_details, JSONB.valueOf(schema.binder().bind(requirement)))
                .where(requirement_id.equalIgnoreCase(requirement.id()))
                .execute();

    }

    public void remove(Requirement requirement) {

        jooq.delete(schema.requirements()).where(requirement_id.equalIgnoreCase(requirement.id())).execute();

    }

}
