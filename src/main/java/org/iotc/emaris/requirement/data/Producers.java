package org.iotc.emaris.requirement.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.requirement.Requirement;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<Requirement> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, Requirement.class);
    } 
}
