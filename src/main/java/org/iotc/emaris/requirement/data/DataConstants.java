package org.iotc.emaris.requirement.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;
import org.jooq.JSONB;

public class DataConstants {

    public static final String requirement_table  = "requirement_opt";

    public static final Field<String> requirement_id = field(name(requirement_table, "id"), String.class);
    public static final Field<String> requirement_state = field(name(requirement_table,"state"), String.class);
    public static final Field<String> requirement_lineage = field(name(requirement_table,"lineage"), String.class);
	public static final Field<JSONB> requirement_details = field(name(requirement_table, "details"), JSONB.class);
    
}
