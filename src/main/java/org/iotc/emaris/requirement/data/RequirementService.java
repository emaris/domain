package org.iotc.emaris.requirement.data;

import static apprise.backend.validation.Mode.create;
import static org.iotc.emaris.requirement.Constants.requirementPrefix;
import static org.iotc.emaris.requirement.Requirement.State.inactive;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Requirement;

import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class RequirementService {

    @Inject
    RequirementDao dao;

    @Inject
    User logged;

    @Inject
    Event<Requirement> events;

    public Requirement add(@NonNull Requirement requirement) {

        if (requirement.id() == null)
            requirement.id(Id.mint(requirementPrefix));

        if (requirement.state() == null)
            requirement.state(inactive);

        requirement.validateNow(create);

        requirement.touchedBy(logged.username());

        log.info("adding requirement {}", requirement.ref());

        dao.add(requirement);

        events.select(Added.event).fire(requirement);

        return requirement;
    }

    public Requirement update(@NonNull Requirement updated) {

        Requirement requirement = dao.get(updated.id());

        requirement.updateWith(updated);

        requirement.validateNow(Mode.update);

        requirement.touchedBy(logged.username());

        log.info("updating requirement {}", requirement.ref());

        dao.update(requirement);

        events.select(Updated.event).fire(requirement);

        return requirement;

    }

    public Requirement remove(@NonNull String id) {

        Requirement requirement = dao.get(id);

        requirement.validateNow(Mode.delete);

        // observers might still want to see who's removing this.
        requirement.touchedBy(logged.username());

        log.info("removing requirement {}", requirement.ref());

        dao.remove(requirement);

        events.select(Removed.event).fire(requirement);

        return requirement;

    }
}
