package org.iotc.emaris.requirement.push;

import org.iotc.emaris.requirement.Requirement;

import apprise.backend.event.StandardEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class RequirementChange extends StandardEvent<RequirementChange> {

    public static enum Type { add, remove, change }

    Requirement requirement;
    Type type;

}

