package org.iotc.emaris.requirement.push;

import static org.iotc.emaris.requirement.Constants.layoutProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;

import org.iotc.emaris.requirement.Constants;
import org.iotc.emaris.requirement.Requirement;
import org.iotc.emaris.requirement.data.RequirementDao;

import apprise.backend.event.EventBus;
import apprise.backend.iam.model.User;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;

@ApplicationScoped
public class Observers {

    public static final String topic = Constants.requirementType;

    @Inject
    User logged;

    @Inject
    EventBus bus;

    @Inject
    RequirementDao requirements;

    Requirement minimalCopyOf (Requirement requirement) {

        var minimalCopy = requirements.get(requirement.id());

        minimalCopy.properties().clear(layoutProperty);

        return minimalCopy;
    }

    void onAdd(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Added Requirement requirement) {

        onEvent(new RequirementChange().type(RequirementChange.Type.add).requirement(minimalCopyOf(requirement)));
    }

    void onUpdate(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated Requirement requirement) {

        onEvent(new RequirementChange().type(RequirementChange.Type.change).requirement(minimalCopyOf(requirement)));
    }

    void onRemove(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed Requirement requirement) {

        requirement.properties().clear(layoutProperty);

        onEvent(new RequirementChange().type(RequirementChange.Type.remove).requirement(requirement));
    }   

    void onEvent( RequirementChange change) {

        bus.instanceFor(topic).publish(change.origin(logged.username()));
    }
    
}
