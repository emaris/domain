package org.iotc.emaris.product.push;

import org.iotc.emaris.product.Product;

import apprise.backend.event.StandardEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ProductChange extends StandardEvent<ProductChange> {

    public static enum Type { add, remove, change }

    Product product;
    Type type;

}

