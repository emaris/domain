package org.iotc.emaris.product.push;

import static org.iotc.emaris.product.Constants.layoutProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;

import org.iotc.emaris.product.Constants;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.product.data.ProductDao;

import apprise.backend.event.EventBus;
import apprise.backend.iam.model.User;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;

@ApplicationScoped
public class Observers {

    public static final String topic = Constants.productType;

    @Inject
    User logged;

    @Inject
    EventBus bus;

    @Inject
    ProductDao products;


    Product minimalCopyOf (Product product) {

        var minimalCopy = products.get(product.id());

        minimalCopy.properties().clear(layoutProperty);

        return minimalCopy;
    }

    void onAdd(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Added Product product) {


        onEvent(new ProductChange().type(ProductChange.Type.add).product(minimalCopyOf(product)));
    }

    void onUpdate(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated Product product) {


        onEvent(new ProductChange().type(ProductChange.Type.change).product(minimalCopyOf(product)));
    }

    void onRemove(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed Product product) {

        product.properties().clear(layoutProperty);

        onEvent(new ProductChange().type(ProductChange.Type.remove).product(product));
    }   

    void onEvent( ProductChange change) {

        bus.instanceFor(topic).publish(change.origin(logged.username()));
    }
    
}
