package org.iotc.emaris.product;

import static apprise.backend.iam.model.Action.action;
import static org.iotc.emaris.product.Constants.productType;

import apprise.backend.iam.model.Action;

public class Rights {

    public static final Action manage_products = action("manage").type(productType).make();
    public static final Action edit_products = action("manage","edit").type(productType).make();

}
