package org.iotc.emaris.product;

public class Constants {
    
	public static final String productPrefix = "PD";
	public static final String productType = "product";
	public static final String layoutProperty = "layout";
}
