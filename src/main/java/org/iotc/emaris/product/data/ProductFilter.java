package org.iotc.emaris.product.data;

import static org.iotc.emaris.product.data.DataConstants.product_state;

import org.iotc.emaris.product.Product;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class ProductFilter {

	Product.State state;

	public static ProductFilter filter() {
		return new ProductFilter();
	}

	public static ProductFilter withNoConditions() {
		return new ProductFilter();
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (state != null)
			condition = condition.and(product_state.eq(state.name()));
		
		return condition;
	} 
}