package org.iotc.emaris.product.data;


import static apprise.backend.validation.Mode.create;
import static org.iotc.emaris.product.Constants.productPrefix;
import static org.iotc.emaris.product.Product.State.inactive;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.product.Product;

import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class ProductService {

    @Inject
    ProductDao dao;

    @Inject
    User logged;

    @Inject
    Event<Product> events;

    public Product add(@NonNull Product product) {

        if (product.id() == null)
            product.id(Id.mint(productPrefix));

        if (product.state() == null)
            product.state(inactive);

        product.validateNow(create);

        product.touchedBy(logged.username());

        log.info("adding product {}", product.ref());

        dao.add(product);

        events.select(Added.event).fire(product);

        return product;
    }

    public Product update(@NonNull Product updated) {

        Product product = dao.get(updated.id());

        product.updateWith(updated);

        product.validateNow(Mode.update);

        product.touchedBy(logged.username());

        log.info("updating product {}", product.ref());

        dao.update(product);

        events.select(Updated.event).fire(product);

        return product;

    }

    public Product remove(@NonNull String id) {

        Product product = dao.get(id);

        product.validateNow(Mode.delete);
					
		// observers might still want to see who's removing this.
		product.touchedBy(logged.username());
		
		log.info("removing product {}", product.ref());
		
		dao.remove(product);

		events.select(Removed.event).fire(product);
		
		return product;
	

	}
}
