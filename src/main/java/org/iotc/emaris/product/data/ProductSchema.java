package org.iotc.emaris.product.data;

import static org.iotc.emaris.product.data.DataConstants.product_details;
import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.JSONB;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.product.Product;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.SchemaDao;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class ProductSchema {

    @Inject
    Config config;

    @Inject
    SchemaDao schemas;

    @Inject
    DSLContext jooq;

    @Getter
    Table<Record> products;

    @Inject
    @Getter
    Binder<Product> binder;

    @Inject
    Event<SchemaReady<Product>> events;

    @Getter
    RecordMapper<Record, Product> mapper = record -> binder.bind(record.get(product_details).data());


    public boolean isStaged() {

       return schemas.exists(products);

    }

    public void stageTables() {

        products = table(name(config.dbschema(), DataConstants.product_table));

        boolean firstTime = !schemas.exists(products);

        String stmt = jooq.createTableIfNotExists(products)
                .column(DataConstants.product_id, VARCHAR.length(100))
                .column(DataConstants.product_state, VARCHAR.nullable(false))
                .column(DataConstants.product_details, JSONB.nullable(false))
                .column(DataConstants.product_lineage, VARCHAR.nullable(true))
                .constraints(
                        constraint(products.getName() + "_pkey").primaryKey(DataConstants.product_id))
                .getSQL();

        jooq.execute(stmt);

       events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());
        

    }
}
