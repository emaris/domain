package org.iotc.emaris.product.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static org.iotc.emaris.product.data.DataConstants.product_details;
import static org.iotc.emaris.product.data.DataConstants.product_id;
import static org.iotc.emaris.product.data.DataConstants.product_lineage;
import static org.iotc.emaris.product.data.DataConstants.product_state;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.product.Product;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.JSONB;
import org.jooq.Record;

import apprise.backend.Exceptions.NoSuchEntityException;

@ApplicationScoped
public class ProductDao {

    @Inject
    DSLContext jooq;
    
    @Inject
    ProductSchema schema;

    public List<Product> all(ProductFilter filter) {

        return jooq.select()
                .from(schema.products())
                .where(filter.toCondition())
                .fetch(schema.mapper());

    }

    public String rawSummaries() {

        return jooq.fetchOne(format(
                "select jsonb_agg((%s #- '{properties,layout}')  - 'documents') from %s", product_details,
                schema.products()))
                .get(0, JSONB.class).data();

    }

    public int size(ProductFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.products()).where(filter.toCondition()));
    }

    public Product get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown product %s", id)));

    }

    public Optional<Product> lookup(String id) {

        return Optional.ofNullable(jooq.select()
                .from(schema.products())
                .where(product_id.equalIgnoreCase(id))
                .fetchOne(schema.mapper()));


    }

    public void add(Product product) {

        try {

            add(product, false);

        } catch (RuntimeException dae) {

            map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(format(
                                    ("product refers to resources that don't exist (lineage)."), product.id())))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("product %s already exists."), product.id())))

            );
        }

    }

    public void add(Product product, boolean ignoreOnDuplicate) {

        InsertOnDuplicateStep<Record> insert = jooq.insertInto(schema.products())
                .set(product_id, product.id())
                .set(product_state, product.state().name())
                .set(product_lineage,
                        product.lineage() == null ? null : product.lineage().stream().collect(joining(":")))
                .set(product_details, JSONB.valueOf(schema.binder().bind(product)));

        if (ignoreOnDuplicate)
            insert.onDuplicateKeyIgnore();

        insert.execute();


    }

    public void update(Product product) {

        jooq.update(schema.products())
                .set(product_state, product.state().name())
                .set(product_details, JSONB.valueOf(schema.binder().bind(product)))
                .set(product_lineage,
                        product.lineage() == null ? null : product.lineage().stream().collect(joining(":")))
                .where(product_id.equalIgnoreCase(product.id()))
                .execute();

    }

    public void remove(Product product) {

        jooq.delete(schema.products()).where(product_id.equalIgnoreCase(product.id())).execute();

    }
}
