package org.iotc.emaris.product.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;
import org.jooq.JSONB;

public class DataConstants {

    public static final String product_table  = "product_opt";

    public static final Field<String> product_id = field(name(product_table, "id"), String.class);
    public static final Field<String> product_state = field(name(product_table,"state"), String.class);
    public static final Field<String> product_lineage = field(name(product_table,"lineage"), String.class);
	public static final Field<JSONB> product_details = field(name(product_table, "details"), JSONB.class);
    
}
