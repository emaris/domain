package org.iotc.emaris.product.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.product.Product;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<org.iotc.emaris.product.Product> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, Product.class);
    } 
}
