package org.iotc.emaris.product.api;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.product.Constants.layoutProperty;
import static org.iotc.emaris.product.api.ApiConstants.productapi;
import static org.iotc.emaris.product.data.ProductFilter.withNoConditions;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.product.data.ProductService;

import lombok.NonNull;

public interface LifecycleApi {

	@Operation(summary = "Returns all products")

	List<Product> fetchAll();

	Response fetchRawSummaries();

	@Operation(summary="Returns one product")

	Product fetchOne(String rid);


	@Operation(summary="Creates a new product", description="Requires management rights")
	Product addOne(Product product);


	@Operation(summary="Updates a product",description="Requires management rights")
	
	Product updateOne(String rid, Product product);

	
	@Operation(summary="Deletes a product",description="Requires management rights")
	void removeOne(String rid);





	@Path(productapi)
	public class Default implements LifecycleApi {

		@Inject
		ProductDao dao;

		@Inject
		ProductService service;

		@GET
		@Produces(APPLICATION_JSON)
		@Override
		public List<Product> fetchAll() {
			
			return dao.all(withNoConditions()).stream().map(p->{ 
				
				p.properties().clear(layoutProperty); 
				return p;
			
			}).collect(toList());

		}


		@GET
		@Path("raw")
		@Produces(APPLICATION_JSON)
		@Override
		public Response fetchRawSummaries() {

			return Response.ok().entity(dao.rawSummaries()).build();

		}
		
		@GET
		@Produces(APPLICATION_JSON)
		@Path("{rid}")
		@Override
		public Product fetchOne(@NonNull @PathParam("rid") String rid) {
			
			return dao.get(rid);
		
		}


		@POST
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Product addOne(@NonNull Product product) {
			
			return service.add(product);
		}


		@PUT
		@Path("{rid}")
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Product updateOne(@NonNull @PathParam("rid") String tid, Product product) {
			
			if (!product.id().equals(tid))
				throw new IllegalArgumentException(format("product id '%s' does not match id in payload ('%s')",tid, product.id()));
			
			return service.update(product);
		}


		@DELETE 
		@Path("{rid}") 
		@Override 
		public void removeOne(@NonNull @PathParam("rid") String id) {
			
			service.remove(id);
			
		}
	}
}


