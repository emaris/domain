package org.iotc.emaris.product.api;

import static java.lang.Integer.MAX_VALUE;
import static org.iotc.emaris.product.Rights.edit_products;
import static org.iotc.emaris.product.Rights.manage_products;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.iotc.emaris.product.Product;

import apprise.backend.iam.model.User;

@Decorator
@Priority(MAX_VALUE)
public abstract class LifecycleSecureApi implements LifecycleApi {

    @Inject
    @Delegate
    LifecycleApi unsecure;

    @Inject
    User logged;

    @Override
    public Product addOne(Product product) {

        logged.assertCan(manage_products);

        return unsecure.addOne(product);
    }

    @Override
    public Product updateOne(String id, Product product) {

        logged.assertCan(edit_products.on(id));

        return unsecure.updateOne(id, product);
    }


    @Override
	public void removeOne(String id) {
	
		logged.assertCan(manage_products.on(id));
		
		unsecure.removeOne(id);
	}

}
