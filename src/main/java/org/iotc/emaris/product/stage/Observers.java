package org.iotc.emaris.product.stage;

import static apprise.backend.iam.stage.StagedData.bootadmin;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.iotc.emaris.product.data.ProductDao;
import org.iotc.emaris.product.Product;

import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.Staged;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    ProductDao dao;




    @Transactional
    void stageAllOnFirstStart(@Observes @Added SchemaReady<Product> event,  @Staged Instance<Product> singletons, @Staged Instance<List<Product>> groups) {

        stageProducts(singletons, groups,true);
    }

    @Transactional
    void stagePredefineOnStart(@Observes @Updated SchemaReady<Product> event, @Staged Instance<Product> singletons,@Staged Instance<List<Product>> groups) {

        stageProducts(singletons,groups,false);
    }


    void stageProducts(@Staged Instance<Product> singletons,Instance<List<Product>> groups, boolean firstStart ) {

        Predicate<Product> selected = r -> firstStart ? true : r.predefined();

        List<Product> products = singletons.stream().filter(selected).collect(toList());

        products.addAll(groups.stream()
            .flatMap(t -> t.stream())
            .filter(selected)
            .collect(toList()));

        if (products.size() > 0) {
           
            log.info("staging products ({})", products.stream().map(c -> c.id()).collect(joining(" , ")));
           
            products.stream()
                    .map(r -> r.state(Product.State.active).touchedBy(bootadmin.username()))
                    .forEach(r -> dao.add(r, true));
        }
    }

}
