package org.iotc.emaris.campaign;

import static java.lang.String.format;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import apprise.backend.model.Bag;
import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.model.Multilingual;
import apprise.backend.model.Predefined;
import apprise.backend.tag.Tagged;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Campaign implements Lifecycled<Campaign.State, Campaign>, Predefined<Campaign.Predefined, Campaign>,
        Tagged<Campaign>, Validated, Comparable<Campaign> {

    public static enum State {

        active, archived

    }

    public static enum Predefined {
        guarded
    }

    String id;

    boolean guarded;

    boolean predefined;
    Set<Predefined> predefinedProperties = new HashSet<>();

    Lifecycle<State> lifecycle = new Lifecycle<>(State.active);

    Multilingual name = new Multilingual();
    Multilingual description = new Multilingual();

    Set<String> tags = new HashSet<>();

    String lineage;

    Bag properties = new Bag();

    public Campaign(Campaign other) {

        updateWith(other)
                .id(other.id())
                .predefined(other.predefined())
                .predefinedProperties(new HashSet<>(other.predefinedProperties()))
                .guarded(other.guarded())
                .tags(other.tags())
                .lineage(other.lineage)
                .lifecycle(new Lifecycle<>(other.lifecycle()));

    }

    public Optional<String> lineage() {
        return Optional.ofNullable(lineage);
    }

    public Campaign copy() {

        return new Campaign(this);

    }

    public Campaign updateWith(Campaign other) {

        // these can change at all times
        name(new Multilingual(other.name()))
                .description(new Multilingual(other.description()))
                .properties(new Bag(other.properties()));

        guarded(predefined(Predefined.guarded) ? this.guarded() : other.guarded());

        if (!this.guarded())
            tags(other.tags())
                    .lineage(other.lineage)
                    .lifecycle().updateWith(other.lifecycle());

        return this;

    }

    public Campaign tags(Collection<String> tags) {
        this.tags = new HashSet<>(tags);
        return this;
    }

    public String ref() {
        return format("%s (%s)", name.inDefaultLanguage(), id);
    }

    @Override
    public List<String> validate(Mode mode) {

        return CheckDsl.given(this)
                .check($ -> id != null).or("missing identifier")
                .provided(name != null)
                .check($ -> name.inDefaultLanguage() != null).or("no name in default language")
                .provided(mode == Mode.create)
                .check($ -> !predefined()).or("invalid state: predefined")
                .provided(mode == Mode.delete)
				.check($ -> !predefined && !guarded)
				.or("cannot remove campaign %s because it is active, predefined, or protected", ref())
                .andCollect();
    }

    @Override
    public int compareTo(Campaign o) {
        return name().compareTo(o.name());
    }
}
