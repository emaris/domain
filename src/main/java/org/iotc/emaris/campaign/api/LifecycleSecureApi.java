package org.iotc.emaris.campaign.api;

import static java.lang.Integer.MAX_VALUE;
import static org.iotc.emaris.campaign.Rights.manage_campaigns;
import static org.iotc.emaris.campaign.Rights.edit_campaigns;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.archive.Dtos.CampaignData;

import apprise.backend.iam.model.User;

@Decorator
@Priority(MAX_VALUE)
public abstract class LifecycleSecureApi implements LifecycleApi {

    @Inject
    @Delegate
    LifecycleApi unsecure;

    @Inject
    User logged;

    @Override
    public Campaign addOne(Campaign campaign) {

        logged.assertCan(manage_campaigns);

        return unsecure.addOne(campaign);
    }

    @Override
    public CampaignData updateOne(String id, Campaign campaign) {

        logged.assertCan(edit_campaigns.on(id));

        return unsecure.updateOne(id, campaign);
    }


    @Override
	public void removeOne(String id) {
	
		logged.assertCan(manage_campaigns.on(id));
		
		unsecure.removeOne(id);
	}

}
