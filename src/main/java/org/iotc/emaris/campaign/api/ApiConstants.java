package org.iotc.emaris.campaign.api;


public class ApiConstants {
	
	public static final String campaignapi = "campaign";
	public static final String usageapi = "usage";
	public static final String includeArchive = "archive";

}
