package org.iotc.emaris.campaign.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.requirement.Constants.requirementType;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;

import lombok.Data;
import lombok.NonNull;

public interface UsageApi {



	UsageData usage(String type, String template);


	@Data
	public static class UsageData {

		List<String> campaigns;

	}


	@Path(ApiConstants.campaignapi+"/"+ApiConstants.usageapi) 
	public class Default implements UsageApi {

		@Inject
		RequirementInstanceDao requirements;


		@Inject
		ProductInstanceDao products;

		@Inject
		EventInstanceDao events;


	 
		@GET
		@Path("{type}/{tid}")
		@Produces(APPLICATION_JSON)
		@Override
		public UsageData usage(@NonNull @PathParam("type") String type, @NonNull @PathParam("tid") String template) {

			List<String> campaigns = new ArrayList<>();

			switch(type) {

				case requirementType : campaigns.addAll(requirements.usage(template)); break;
				case productType : campaigns.addAll(products.usage(template)); break;
				case eventType : campaigns.addAll(events.usage(template)); break;
			}

			
			return new UsageData().campaigns(campaigns);
		
		}

	}
		
}


