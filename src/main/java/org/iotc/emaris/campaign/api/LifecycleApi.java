package org.iotc.emaris.campaign.api;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.iotc.emaris.campaign.api.ApiConstants.campaignapi;
import static org.iotc.emaris.campaign.api.ApiConstants.includeArchive;
import static org.iotc.emaris.campaign.data.CampaignFilter.withNoConditions;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.Campaign.State;
import org.iotc.emaris.campaign.archive.CampaignArchiveDao;
import org.iotc.emaris.campaign.archive.Dtos.CampaignData;
import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.campaign.data.CampaignInstanceService;
import org.iotc.emaris.campaign.data.CampaignService;
import org.iotc.emaris.campaign.data.CampaignSummary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

public interface LifecycleApi {

	@Operation(summary = "Returns all campaigns, along with key summary data")

	List<CampaignAndSummary> fetchAll();


	@Operation(summary="Returns one campaign")

	CampaignData fetchOne(String rid, Boolean lineageMode);


	@Operation(summary="Creates a new campaign", description="Requires management rights")
	Campaign addOne(Campaign campaign);


	@Operation(summary="Updates a campaign",description="Requires management rights")
	
	CampaignData updateOne(String rid, Campaign campaign);

	
	@Operation(summary="Deletes a campaign",description="Requires management rights")
	void removeOne(String rid);


	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class CampaignAndSummary {

		Campaign campaign;
		CampaignSummary summary;
	}


	@Path(campaignapi)
	public class Default implements LifecycleApi {

		@Inject
		CampaignDao dao;

		@Inject
		CampaignService service;

		@Inject
		CampaignArchiveDao archive;

		@Inject
		CampaignInstanceService instances;

		@GET
		@Produces(APPLICATION_JSON)
		@Override
		public List<CampaignAndSummary> fetchAll() {
			
			return dao.all(withNoConditions()).stream().map(c->new CampaignAndSummary(c,instances.summaryOf(c))).collect((toList()));

		}

		@GET
		@Produces(APPLICATION_JSON)
		@Path("{cid}")
		@Override
		public CampaignData fetchOne(@NonNull @PathParam("cid") String cid, @QueryParam(includeArchive) @DefaultValue("true") Boolean mode) {
			
			Campaign campaign = dao.get(cid);

			CampaignData response = new CampaignData().campaign(campaign);

			return mode == true ?  response.archive(campaign.state() == State.archived ? archive.fetch(campaign.id())  : emptyList()) : response;
		
		}


		@POST
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public Campaign addOne(@NonNull Campaign campaign) {
			
			return service.add(campaign);
		}


		@PUT
		@Path("{rid}")
		@Consumes(APPLICATION_JSON)
		@Produces(APPLICATION_JSON)
		@Override
		public CampaignData updateOne(@NonNull @PathParam("rid") String tid, Campaign campaign) {
			
			if (!campaign.id().equals(tid))
				throw new IllegalArgumentException(format("campaign id '%s' does not match id in payload ('%s')",tid, campaign.id()));
			
			var updated =  service.update(campaign);

			return new CampaignData().campaign(campaign).archive(updated.state() == State.archived ? archive.fetch(updated.id())  : emptyList());
		}


		@DELETE 
		@Path("{rid}") 
		@Override 
		public void removeOne(@NonNull @PathParam("rid") String id) {
			
			service.remove(id);
			
		}
	}
}


