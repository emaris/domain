package org.iotc.emaris.campaign.data;

import static java.lang.String.format;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.enterprise.inject.Any;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.common.RemovedInstanceEvents;

import apprise.backend.message.data.MessageDao;
import apprise.backend.model.LifecycleQualifiers.Removed;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {
    
    @Inject
    MessageDao messages;

    void cleanMessagesOnCampaignRemoval(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed Campaign campaign) {

        var removed = messages.removeMatching(campaign.id());

        log.info("removed {} messages following removal of campaign {}", removed, campaign.name().inDefaultLanguage());
      
    }


    void cleanMessagesOnInstanceRemoval(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Any RemovedInstanceEvents.Event<? extends CampaignInstance<?>> event) {

        event.instances().forEach(instance -> {

            var removed = messages.removeMatching(instance.campaign(), format("%s:%s@%s", instance.campaign(), instance.source(), instance.instanceType()));

            log.info("removed {} messages following removal of instance {} in campaign {}", removed, instance.source(), instance.campaign());

        });

    }
}
