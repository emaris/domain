package org.iotc.emaris.campaign.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;

public class DataConstants {

    public static final String campaign_table = "campaign";

    public static final Field<String> campaign_id = field(name(campaign_table, "id"), String.class);
    public static final Field<String> campaign_state = field(name(campaign_table, "state"), String.class);
    public static final Field<String> campaign_lineage = field(name(campaign_table, "lineage"), String.class);
    public static final Field<String> campaign_details = field(name(campaign_table, "details"), String.class);

    public static final String archive_table  = "archive";

    public static final Field<String> archive_id = field(name(archive_table, "id"), String.class);
    public static final Field<String> archive_campaign = field(name(archive_table,"campaign"), String.class);
    public static final Field<String> archive_tenant = field(name(archive_table,"tenant"), String.class);
    public static final Field<String> archive_type = field(name(archive_table,"type"), String.class);
    public static final Field<String> archive_details = field(name(archive_table, "details"), String.class);

}
