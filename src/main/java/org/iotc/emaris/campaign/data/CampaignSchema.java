package org.iotc.emaris.campaign.data;

import static org.jooq.impl.DSL.constraint;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;
import static org.jooq.impl.SQLDataType.VARCHAR;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.data.Config;
import apprise.backend.data.SchemaDao;
import apprise.backend.data.Events.SchemaReady;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class CampaignSchema {

        @Inject
        Config config;

        @Inject
        SchemaDao schemas;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> campaigns;

        @Getter
        Table<Record> archive;

        @Inject
        @Getter
        Binder<Campaign> binder;

        @Inject
        Event<SchemaReady<Campaign>> events;

        @Getter
        RecordMapper<Record, Campaign> mapper = record -> binder.bind(record.get(DataConstants.campaign_details));

        public boolean isStaged() {

                return schemas.exists(campaigns) && schemas.exists((archive));

        }

        public void stageTables() {

                stageCampaigns();
                stageArchive();

        }

        public void stageCampaigns() {

                campaigns = table(name(config.dbschema(), DataConstants.campaign_table));

                boolean firstTime = !schemas.exists(campaigns);

                String stmt = jooq.createTableIfNotExists(campaigns)
                                .column(DataConstants.campaign_id, VARCHAR.length(100))
                                .column(DataConstants.campaign_state, VARCHAR.nullable(false))
                                .column(DataConstants.campaign_lineage, VARCHAR.nullable(true))
                                .column(DataConstants.campaign_details, VARCHAR.nullable(false))
                                .constraints(
                                                constraint(campaigns.getName() + "_pkey")
                                                                .primaryKey(DataConstants.campaign_id))
                                .getSQL();

                jooq.execute(stmt);

                events.select(firstTime ? Added.event : Updated.event).fire(SchemaReady.now());

        }


        public void stageArchive() {

                archive = table(name(config.dbschema(), DataConstants.archive_table));

                String stmt = jooq.createTableIfNotExists(archive)
                                .column(DataConstants.archive_id, VARCHAR.length(100).nullable(false))
                                .column(DataConstants.archive_campaign, VARCHAR.nullable(false))
                                .column(DataConstants.archive_type, VARCHAR.nullable(false))
                                .column(DataConstants.archive_tenant, VARCHAR.nullable(true))
                                .column(DataConstants.archive_details, VARCHAR.nullable(false))
                                .constraints(

                                                constraint(archive.getName() + "_pkey")
                                                                .unique(DataConstants.archive_id, DataConstants.archive_campaign),
                                                constraint("campaign_fkey").foreignKey(DataConstants.archive_campaign)
                                                                .references(campaigns, DataConstants.campaign_id)
                                                                .onDeleteCascade()

                                ).getSQL();


                jooq.execute(stmt);

                jooq.createIndexIfNotExists(archive.getName()+ "_campaign_index").on(archive, DataConstants.archive_campaign).execute();

        }
}
