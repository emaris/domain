package org.iotc.emaris.campaign.data;

import static org.iotc.emaris.campaign.data.DataConstants.campaign_state;

import org.iotc.emaris.campaign.Campaign;
import org.jooq.Condition;
import org.jooq.impl.DSL;

import lombok.Data;

@Data
public class CampaignFilter {

	Campaign.State state = null;

	public static CampaignFilter filter() {
		return new CampaignFilter();
	}

	public static CampaignFilter withNoConditions() {
		return new CampaignFilter();
	}

	Condition toCondition() {

		Condition condition = DSL.condition(true);

		if (state != null)
			condition = condition.and(campaign_state.eq(state.name()));
		
		return condition;
	} 
}