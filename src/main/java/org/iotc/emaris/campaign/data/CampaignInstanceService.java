package org.iotc.emaris.campaign.data;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;

@ApplicationScoped
public class CampaignInstanceService {

    @Inject
    EventInstanceDao dao;

    public CampaignSummary summaryOf(Campaign campaign) {

        CampaignSummary summary = new CampaignSummary();

        try {

            dao.startOf(campaign.id()).ifPresent(date->summary.startDate(date));
        }
        catch(Exception tolerate){}

        try {

            dao.endOf(campaign.id()).ifPresent(date->summary.endDate(date));
        }
        catch(Exception tolerate){}
       
        return summary;
    }
}
