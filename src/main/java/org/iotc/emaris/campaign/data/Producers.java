package org.iotc.emaris.campaign.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.iotc.emaris.campaign.Campaign;

import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    


    @Produces @ApplicationScoped
    Binder<Campaign> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, Campaign.class);
    } 
}
