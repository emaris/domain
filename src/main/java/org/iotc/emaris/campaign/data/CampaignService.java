package org.iotc.emaris.campaign.data;

import static apprise.backend.validation.Mode.create;
import static org.iotc.emaris.campaign.Campaign.State.active;
import static org.iotc.emaris.campaign.Campaign.State.archived;
import static org.iotc.emaris.campaign.Constants.campaignPrefix;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.archive.CampaignArchiveService;

import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class CampaignService {

    @Inject
    CampaignDao dao;

    @Inject
    User logged;

    @Inject
    Event<Campaign> events;

    @Inject
    CampaignArchiveService archiveService;

    public Campaign add(@NonNull Campaign campaign) {

        if (campaign.id() == null)
            campaign.id(Id.mint(campaignPrefix));

        if (campaign.state() == null)
            campaign.state(active);

        campaign.validateNow(create);

        campaign.touchedBy(logged.username());

        log.info("adding campaign {}", campaign.ref());

        dao.add(campaign);

        events.select(Added.event).fire(campaign);

        return campaign;
    }

    public Campaign update(@NonNull Campaign updated) {

        Campaign campaign = dao.get(updated.id());

        var archive = updated.state() == archived && campaign.state() != archived;

        campaign.updateWith(updated);

        campaign.validateNow(Mode.update);

        campaign.touchedBy(logged.username());

        log.info("updating campaign {}", campaign.ref());

        dao.update(campaign);

        if (archive)
            archiveService.archive(campaign);

        events.select(Updated.event).fire(campaign);
      
        return campaign;

    }

    public Campaign remove(@NonNull String id) {

        Campaign campaign = dao.get(id);

        campaign.validateNow(Mode.delete);

        // observers might still want to see who's removing this.
        campaign.touchedBy(logged.username());

        log.info("removing campaign {}", campaign.ref());

        dao.remove(campaign);

        events.select(Removed.event).fire(campaign);

        return campaign;

    }
}
