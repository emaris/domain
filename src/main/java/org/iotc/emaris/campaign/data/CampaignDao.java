package org.iotc.emaris.campaign.data;

import static apprise.backend.data.ErrorCode.dangling;
import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static apprise.backend.data.Utils.sanitized;
import static apprise.backend.tag.data.DataConstants.tagged;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.iotc.emaris.campaign.data.DataConstants.campaign_details;
import static org.iotc.emaris.campaign.data.DataConstants.campaign_id;
import static org.iotc.emaris.campaign.data.DataConstants.campaign_lineage;
import static org.iotc.emaris.campaign.data.DataConstants.campaign_state;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_campaign;
import static org.iotc.emaris.partyinstance.data.DataConstants.partyinstance_source;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.partyinstance.data.PartyInstanceSchema;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.Record;
import org.jooq.Select;
import org.jooq.SelectOnConditionStep;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.iam.LoginService;
import apprise.backend.iam.admin.data.TenantAwareDao;
import apprise.backend.tag.data.TagInstanceDao;
import apprise.backend.tag.data.TagMappers;
import apprise.backend.tag.data.TagSchema;

@ApplicationScoped
public class CampaignDao extends TenantAwareDao {

    @Inject
    DSLContext jooq;

    @Inject
    LoginService login;
  

    @Inject
    TagInstanceDao tags;

    @Inject
    TagSchema tagschema;

    @Inject
    PartyInstanceSchema partyinstancSchema;

    @Inject
    TagMappers tagmappers;

    @Inject
    CampaignSchema schema;

    public List<Campaign> all(CampaignFilter filter) {

        return fetch(filter.toCondition()).collect(toList());

    }

    public int size(CampaignFilter filter) {
        return jooq.fetchCount(jooq.select().from(schema.campaigns()).where(filter.toCondition()));
    }

    public Campaign get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown campaign %s", id)));

    }

    public Optional<Campaign> lookup(String id) {

        return fetch(campaign_id.equalIgnoreCase(id)).findFirst();

    }

    public void add(Campaign campaign) {

        try {

            add(campaign, false);

        } catch (RuntimeException dae) {

            map(dae,

                    on(dangling)
                            .to(() -> new IllegalStateException(format(
                                    ("campaign refers to resources that don't exist (lineage)."), campaign.id())))

                    ,

                    on(duplicate)
                            .to(() -> new IllegalStateException(
                                    format(("campaign %s already exists."), campaign.id())))

            );
        }

    }

    public void add(Campaign campaign, boolean ignoreOnDuplicate) {

        InsertOnDuplicateStep<Record> insert = jooq.insertInto(schema.campaigns())
                .set(campaign_id, campaign.id())
                .set(campaign_state, campaign.state().name())
                .set(campaign_lineage, campaign.lineage() == null ? null : campaign.lineage().stream().collect(joining(":")))
                .set(campaign_details, schema.binder().bind(pruned(campaign)));

        if (ignoreOnDuplicate)
            insert.onDuplicateKeyIgnore();

        int inserted = insert.execute();

        if (inserted>0)
            tags.replaceTags(campaign.id(), campaign.tags());


    }


	public void update(Campaign campaign) {

		jooq.update(schema.campaigns())
                .set(campaign_state, campaign.state().name())
                .set(campaign_lineage, campaign.lineage() == null ? null : campaign.lineage().stream().collect(joining(":")))
				.set(campaign_details, schema.binder().bind(pruned(campaign)))
				.where(campaign_id.equalIgnoreCase(campaign.id()))
				.execute();

		tags.replaceTags(campaign.id(), campaign.tags());

    }
    
	public void remove(Campaign campaign) {

		jooq.delete(schema.campaigns()).where(campaign_id.equalIgnoreCase(campaign.id())).execute();

		tags.removeTags(campaign.id());

    }
    



    // helpers

    private Stream<Campaign> fetch(Condition... conditions) {

        SelectOnConditionStep<Record> basequery = jooq.select()
                .from(schema.campaigns())
                .leftJoin(tagschema.tag_instances()).on(campaign_id.eq(tagged));

        Select<Record> query = (login.logged().map(u->u.tenant().none()).orElse(true) ? 
        
                    basequery : 
                    
                    basequery.join(partyinstancSchema.partyinstances())
                    // if there's a tenant we strictly join with party instances to then filter bsed on logged user.
                    .on(partyinstance_campaign.eq(campaign_id))
                    .and(filterForTenancyOver(partyinstance_source))
                )
        
        .where(conditions);

        return query.fetchGroups(schema.mapper())
                .entrySet()
                .stream()
                .map(e -> {

                    Campaign campaign = e.getKey();

                    campaign.tags(
                            sanitized(e.getValue().stream().map(tagmappers.tag_instance()::map).collect(toList())));

                    return campaign;
                });

    }

    // removed normalized relationships for serialisations (reconstructed on
    // deserialisation)
    private Campaign pruned(Campaign campaign) {

        return campaign.copy().tags(emptyList());

    }
}
