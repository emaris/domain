package org.iotc.emaris.campaign.data;

import java.time.OffsetDateTime;

import lombok.Data;

@Data
public class CampaignSummary {
    
    OffsetDateTime startDate;
    OffsetDateTime endDate;
}
