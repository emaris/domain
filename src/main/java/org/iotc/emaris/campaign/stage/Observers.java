package org.iotc.emaris.campaign.stage;

import static apprise.backend.iam.stage.StagedData.bootadmin;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.function.Predicate;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.data.CampaignDao;

import apprise.backend.data.Events.SchemaReady;
import apprise.backend.data.Staged;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Updated;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

    @Inject
    CampaignDao dao;

    void stageAllOnFirstStart(@Observes @Added SchemaReady<Campaign> event, @Staged Instance<Campaign> singletons,
            @Staged Instance<List<Campaign>> groups) {

        stageCampaigns(singletons, groups, true);
    }

    void stagePredefineOnStart(@Observes @Updated SchemaReady<Campaign> event,
            @Staged Instance<Campaign> singletons, @Staged Instance<List<Campaign>> groups) {

        stageCampaigns(singletons, groups, false);
    }

    void stageCampaigns(@Staged Instance<Campaign> singletons, Instance<List<Campaign>> groups,
            boolean firstStart) {

        Predicate<Campaign> selected = r -> firstStart ? true : r.predefined();

        List<Campaign> campaigns = singletons.stream().filter(selected).collect(toList());

        campaigns.addAll(groups.stream()
                .flatMap(t -> t.stream())
                .filter(selected)
                .collect(toList()));

        if (campaigns.size() > 0) {

            log.info("staging campaigns ({})", campaigns.stream().map(c -> c.id()).collect(joining(" , ")));

            campaigns.stream()
                    .map(r -> r.state(Campaign.State.active).touchedBy(bootadmin.username()))
                    .forEach(r -> dao.add(r, true));
        }
    }

}
