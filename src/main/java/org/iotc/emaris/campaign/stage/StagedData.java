package org.iotc.emaris.campaign.stage;

import static apprise.backend.Exceptions.storyOf;
import static apprise.backend.model.Multilingual.text;
import static apprise.backend.model.Multilingual.Language.fr;
import static apprise.backend.tag.Constants.categoryPrefix;
import static apprise.backend.tag.Constants.tagPrefix;
import static apprise.backend.tag.TagCategory.Cardinality.one;
import static apprise.backend.tag.stage.StagedData.idformat;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.iotc.emaris.campaign.Constants.campaignType;
import static org.iotc.emaris.campaign.Constants.complianceType;
import static org.iotc.emaris.event.Constants.eventPrefix;
import static org.iotc.emaris.stage.StagedData.iotcToggle;

import java.io.InputStream;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.event.Event;
import org.iotc.emaris.event.Event.Predefined;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.config.CoreConfig;
import apprise.backend.data.Staged;
import apprise.backend.model.Bag;
import apprise.backend.tag.Tag;
import apprise.backend.tag.TagCategory;
import apprise.backend.tag.stage.StagedData.Field;
import apprise.backend.tag.stage.StagedData.Field.Type;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class StagedData {

        private static final String campaignResource = "/META-INF/iotc-campaigns.stage";

        @Produces
        @Staged
        List<Campaign> campaigns(CoreConfig config, ObjectMapper mapper) {

                if (!config.toggles().isActive(iotcToggle))
                        return emptyList();

                InputStream instances = getClass().getResourceAsStream(campaignResource);

                if (instances == null) {
                        log.warn("cannot stage campaigns, no {} ", campaignResource);
                        return emptyList();
                }

                try {
                        return asList(mapper.readValue(instances, Campaign[].class));
                } catch (Throwable t) {
                        log.warn("cannot stage campaigns: {} ", storyOf(t));
                        return emptyList();
                }

        }

        @Produces
        @Staged
        public static Event campaignStart = new Event()
                        .id(format(idformat, eventPrefix, campaignType, "start"))
                        .type(campaignType)
                        .predefine(Predefined.cardinality)
                        .predefined(true)
                        .managed(true)
                        .name(text().inDefaultLanguage("Campaign Start").in(fr, "Début de campagne"))
                        .description(text().inDefaultLanguage("The start of a campaign.").in(fr,
                                        "Le début d'une campagne."))
                        .properties(new Bag()
                                        .set("relatable", true).withDefault());

        @Produces
        @Staged
        public static Event campaignEnd = new Event()
                        .id(format(idformat, eventPrefix, campaignType, "end"))
                        .type(campaignType)
                        .predefined(true)
                        .predefine(Predefined.cardinality)
                        .managed(true)
                        .name(text().inDefaultLanguage("Campaign End").in(fr, "Fin de campagne"))
                        .description(text().inDefaultLanguage("The end of a campaign.").in(fr,
                                        "Le fin d'une campagne."))
                        .properties(new Bag()
                                        .set("relatable", true).withDefault());

        @Produces
        @Staged
        public static TagCategory defaultScale = new TagCategory()
                        .id(format(idformat, categoryPrefix, complianceType, "defaultscale"))
                        .type(complianceType)
                        .cardinality(one)

                        .name(text()
                                        .inDefaultLanguage("Default Scale")
                                        .in(fr, "Échelle par Défaut"))

                        .description(text()
                                        .inDefaultLanguage(
                                                        "A predefined vocabulary to assess the compliance of a party to one or more obligations.")
                                        .in(fr, "Vocabulaire prédéfini pour évaluer la conformité d'une partie à une ou plusieurs obligations."))

                        .properties(new Bag()
                                        .set("color", "#e65100").withDefault()
                                        .set(Field.name, Field.class).with(f ->

                                        f.enabled(true)
                                                        .type(Type.radio)
                                                        .title(text()
                                                                        .inDefaultLanguage("Level")
                                                                        .in(fr, "Niveau"))
                                                        .message(text()
                                                                        .inDefaultLanguage(
                                                                                        "Select one compliance level.")
                                                                        .in(fr, "Sélectionnez un niveau de conformité."))
                                                        .help(text()
                                                                        .inDefaultLanguage(
                                                                                        "A compliance level measures the extent and quality of the actions taken by a party to meet its obligations.")
                                                                        .in(fr, "Un niveau de conformité mesure l'étendue et la qualité des mesures prises par une partie pour s'acquitter de ses obligations."))

                                        ));

        @Produces
        @Staged
        public static Tag notCompliant = new Tag()
                        .id(format(idformat, tagPrefix, complianceType, "notcompliant"))
                        .type(complianceType)
                        .category(defaultScale.id())
                        .name(text()
                                        .inDefaultLanguage("Not Compliant")
                                        .in(fr, "Non conforme"))
                        .description(text()
                                        .inDefaultLanguage("Not Compliant")
                                        .in(fr, "Non conforme"))

                        .properties(new Bag()
                                        .set("value", 0).withDefault());

        @Produces
        @Staged
        public static Tag partiallyCompliant = new Tag()
                        .id(format(idformat, tagPrefix, complianceType, "partiallycompliant"))
                        .type(complianceType)
                        .category(defaultScale.id())
                        .name(text()
                                        .inDefaultLanguage("Partially Compliant")
                                        .in(fr, "Partiellement conforme"))
                        .description(text()
                                        .inDefaultLanguage("Partially Compliant")
                                        .in(fr, "Partiellement conforme"))
                        .properties(new Bag()
                                        .set("value", 10).withDefault());

        @Produces
        @Staged
        public static Tag compliant = new Tag()
                        .id(format(idformat, tagPrefix, complianceType, "compliant"))
                        .type(complianceType)
                        .category(defaultScale.id())
                        .name(text()
                                        .inDefaultLanguage("Compliant")
                                        .in(fr, "Conforme"))
                        .description(text()
                                        .inDefaultLanguage("Compliant")
                                        .in(fr, "Conforme"))
                        .properties(new Bag()
                                        .set("value", 30).withDefault());

        public StagedData() {
        }
}
