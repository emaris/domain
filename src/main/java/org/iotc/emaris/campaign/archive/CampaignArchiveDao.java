package org.iotc.emaris.campaign.archive;

import static apprise.backend.Exceptions.sneak;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.archive.Dtos.ArchiveEntry;
import org.iotc.emaris.campaign.data.CampaignSchema;
import org.iotc.emaris.campaign.data.DataConstants;
import org.jooq.DSLContext;
import org.jooq.Query;
import org.jooq.Record;
import org.jooq.exception.DataAccessException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.data.ErrorCode;
import apprise.backend.iam.admin.data.TenantAwareDao;
import lombok.SneakyThrows;

@ApplicationScoped
public class CampaignArchiveDao extends TenantAwareDao {

    @Inject
    CampaignSchema schema;

    @Inject
    DSLContext jooq;

    @Inject
    ObjectMapper mapper;

    public List<JsonNode> fetch(String campaign) {

        Function<Record, JsonNode> map = sneak( e ->
        
            mapper.readTree(e.get( DataConstants.archive_details))
    
        );

        return jooq.select().from(schema.archive()).where(DataConstants.archive_campaign.eq(campaign)).and(filterForTenancyOver(DataConstants.archive_tenant)).fetch(map::apply);

    }


    @SneakyThrows
    public void add(List<ArchiveEntry> entries) {

       Collection<Query> stmts = new ArrayList<>();

		for (ArchiveEntry entry : entries)
			stmts.add(jooq.insertInto(schema.archive())
					.columns(DataConstants.archive_id, DataConstants.archive_campaign, DataConstants.archive_tenant, DataConstants.archive_type, DataConstants.archive_details)
					.values(entry.id(),
                            entry.campaign(),
                            entry.tenant(),
							entry.type(),
							mapper.writeValueAsString(entry)));

        try {

			jooq.batch(stmts).execute();

		} catch (DataAccessException dae) {

			ErrorCode.map(dae);
		}


    }

}
