package org.iotc.emaris.campaign.archive;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.archive.Dtos.ArchiveEntry;
import org.iotc.emaris.eventinstance.data.EventInstanceDao;
import org.iotc.emaris.eventinstance.data.EventInstanceFilter;
import org.iotc.emaris.partyinstance.data.PartyInstanceDao;
import org.iotc.emaris.partyinstance.data.PartyInstanceFilter;
import org.iotc.emaris.productinstance.data.ProductInstanceDao;
import org.iotc.emaris.productinstance.data.ProductInstanceFilter;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceDao;
import org.iotc.emaris.requirementinstance.data.RequirementInstanceFilter;

import apprise.backend.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class CampaignArchiveService {

    @Inject
    RequirementInstanceDao requirements;

    @Inject
    ProductInstanceDao products;

    @Inject
    PartyInstanceDao parties;

    @Inject
    EventInstanceDao events;

    @Inject
    CampaignArchiveDao dao;

    public void archive(Campaign campaign) {

        log.info("archiving {}", campaign.name().inDefaultLanguage());

        List<ArchiveEntry> entries = new ArrayList<>();

        entries.addAll(
                parties.allSourcesMatching(PartyInstanceFilter.filter().campaign(campaign.id())).stream()
                        .map(t -> new ArchiveEntry(campaign.id()).from(t))
                        .collect(toList()));

        entries.addAll(
                requirements.allSourcesMatching(RequirementInstanceFilter.filter().campaign(campaign.id())).stream()
                        .map(r -> new ArchiveEntry(campaign.id()).from(r))
                        .collect(toList()));

        entries.addAll(
                products.allSourcesMatching(ProductInstanceFilter.filter().campaign(campaign.id())).stream()
                        .map(p -> new ArchiveEntry(campaign.id()).from(p))
                        .collect(toList()));

        entries.addAll(
                events.allSourcesMatching(EventInstanceFilter.filter().campaign(campaign.id())).stream()
                        .map(e -> new ArchiveEntry(campaign.id()).from(e))
                        .collect(toList()));

        dao.add(entries);
    }

}
