package org.iotc.emaris.campaign.archive;

import static apprise.backend.iam.Constants.tenantType;
import static org.iotc.emaris.event.Constants.eventType;
import static org.iotc.emaris.product.Constants.productType;
import static org.iotc.emaris.requirement.Constants.requirementType;

import java.util.ArrayList;
import java.util.List;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.event.Event;
import org.iotc.emaris.product.Product;
import org.iotc.emaris.requirement.Requirement;

import com.fasterxml.jackson.databind.JsonNode;

import apprise.backend.iam.model.Tenant;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

public class Dtos {

    @Data
    @RequiredArgsConstructor
    @NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
    public static class ArchiveEntry {

        final String campaign;

        String id;
        String type;
        String tenant;
        Object details;
    
        ArchiveEntry from(Requirement requirement) {
    
            id(requirement.id()).type(requirementType).details(requirement);
    
            return this;
        }
    
    
        ArchiveEntry from(Product product) {
    
            id(product.id()).type(productType).details(product);
    
            return this;
        }
    
        ArchiveEntry from(Tenant tenant) {
    
            id(tenant.id()).type(tenantType).tenant(tenant.id()).details(tenant);
    
            return this;
        }
    
        ArchiveEntry from(Event event) {
    
            id(event.id()).type(eventType).details(event);
    
            return this;
        }

    }

    @Data
    public static class CampaignData {

        Campaign campaign;
        List<JsonNode> archive = new ArrayList<>();

    }

}
