package org.iotc.emaris.campaign;

import static apprise.backend.iam.model.Action.action;
import static org.iotc.emaris.campaign.Constants.campaignType;

import apprise.backend.iam.model.Action;

public class Rights {

    public static final Action manage_campaigns = action("manage").type(campaignType).make();
    public static final Action edit_campaigns = action("manage","edit").type(campaignType).make();

}
