package org.iotc.emaris.campaign;

import static java.lang.String.format;
import static org.iotc.emaris.campaign.Constants.refdelimiter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import apprise.backend.model.Bag;
import apprise.backend.tag.Tagged;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

public interface CampaignInstance<I extends CampaignInstance<I>> extends Tagged<I> {

    String id();

    I id(String id);

    String campaign();

    I campaign(String campaign);

    String instanceType();

    String source();

    I source(String source);

    Optional<InstanceRef> lineage();

    I lineage(InstanceRef lineage);


    Bag properties();

    I properties(Bag bag);

    I copy();

    default InstanceRef ref() {
        return new InstanceRef(campaign(), source());
    }

    @AllArgsConstructor
    @NoArgsConstructor(force = true)
    @Data
    public static class InstanceRef {

        final String campaign;
        final String source;

        public InstanceRef(String ref) {

            String[] parts = ref.split(refdelimiter);

            this.source=parts[0];
            this.campaign=parts[1];
    
        }

        public String toString() {
            return format("%s@%s", source(),campaign());
        }

    } 



    @Getter
    @EqualsAndHashCode
    @ToString
    @RequiredArgsConstructor
    @NoArgsConstructor(force = true)
    public static abstract class Base<I extends CampaignInstance<I>> implements CampaignInstance<I>, Validated {

        String id;
        String campaign;

        final String instanceType;
        String source;

        InstanceRef lineage;

        Bag properties = new Bag();

        Set<String> tags = new HashSet<>();

        protected Base(I other) {

            this.instanceType = other.instanceType();

            this.updateWith(other)
                    .id(other.id())
                    .campaign(other.campaign())
                    .source(other.source());
        }

        public I updateWith(I other) {

            this.tags(other.tags())
                    .properties(new Bag(other.properties()));

            if (other.lineage().isPresent())
                lineage(other.lineage().get());
                
            return self();

        }

        public I id(String id) {
            this.id = id;
            return self();
        }

        public I campaign(String campaign) {
            this.campaign = campaign;
            return self();
        }

        public I source(String source) {
            this.source = source;
            return self();
        }

        public Optional<InstanceRef> lineage() {

            return Optional.ofNullable(lineage);
        }

        public I lineage(InstanceRef lineage) {
            this.lineage = lineage;
            return self();
        }


        @Override
        public I tags(Collection<String> tags) {
            this.tags = new HashSet<>(tags);
            return self();
        }

        @Override
        public I properties(Bag properties) {
            this.properties = properties;
            return self();
        }

        @Override
        public List<String> validate(Mode mode) {

            return CheckDsl.given(this)
                    .check($ -> id() != null).or("missing identifier")
                    .check($ -> campaign() != null).or("missing campaign")
                    .provided(mode==Mode.create).check($ -> source() != null).or("missing source")
                    .check($ -> instanceType() != null).or("missing instance type")
                    .andCollect();

        }

        // helpers

        @SuppressWarnings("unchecked")
        private I self() {

            return (I) this;
        }

    }

}
