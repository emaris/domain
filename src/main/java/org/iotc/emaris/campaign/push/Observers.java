package org.iotc.emaris.campaign.push;

import static org.iotc.emaris.campaign.Constants.campaignType;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Inject;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.campaign.Campaign.State;
import org.iotc.emaris.campaign.data.CampaignDao;
import org.iotc.emaris.campaign.push.CampaignChange.Type;
import org.iotc.emaris.common.RemovedInstanceEvents;

import apprise.backend.event.EventBus;
import apprise.backend.iam.model.User;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;

@ApplicationScoped
//@Slf4j
public class Observers {

    public static final String topic = campaignType;

    @Inject
    User logged;

    @Inject
    EventBus bus;

    @Inject
    CampaignDao dao;

    void onAdd(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Added Campaign campaign) {

        var pushevent = new CampaignChange().type(Type.add).campaign(campaign).origin(logged.username());

        bus.instanceFor(topic).publish(pushevent);
    }

    void onUpdate(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated Campaign campaign) {

        var pushevent = new CampaignChange().type(Type.change).campaign(campaign).origin(logged.username());

        bus.instanceFor(topic).publish(pushevent);
    }

    void onRemove(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed Campaign campaign) {

        var pushevent = new CampaignChange().type(Type.remove).campaign(campaign).origin(logged.username());

        bus.instanceFor(topic).publish(pushevent);

    }

    // trigger:  new instances.
    void pushBulkInstancesAdd(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Added List<? extends CampaignInstance<?>> instances) {

        pushBulkInstancesChange(instances);

    }

    // trigger: one instance has changed.
    void pushInstanceChange(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated CampaignInstance<?> instance) {

        pushBulkInstancesChange(List.of(instance));

    }

    // trigger: many instances have changed.
    void pushBulkInstancesChange(@Observes(during = TransactionPhase.AFTER_SUCCESS) @Updated List<? extends CampaignInstance<?>> instances) {

        if (instances.isEmpty())
            return;

        var campaign = instances.get(0).campaign();

        var type = instances.get(0).instanceType();

        var pushevent = new CampaignChange().type(Type.change).instances(instances).instanceType(type)
                .origin(logged.username());

        dao.lookup(campaign).ifPresent(c ->  {
        
            if (c.state()!=State.archived)
                bus.instanceFor(topic).publish(pushevent.campaign(c));
        
        });

    }

    // NOTE: removal observers below are all high-priority because we want to push an event before trails and submissions are removed in cascade.
    // so we can query the trails and include them in the push event. this aggregation allows the client to handle a single event, instead of coordinating
    // cascade push events, which would be super-difficult.

    // trigger: many instances have been removed "directly", because clients called bulk apis.
    void pushBulkInstanceRemove(
        @Observes(during = TransactionPhase.AFTER_SUCCESS)  @Removed RemovedInstanceEvents.Event<?> event) {

        commonPushInstanceRemove(event, logged.username());

    }

    //  trigger: many instances have been removed in cascade from removing something else. 
    //  why separate the two? because we can omit the origin here so that it bounces back to the triggering browser and syncs it with cascade removals that
    //  are about to happen with lower-priority cascade observers.
    void pushBulkInstanceCascadeRemove(
        @Observes(during = TransactionPhase.AFTER_SUCCESS) @Removed(cascade = true) RemovedInstanceEvents.Event<?> event) {

        commonPushInstanceRemove(event, null);

    }

    // shared push on all instance removals, for any trigger.

    void commonPushInstanceRemove(RemovedInstanceEvents.Event<?> event, String origin) {

        if (event.instances().isEmpty())
            return;

        var type = event.instances().get(0).instanceType();

        var pushevent = new CampaignChange().type(Type.remove).instances(event.instances()).trails(event.trails()).instanceType(type).origin(origin);

        var campaign = event.instances().get(0).campaign();

        dao.lookup(campaign).ifPresent(c -> bus.instanceFor(topic).publish((pushevent.campaign(c))));

    }
}
