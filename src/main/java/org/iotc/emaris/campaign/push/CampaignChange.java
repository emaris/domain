package org.iotc.emaris.campaign.push;

import java.util.ArrayList;
import java.util.List;

import org.iotc.emaris.campaign.Campaign;
import org.iotc.emaris.campaign.CampaignInstance;
import org.iotc.emaris.submission.Trail;

import apprise.backend.event.StandardEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CampaignChange extends StandardEvent<CampaignChange> {

    public static enum Type { add, remove, change }

    Campaign campaign;
    List<? extends CampaignInstance<?>> instances = new ArrayList<>();
    List<? extends Trail> trails = new ArrayList<>();
    Type type;
    String instanceType;

}

