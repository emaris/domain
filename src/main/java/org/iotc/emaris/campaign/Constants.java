package org.iotc.emaris.campaign;

public class Constants {

    public static final String campaignPrefix="CMP";
    public static final String campaignType="campaign";
    
    public static final String complianceType="compliance";
    public static final String refdelimiter="@";

    public static final String suspendSubmissionsProperty = "suspendSubmissions";
    public static final String suspenOnEndProperty = "suspendOnEnd";
    

    public static final String muted = "muted";

}
