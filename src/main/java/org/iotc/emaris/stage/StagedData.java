package org.iotc.emaris.stage;

import static apprise.backend.Exceptions.storyOf;
import static apprise.backend.bytestream.model.Bytestream.State.active;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.io.InputStream;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.bytestream.model.Bytestream;
import apprise.backend.config.CoreConfig;
import apprise.backend.data.Staged;
import apprise.backend.iam.model.Tenant;
import apprise.backend.iam.model.User;
import apprise.backend.tag.Tag;
import apprise.backend.tag.TagCategory;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class StagedData {

    public static final String themesStream = "BS-themes";

    public static final String iotcToggle = "iotc";
    private static final String countryResource = "/META-INF/iotc-countries.stage";
    private static final String tagsResource = "/META-INF/iotc-tags.stage";
    private static final String categoriesResource = "/META-INF/iotc-categories.stage";
    private static final String usersResource = "/META-INF/iotc-users.stage";

    @Produces
    @Staged
    Bytestream themes = Bytestream.jsonstream(new Object[] {}).id(themesStream).state(active).name("themes.json");


    @Produces
    @Staged
    List<Tenant> countries(CoreConfig config, ObjectMapper mapper) {

        if (!config.toggles().isActive(iotcToggle))
            return emptyList();

        InputStream countries = getClass().getResourceAsStream(countryResource);

        if (countries == null) {
            log.warn("cannot stage countries, no {} ", countryResource);
            return emptyList();
        }

        try {
            return asList(mapper.readValue(countries, Tenant[].class));
        } catch (Throwable t) {
            log.warn("cannot stage countries: {} ", storyOf(t));
            return emptyList();
        }

    }

    @Produces
    @Staged
    List<User> users(CoreConfig config, ObjectMapper mapper) {

        if (!config.toggles().isActive(iotcToggle))
            return emptyList();

        InputStream users = getClass().getResourceAsStream(usersResource);

        if (users == null) {
            log.warn("cannot stage users, no {} ", usersResource);
            return emptyList();
        }

        try {
            return asList(mapper.readValue(users, User[].class));
        } catch (Throwable t) {
            log.warn("cannot stage users: {} ", storyOf(t));
            return emptyList();
        }

    }

    @Produces
    @Staged
    List<TagCategory> categories(CoreConfig config, ObjectMapper mapper) {

        if (!config.toggles().isActive(iotcToggle))
            return emptyList();

        InputStream categories = getClass().getResourceAsStream(categoriesResource);

        if (categories == null) {
            log.warn("cannot stage categories, no {} ", categoriesResource);
            return emptyList();
        }

        try {
            return asList(mapper.readValue(categories, TagCategory[].class));
        } catch (Throwable t) {
            log.warn("cannot stage categories: {} ", storyOf(t));
            return emptyList();
        }

    }


    @Produces
    @Staged
    List<Tag> tags(CoreConfig config, ObjectMapper mapper) {

        if (!config.toggles().isActive(iotcToggle))
            return emptyList();

        InputStream tags = getClass().getResourceAsStream(tagsResource);

        if (tags == null) {
            log.warn("cannot stage tags, no {} ", tagsResource);
            return emptyList();
        }

        try {
            return asList(mapper.readValue(tags, Tag[].class));
        } catch (Throwable t) {
            log.warn("cannot stage tags: {} ", storyOf(t));
            return emptyList();
        }

    }
}
