package org.iotc.emaris;

import static javax.interceptor.Interceptor.Priority.APPLICATION;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;

import apprise.backend.iam.admin.data.MemoryRoleDao;
import apprise.backend.iam.admin.data.RoleDao;

@ApplicationScoped
@Priority(APPLICATION)
public class Producers {


	// uses static roles

	@Produces
	@ApplicationScoped
	@Alternative
	RoleDao memorydao() {

		return new MemoryRoleDao();
	}
	
	//private static String jndiname="mail/apprise";

	// @Produces
	// @SneakyThrows
	// Session mailsession() {
	// 	return Session.class.cast( new InitialContext().lookup(jndiname) );
	// }
	
	
}
